<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Estrategia_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert_mision_vision($data){

        $query = $this->db->insert('MISION_VISION', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;
    }

    function existe_mv($rut){

        $sql="SELECT MV_ID
              FROM MISION_VISION
              WHERE '$rut'=EMP_RUT";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return 1; else return 0;

    }


    function get_mision_vision($rut){

        $sql="SELECT * FROM MISION_VISION WHERE EMP_RUT = '$rut'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
    }

    function actualizar_mision_vision($data,$rut) {

        $this->db->where('EMP_RUT', $rut);
        $query = $this->db->update('MISION_VISION', $data);



        if ($query)
            return true;
        else
            return false;
    }
    
    function insert_propuesta($data){

        $query = $this->db->insert('PROPUESTA_DE_VALOR', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;
    }
    
    function get_propuesta($rut){

        $sql="SELECT * FROM PROPUESTA_DE_VALOR WHERE EMP_RUT = '$rut'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
    }
    
    
    function existe_propuesta($rut){

        $sql="SELECT PV_ID
              FROM PROPUESTA_DE_VALOR
              WHERE '$rut'=EMP_RUT";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return 1; else return 0;

    }
    
    function update_propuesta($data,$rut) {

        $this->db->where('EMP_RUT', $rut);
        $query = $this->db->update('PROPUESTA_DE_VALOR', $data);



        if ($query)
            return true;
        else
            return false;
    }

     function ver_foda($rut)
    {
        $query= $this->db->query("SELECT * FROM FODA WHERE EMP_RUT='$rut'");
        return $query->result();
    }
    
    function insert_foda($data){

        $query = $this->db->insert('FODA', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;
    }

    function existe_foda($rut,$afo_id){

        $sql="SELECT FO_ID
              FROM FODA
              WHERE '$rut'=EMP_RUT AND AFO_ID='$afo_id'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return 1; else return 0;

    }

    function eliminar_foda($id_foda,$atributo)
    {
        $this->db->where('FO_ID',$id_foda);
        $this->db->where('AFO_ID',$atributo);
        $this->db->delete('FODA');

    }
    
     function update_foda($data,$rut,$afo_id,$fo_id) {

        $this->db->where('EMP_RUT', $rut);
        $this->db->where('AFO_ID', $afo_id);
        $this->db->where('FO_ID', $fo_id);
        $query = $this->db->update('FODA', $data);



        if ($query)
            return true;
        else
            return false;
    }
    
     
        function insert_pest($data){

        $query = $this->db->insert('ANALISIS_PEST', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;
    }
    
    
      function get_pest($rut,$id){
        
        $this->db->select('ANP_DESCRIPCION');
        $this->db->from('ANALISIS_PEST');
         $this->db->where('EMP_RUT',$rut);
         $this->db->where('FPE_ID',$id);
        $pest=$this->db->get();
  
        return $pest->row();

    }
    
    function existe_pest($rut,$id){
        $sql="SELECT FPE_ID
              FROM ANALISIS_PEST
              WHERE '$rut'=EMP_RUT AND FPE_ID='$id'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return 1; else return 0;
    }
    
        function update_pest($data,$rut,$fpe_id) {

        $this->db->where('EMP_RUT', $rut);
        $this->db->where('FPE_ID', $fpe_id);
        $query = $this->db->update('ANALISIS_PEST', $data);



        if ($query)
            return true;
        else
            return false;
    }


    

    function consultar_usuario($id_foda,$rut)
    {
        $sql="SELECT * FROM FODA WHERE EMP_RUT = '$rut' AND FO_ID=$id_foda";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
    }

   
    
    
        function insert_4p($data){

        $query = $this->db->insert('ANALISIS_CUATROP', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;
    }
function update_4p($data,$rut,$cup_id) {

        $this->db->where('EMP_RUT', $rut);
        $this->db->where('CUP_ID', $cup_id);
        $query = $this->db->update('ANALISIS_CUATROP', $data);



        if ($query)
            return true;
        else
            return false;
    }

      function get_4p($rut,$id){
        
        $this->db->select('ACP_DESCRIPCION');
        $this->db->from('ANALISIS_CUATROP');
         $this->db->where('EMP_RUT',$rut);
         $this->db->where('CUP_ID',$id);
        $pest=$this->db->get();
  
        return $pest->row();

    }
    
    function existe_4p($rut,$id){
        $sql="SELECT ACP_ID
              FROM ANALISIS_CUATROP
              WHERE '$rut'=EMP_RUT AND CUP_ID='$id'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return 1; else return 0;
    }

    public function insert_obj_est($data)
    {
        $query = $this->db->insert('OBJETIVOS_ESTRATEGICOS', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;
    }
    
      function update_objetivos_estrategicos($data,$oe_id,$rut) {

        $this->db->where('EMP_RUT', $rut);
        $this->db->where('OE_ID', $oe_id);
        $query = $this->db->update('OBJETIVOS_ESTRATEGICOS', $data);



        if ($query)
            return true;
        else
            return false;
    }

    public function existen_objetivos($rut)
    {
        $sql="SELECT OE_ID
              FROM OBJETIVOS_ESTRATEGICOS
              WHERE '$rut'=EMP_RUT";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return 1; else return 0;
    }

    public function objetivos($rut)
    {
        $query= $this->db->query("SELECT * FROM OBJETIVOS_ESTRATEGICOS WHERE EMP_RUT='$rut'");
        return $query->result();
    }
    public function get_descripcionxobjetivo($oe_id)
    {
        $query= $this->db->query("SELECT OE_DESCRIPCION FROM OBJETIVOS_ESTRATEGICOS WHERE OE_ID='$oe_id'");
        return $query->row();
    }

    function consultar_usuario2($id_obj,$rut)
    {
        $sql="SELECT * FROM OBJETIVOS_ESTRATEGICOS WHERE EMP_RUT = '$rut' AND OE_ID=$id_obj";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
    }

    function eliminar_objetivo($id_obj)
    {
        $this->db->where('OE_ID',$id_obj);
        $this->db->delete('OBJETIVOS_ESTRATEGICOS');

    }

    public function insert_fce($data)
    {
        $query = $this->db->insert('FCE', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;
    }
    
         function update_fce($data,$fce_id,$oe_id) {

        $this->db->where('FCE_ID',$fce_id);
        $this->db->where('OE_ID', $oe_id);
        $query = $this->db->update('FCE', $data);



        if ($query)
            return true;
        else
            return false;
    }

    public function existen_fce($id)
    {
        $sql="SELECT FCE_ID
              FROM FCE
              WHERE $id=OE_ID";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return 1; else return 0;
    }

    public function factores($id_obj)
    {
        $query= $this->db->query("SELECT * FROM FCE WHERE OE_ID=$id_obj");
        return $query->result();
    }

    public function eliminar_factor($id)
    {
        $this->db->where('FCE_ID',$id);
        $this->db->delete('FCE');
    }

    function consultar_usuario3($rut,$id_factor)
    {
        $sql="SELECT * FROM FCE F, OBJETIVOS_ESTRATEGICOS O WHERE F.FCE_ID=$id_factor AND F.OE_ID=O.OE_ID AND O.EMP_RUT='$rut'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
    }

    function tiene_factores($id_obj)
    {
        $sql="SELECT * FROM FCE F, OBJETIVOS_ESTRATEGICOS O WHERE O.OE_ID=$id_obj AND O.OE_ID=F.OE_ID";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
    }


}

?>