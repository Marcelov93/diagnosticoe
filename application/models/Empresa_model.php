<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model{

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function existerut($rut){
        $sql = "SELECT EMP_RUT
                FROM EMPRESA
                WHERE EMP_RUT='$rut'";

        $query = $this->db->query($sql);


        if($query->num_rows()>0) return 1; else return 0;
    }

    function insertar($data) {


        $query = $this->db->insert('EMPRESA', $data);



        if ($query)
            return true;
        else
            return false;
    }

    public function mostrarEmpresas($id){

        $query=$this->db->query("SELECT * FROM EMPRESA WHERE USU_ID='$id'");
        return $query->result();
    }
    public function getEmpresas($id){

        $sql=("SELECT * FROM EMPRESA WHERE USU_ID='$id'");
        $query = $this->db->query($sql);
      if($query->num_rows()>0) return $query->row(); else return false;
    }

    public function editEmpresa($rut)
    {
        $query=$this->db->query("SELECT * FROM EMPRESA WHERE EMP_RUT='$rut'");
        return $query->result();
    }

    public function update($id_usu,$rut_empresa,$data){

        $this->db->where('EMP_RUT',$rut_empresa);
        $this->db->where('USU_ID',$id_usu);
       $query = $this->db->update('EMPRESA',$data);
        
        
        if ($query)
            return true;
        else
            return false;
    }


}

