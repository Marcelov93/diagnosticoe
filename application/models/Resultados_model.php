    <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultados_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



function dominios_seleccionados($ana_id){
    
    $query = $this->db->query("SELECT * FROM DOMINIO D, RESULTADOS_POR_DOMINIO R
                             WHERE R.DOM_ID=D.DOM_ID AND R.ANA_ID=$ana_id ");
    return $query->result();
}

        function respuestas_contestadas($ana_id){

            $query = $this->db->query("SELECT * FROM ANALISIS A, RESULTADOS_POR_CRITERIO RC, CRITERIO CR, RESPUESTA R
                                       WHERE A.ANA_ID=$ana_id AND A.ANA_ID=RC.ANA_ID AND RC.CRIT_NUMERO=CR.CRIT_NUMERO
                                       AND RC.ID_RESPUESTA=R.ID_RESPUESTA");
            return $query->result();

        }

    //devuelve el menor id de los dominios seleccionados, para que sea el primero en desarrollar
    function primer_dominio($ana_id){

        $this->db->select_min('RESULTADOS_POR_DOMINIO.DOM_ID');
        $this->db->from('RESULTADOS_POR_DOMINIO');
        $this->db->join('DOMINIO', 'DOMINIO.DOM_ID = RESULTADOS_POR_DOMINIO.DOM_ID');
        $this->db->where('ANA_ID',$ana_id);
        $dom=$this->db->get();
        return $dom->row()->DOM_ID;
    }
    
    function getFecha($ana_id){
        $query= $this->db->query("SELECT * FROM ANALISIS WHERE ANA_ID=$ana_id");
        return $query->result();
        
    }

        function getFecha2($ana_id){
            $query= $this->db->query("SELECT ANA_FECHA FROM ANALISIS WHERE ANA_ID=$ana_id");
            return $query->row()->ANA_FECHA;

        }
    
    function puedeComparar($id){
        $query= $this->db->query("SELECT ANA_ID FROM ANALISIS WHERE USU_ID=$id");
        if ($query->num_rows()>0 && $query->num_rows()<=1){
            $ana_id=$query->row()->ANA_ID;
            $query2=$this->db->query("SELECT ANA_ID FROM RESULTADOS_POR_DOMINIO WHERE ANA_ID=$ana_id AND PUNTAJEXDOMINIO IS NULL");
            if($query2->num_rows()>0) return 0; else return 1;
            
        }else{
            if ($query->num_rows()>0) return 2; else return 0; 
        }
        
    }
//    function puedeComparar($id){
//        $query= $this->db->query("SELECT COUNT(ANA_ID) as num FROM ANALISIS WHERE USU_ID=$id");
//        return $query->row()->num;
//        
//    }

    
    
    public function puntaje_omitido($anaid, $domid){
        
        $query = $this->db->query("SELECT SUM(MAXIMO) AS SUMA
                                    FROM(
                                    SELECT CRIT_NUMERO,MAX(RESP_PONDERACION) AS MAXIMO
                                    FROM RESPUESTA
                                    WHERE CRIT_NUMERO IN 
                                    (SELECT CRIT_NUMERO FROM CRITERIO WHERE DOM_ID=$domid AND CRIT_NUMERO 
                                    NOT IN(
                                            SELECT CRIT_NUMERO
                                            FROM RESULTADOS_POR_CRITERIO 
                                             WHERE ANA_ID = $anaid AND CRIT_NUMERO))
                                    GROUP BY CRIT_NUMERO) AS PONDERACIONES");
        
        return $query->row()->SUMA;
    }
    
    public function get_anaid_inicial($anaid){
        
        
        $query = $this->db->query("SELECT ANA_ID_INICIAL FROM  ANALISIS WHERE ANA_ID=$anaid ");
        
        return $query->row()->ANA_ID_INICIAL;
        
    }
    
    public function continua($ana_id){

        $this->db->select_max('ANA_ID');
        $this->db->from('RESULTADOS_POR_DOMINIO');
        $this->db->where('PUNTAJEXDOMINIO',NULL);
        $this->db->where('ANA_ID',$ana_id);
        $ana=$this->db->get();
        return $ana->row()->ANA_ID;
 
    }
    
    public function analisis_no_terminado($ana_id){
       $query=$this->db->query("SELECT * FROM RESULTADOS_POR_DOMINIO WHERE ANA_ID=$ana_id AND PUNTAJEXDOMINIO IS NULL");
       if ($query->num_rows()>0) return 0; else return 1;
    }
    
    function es_comparable($id_ana1,$id_ana2,$id_user)
    {
        $query=$this->db->query("SELECT * FROM ANALISIS WHERE USU_ID=$id_user AND (ANA_ID_INICIAL=$id_ana1 OR ANA_ID_INICIAL=$id_ana2) AND (ANA_ID=$id_ana1 OR ANA_ID=$id_ana2)");
 
        return $query->result();
    }
    
    function tiene_respuestas($id_ana,$id_dom)
    {
        $query=$this->db->query("SELECT * FROM RESULTADOS_POR_DOMINIO WHERE ANA_ID=$id_ana AND DOM_ID= $id_dom AND PUNTAJEXDOMINIO IS NOT NULL ");
 
       if ($query->num_rows()>0) return 1; else return 0;
    }
    
    

}