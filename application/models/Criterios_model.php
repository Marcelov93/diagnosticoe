<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Criterios_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function mostrar(){

        $query=$this->db->query("SELECT * FROM CRITERIO");
        return $query->result();
    }

    function mostrar_respuestas(){

        $query=$this->db->query("SELECT * FROM RESPUESTA");
        return $query->result();
    }
    
    
    
    function preguntasxdominio($desde, $hasta){
        $query=$this->db->query("SELECT * FROM CRITERIO C, SUBDOMINIO S  WHERE C.CRIT_NUMERO>=$desde AND C.CRIT_NUMERO<=$hasta AND C.SUB_ID=S.SUB_ID");
        return $query->result();
        
    }
    function preguntasxdominioSinsub($desde, $hasta){
        $query=$this->db->query("SELECT * FROM CRITERIO WHERE CRIT_NUMERO>=$desde AND CRIT_NUMERO<=$hasta");
        return $query->result();
        
    }
    
    function respuestasxdominio($desde, $hasta){
        $query=$this->db->query("SELECT * FROM RESPUESTA WHERE CRIT_NUMERO>=$desde AND CRIT_NUMERO<=$hasta");
        return $query->result();
        
    }
    
    
    function resultadosxcriterio($data){
        
        $query = $this->db->insert('RESULTADOS_POR_CRITERIO', $data);
     
        if ($query)
            return $this->db->insert_id();
        else
            return false;    
        
    }

    function resultadosxdominio($ana_id,$dom_id,$puntaje){

  $this->db->set('PUNTAJEXDOMINIO', $puntaje);
  $this->db->where('ANA_ID',$ana_id);
  $this->db->where('DOM_ID',$dom_id);
  $this->db->update('RESULTADOS_POR_DOMINIO');

      }
      
      
 
    function ponderacionxdominio($data){

        $query = $this->db->insert('RESULTADOS_POR_DOMINIO', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;

    }
    
    function inicio_diagnostico($data){
        
             $query = $this->db->insert('ANALISIS', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;
    }
    

    function mostrar_dominios(){

        $query=$this->db->query("SELECT * FROM DOMINIO");
        return $query->result();
    }
    
    
    function getIDanalisis($iduser,$idemp){

        $this->db->select_max('ANA_ID');
        $this->db->from('ANALISIS');
        $this->db->where('EMP_RUT',$idemp);
        $this->db->where('USU_ID',$iduser);
        $ana=$this->db->get();
        return $ana->row()->ANA_ID;
        
    }
    function getana_id($iduser){

        $this->db->select_max('ANA_ID');
        $this->db->from('ANALISIS');
        $this->db->where('USU_ID',$iduser);
        $ana=$this->db->get();
        return $ana->row()->ANA_ID;
        
    }
    
      function getIDempresa($id){


          $this->db->select('EMP_RUT');
          $this->db->from('EMPRESA');
          $this->db->where('USU_ID',$id);
          $rut=$this->db->get();
          return $rut->row()->EMP_RUT;
 

  }
    function mostrar_diagnosticos($iduser)
    {
        $query=$this->db->query("SELECT * FROM ANALISIS WHERE USU_ID=$iduser");
        return $query->result();
    }

    function mostrar_diagnosticos2($iduser)
    {
        $query=$this->db->query("SELECT * FROM ANALISIS WHERE USU_ID=$iduser AND ANA_ID_INICIAL<>'NULL'");
        return $query->result();
    }
    
    function mostrar_diagnosticos3($iduser)
    {
        $query=$this->db->query("SELECT * FROM ANALISIS A WHERE A.USU_ID=$iduser AND A.ANA_ID NOT IN(SELECT ANA_ID  FROM RESULTADOS_POR_DOMINIO R WHERE R.PUNTAJEXDOMINIO IS NULL)");
        return $query->result();
    }
    
    function mostrar_diagnosticos4($iduser)
    {
        $query=$this->db->query("SELECT * FROM ANALISIS A WHERE A.USU_ID=$iduser AND ANA_ID_INICIAL<>'NULL' AND  A.ANA_ID NOT IN(SELECT ANA_ID  FROM RESULTADOS_POR_DOMINIO R WHERE R.PUNTAJEXDOMINIO IS NULL)");
        return $query->result();
    }

    function eliminar_criterios($ana_id)
    {
        $this->db->where('ANA_ID', $ana_id);
        $this->db->delete('RESULTADOS_POR_CRITERIO');

    }

    function eliminar_dominios($ana_id)
    {
        $this->db->where('ANA_ID', $ana_id);
        $this->db->delete('RESULTADOS_POR_DOMINIO');

    }

    function eliminar_analisis($ana_id)
    {
        $this->db->where('ANA_ID', $ana_id);
        $this->db->delete('ANALISIS');


    }

    function consultar_usuario($ana_id,$usu_id)
    {
        $sql="SELECT * FROM ANALISIS WHERE USU_ID = '$usu_id' AND ANA_ID='$ana_id'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
    }
    
    function dom_contestado($ana_id,$dom_id){
   

        
        $this->db->select_max('ANA_ID');
        $this->db->from('RESULTADOS_POR_DOMINIO');
        $this->db->where('PUNTAJEXDOMINIO',NULL);
        $this->db->where('ANA_ID',$ana_id);
        $this->db->where('DOM_ID',$dom_id);
        $ana=$this->db->get();
        return $ana->row()->ANA_ID;


    }

    public function insert_mejoramiento($id_ana,$crit_numero,$accion,$presupuesto,$plazo,$responsable,$terminado)
    {
            $this->db->set('ACCION', $accion);
            $this->db->set('PRESUPUESTO', $presupuesto);
            $this->db->set('PLAZO', $plazo);
            $this->db->set('RESPONSABLE', $responsable);
            $this->db->set('TERMINADO',$terminado);
            $this->db->where('ANA_ID',$id_ana);
            $this->db->where('CRIT_NUMERO',$crit_numero);
            $this->db->update('RESULTADOS_POR_CRITERIO');


    }


    public function existe_plan($ana_id,$crit_numero)
    {
        $sql="SELECT TERMINADO FROM RESULTADOS_POR_CRITERIO WHERE CRIT_NUMERO = $crit_numero AND ANA_ID=$ana_id";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row()->TERMINADO; else return false;

    }

    public function insert_presupuesto($id_ana,$id_dom,$presupuesto)
    {

        $this->db->set('PRESUPUESTOXDOMINIO', $presupuesto);
        $this->db->where('ANA_ID',$id_ana);
        $this->db->where('DOM_ID',$id_dom);
        $this->db->update('RESULTADOS_POR_DOMINIO');


    }

    public function existe_presupuesto($ana_id,$dom_id)
    {
        $sql="SELECT FORMAT(PRESUPUESTOXDOMINIO, 0) AS budget  FROM RESULTADOS_POR_DOMINIO WHERE ANA_ID = $ana_id AND DOM_ID=$dom_id";
        $query = $this->db->query($sql);
        return $query->row()->budget;

    }

    public function existe_presupuesto2($ana_id)
    {
        $sql="SELECT ANA_ID FROM RESULTADOS_POR_DOMINIO WHERE ANA_ID = $ana_id AND PRESUPUESTOXDOMINIO>0";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row()->ANA_ID; else return false;

    }

    public function presupuesto_total($ana_id)
    {
        $sql="SELECT FORMAT(SUM(PRESUPUESTOXDOMINIO), 0) AS suma  FROM RESULTADOS_POR_DOMINIO WHERE ANA_ID = $ana_id AND PRESUPUESTOXDOMINIO>0";
        $query = $this->db->query($sql);
        return $query->row()->suma;
    }
    
    public function acciones_informe($ana_id,$inicio,$fin)
    {
        $query=$this->db->query("SELECT * FROM RESULTADOS_POR_CRITERIO R, CRITERIO CR WHERE R.CRIT_NUMERO=CR.CRIT_NUMERO AND  R.ACCION IS NOT NULL AND R.ANA_ID = $ana_id AND R.CRIT_NUMERO BETWEEN $inicio AND $fin");
        return $query->result();
        
    }
    
    
}