<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Usuarios_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
  
    function login($correo,$password){
        
        
        
        $this->db->select('USU_RUT,USU_NOMBRES,USU_CORREO,USU_ID');
        
        $this->db->where('USU_CORREO', $correo);
        $this->db->where('USU_PASSWORD', $password);
        $this->db->where('USU_ESTADO', 1);
        $query = $this->db->get('USUARIO');
        

        if($query->num_rows()>0) return $query->row(); else return false;
    }
    
    
    function check_pass($id,$password){
        
        $this->db->select('USU_RUT');
        $this->db->where('USU_ID',$id);
        $this->db->where('USU_PASSWORD',$password);
        $this->db->where('USU_ESTADO', 1);
        $query = $this->db->get('USUARIO');
       
        
        if($query->num_rows()>0) return 1; else return false;
    }
    
    
    
    function existerut($rut){
        $sql = "SELECT USU_RUT
                FROM USUARIO 
                WHERE USU_ESTADO<>0 AND USU_RUT='$rut'";
        
                $query = $this->db->query($sql);
        
        
        if($query->num_rows()>0) return 1; else return 0;
    }

    function existecorreo($correo){
        $sql = "SELECT USU_RUT
                FROM USUARIO
                WHERE USU_ESTADO<>0 AND USU_CORREO='$correo'";

        $query = $this->db->query($sql);


        if($query->num_rows()>0) return 1; else return 0;
    }
    function existecorreo2($correo,$actual){
        $sql = "SELECT USU_RUT
                FROM USUARIO
                WHERE USU_ESTADO<>0 AND USU_CORREO<>'$actual' AND USU_CORREO='$correo'";

        $query = $this->db->query($sql);


        if($query->num_rows()>0) return 1; else return 0;
    }

    
      function insertar($data) {
          
         
        $query = $this->db->insert('USUARIO', $data);
 
        
        
        if ($query)
            return $this->db->insert_id();
        else
            return false;           
  }
  
  
  public function insertarInfoAdicional ($data){
               
             $query = $this->db->insert('OTROS_ANTECEDENTES', $data);

        if ($query)
            return $this->db->insert_id();
        else
            return false;    
      
      
  }
  
  function getUsuarioxid($id){
      
      $sql="SELECT * FROM USUARIO WHERE USU_ID = '$id'";
      $query = $this->db->query($sql);
      if($query->num_rows()>0) return $query->row(); else return false;
  }
  
  function existe_correo($correo){
      
      $sql="SELECT * FROM USUARIO WHERE USU_CORREO = '$correo'";
      $query = $this->db->query($sql);
      if($query->num_rows()>0) return 1; else return 0;
  }
  function getdata_correo($correo){
      
      $sql="SELECT * FROM USUARIO WHERE USU_CORREO = '$correo'";
      $query = $this->db->query($sql);
      if($query->num_rows()>0) return $query->row(); else return false;
  }
  
  
    function get_otrosantecedentes($id){
      
      $sql="SELECT * FROM OTROS_ANTECEDENTES WHERE USU_ID = '$id'";
      $query = $this->db->query($sql);
      if($query->num_rows()>0) return $query->row(); else return false;
  }
  
  function actualizar($data,$id) {
          
         $this->db->where('USU_ID', $id);
        $query = $this->db->update('USUARIO', $data);
 
        
        
        if ($query)
            return true;
        else
            return false;           
  }

    function actualizar_infoadicional($data,$id) {
          
         $this->db->where('USU_ID', $id);
        $query = $this->db->update('OTROS_ANTECEDENTES', $data);
 
        
        
        if ($query)
            return true;
        else
            return false;           
  }
  
    function existe_empresa($id_usu){

        $sql="SELECT USU_ID
              FROM EMPRESA
              WHERE '$id_usu'=USU_ID";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
    }
    
    function existe_empresa_y_analisis($id_usu){

$this->db->select('ANALISIS.ANA_ID');
$this->db->from('EMPRESA');
$this->db->where('EMPRESA.USU_ID', $id_usu);
$this->db->join('ANALISIS', 'ANALISIS.EMP_RUT = EMPRESA.EMP_RUT');


$query = $this->db->get();
        if($query->num_rows()>0) return 1; else return 0;
    }
    
    function otros_antecedentes($id){
        
        $sql="SELECT OAN_ID
              FROM OTROS_ANTECEDENTES
              WHERE '$id'=USU_ID";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return 1; else return 0;
        
    }
    function get_otros_antecedentes($id){
        
        $sql="SELECT *
              FROM OTROS_ANTECEDENTES
              WHERE '$id'=USU_ID";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
        
    }
    function get_user_data($id){
        
        $sql="SELECT *
              FROM USUARIO
              WHERE '$id'=USU_ID";
        $query = $this->db->query($sql);
        if($query->num_rows()>0) return $query->row(); else return false;
        
    }
  


  
} 

?>