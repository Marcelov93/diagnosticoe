<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('Usuarios_model');
        $this->load->library('session');
//        $this->load->library('email');
        $this->load->helper('url');
        date_default_timezone_set('America/Santiago');
    }

    public function index() {
        $config['activo'] = 'inicio'; //para el class active
        $config['img'] = '2'; // para el tamaño de la imagen del banner 0 es full, 1 esta cortada,2 sin banner
        $config['animacion'] = '1'; //la animacion del menu que solo aparece en el inicio
        
        if($this->session->userdata('normal')){
            redirect('usuarios/perfil');
        }
        else{
            $this->load->view('estructura/menu_superior',$config);
                $this->load->view('nav/login');
        }

    }


    public function login() {
        $config['activo'] = 'login';
        $config['img'] = '2';
        $config['animacion'] = '0';
       $this->load->view('estructura/header');
                if($this->session->userdata('normal')){
            $this->load->view('estructura/menu_superior_usuario',$config);
        }
        else{
        $this->load->view('estructura/menu_superior',$config);
        }
        $this->load->view('nav/login');
    }

    public function info() {
        $config['activo'] = 'info';
        $config['img'] = '2';
        $config['animacion'] = '0';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
            $this->load->view('estructura/menu_superior_usuario',$config);
        }
        else{
            $this->load->view('estructura/menu_superior',$config);
        }
        $this->load->view('nav/info');
    }
    
    
    
    
    
    
    public function recibirdatos() {
        if ($_POST) {
            $correo = $this->input->post('correo');
            $password = sha1($this->input->post("password"));

            if ($usuario = $this->Usuarios_model->login($correo, $password)) {

                $iduser = $usuario->USU_ID;
                $rut = $usuario->USU_RUT;
                $nombres = $usuario->USU_NOMBRES;
                $correo = $usuario->USU_CORREO;


                $datosUsario = (object) array(
                            'IDUSER' => $iduser,
                            'NOMBRES' => $nombres,
                            'RUT' => $rut,
                            'CORREO' => $correo,
                );

                $this->session->set_userdata('normal', $datosUsario);

                if($this->Usuarios_model->existe_empresa($iduser))
                    redirect('Usuarios/perfil');
                else{
                    redirect('Empresa/registro');
                }

            } else {

                $this->session->set_flashdata('registrostatus', 'Correo o contraseña incorrecta');
                redirect('Welcome/login');
            }
        } else {
            redirect('Welcome');
        }
    }

    public function salir() {
        $this->session->sess_destroy();

        redirect('Welcome');
    }
    
    

}
