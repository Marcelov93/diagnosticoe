<?php


class TestResultados extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Resultados_model');
        $this->load->library('unit_test');
        date_default_timezone_set('America/Santiago');
    }

    public function dominios_seleccionados()
    {
        $id_ana=137;

        $dominios=$this->Resultados_model->dominios_seleccionados($id_ana);

        echo $this->unit->run($dominios,'is_array');
    }

    public function respuestas_contestadas()
    {
        $id_ana=150;

        $respuestas=$this->Resultados_model->respuestas_contestadas($id_ana);

        echo $this->unit->run($respuestas,'is_array');
    }

    public function primer_dominio()
    {
        $id_ana=137;

        $dominio=$this->Resultados_model->primer_dominio($id_ana);

        echo $this->unit->run($dominio,'is_string');
    }

    public function get_fecha()
    {
        $id_ana=157;

        $fecha=$this->Resultados_model->getFecha2($id_ana);

        echo $this->unit->run($fecha,'is_string');
    }

    public function puede_comparar()
    {
        $id_user=9;

        $analisis=$this->Resultados_model->puedeComparar($id_user);

        echo $this->unit->run($analisis,'is_string');
    }

    public function puntaje_omitido()
    {
        $id_ana=73;
        $id_dom=6;

        $puntaje=$this->Resultados_model->puntaje_omitido($id_ana,$id_dom);

        echo $this->unit->run($puntaje,'is_string');
    }

    public function get_anaid_inicial()
    {
        $id_ana=93;

        $analisis=$this->Resultados_model->get_anaid_inicial($id_ana);

        echo $this->unit->run($analisis,'is_string');
    }

    public function continuar_diagnostico()
    {
        $id_ana=137;

        $analisis=$this->Resultados_model->continua($id_ana);

        echo $this->unit->run($analisis,'is_string');
    }







}