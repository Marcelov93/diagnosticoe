<?php


class TestEstrategia extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Estrategia_model');
        $this->load->library('unit_test');
        date_default_timezone_set('America/Santiago');
    }

    public function existe_mv()
    {
        $rut_emp='21.165.833-9';

        $mv=$this->Estrategia_model->existe_mv($rut_emp);

        echo $this->unit->run($mv,1);
    }

    public function get_mv()
    {
        $rut_emp='21.165.833-9';

        $mv=$this->Estrategia_model->get_mision_vision($rut_emp);

        echo $this->unit->run($mv,'is_object');
    }

    public function get_propuesta()
    {
        $rut_emp='21.165.833-9';

        $propuesta=$this->Estrategia_model->get_propuesta($rut_emp);

        echo $this->unit->run($propuesta,'is_object');
    }

    public function existe_propuesta()
    {
        $rut_emp='21.165.833-9';

        $propuesta=$this->Estrategia_model->existe_propuesta($rut_emp);

        echo $this->unit->run($propuesta,1);
    }

    public function existe_foda()
    {
        $rut_emp='21.165.833-9';
        $atributo='FORTALEZA';

        $foda=$this->Estrategia_model->existe_foda($rut_emp,$atributo);

        echo $this->unit->run($foda,1);
    }

    public function get_pest()
    {
        $rut_emp='21.165.833-9';
        $factor='POLITICO';

        $pest=$this->Estrategia_model->get_pest($rut_emp,$factor);

        echo $this->unit->run($pest,'is_object');
    }

    public function existe_pest()
    {
        $rut_emp='21.165.833-9';
        $factor='POLITICO';

        $pest=$this->Estrategia_model->existe_pest($rut_emp,$factor);

        echo $this->unit->run($pest,1);
    }

    public function get_4p()
    {
        $rut_emp='21.165.833-9';
        $factor='PRECIO';

        $p=$this->Estrategia_model->get_4p($rut_emp,$factor);

        echo $this->unit->run($p,'is_object');
    }

    public function existe_4p()
    {
        $rut_emp='21.165.833-9';
        $factor='PRECIO';

        $p=$this->Estrategia_model->existe_4p($rut_emp,$factor);

        echo $this->unit->run($p,1);
    }

    public function existen_objetivos()
    {
        $rut_emp='21.165.833-9';

        $objetivo=$this->Estrategia_model->existen_objetivos($rut_emp);

        echo $this->unit->run($objetivo,1);
    }

    public function get_objetivo()
    {
        $id=19;

        $objetivo=$this->Estrategia_model->get_descripcionxobjetivo($id);

        echo $this->unit->run($objetivo,'is_object');
    }

    public function existen_fce()
    {
        $id=20;

        $fce=$this->Estrategia_model->existen_fce($id);

        echo $this->unit->run($fce,1);
    }

    public function insert_mv()
    {
        $datosinsert = array(
            'EMP_RUT' => '9.716.910-1',
            'MV_MISION' => 'mision',
            'MV_VISION' => 'vision',


        );

        $insertarinfo= $this->Estrategia_model->insert_mision_vision($datosinsert);

        echo $this->unit->run($insertarinfo,'is_int');

    }

    public function insert_propuesta()
    {
        $datosinsert = array(
            'EMP_RUT' => '9.716.910-1',
            'PV_PROPUESTA' => 'propuesta',
        );

        $insertarinfo= $this->Estrategia_model->insert_propuesta($datosinsert);

        echo $this->unit->run($insertarinfo,'is_int');
    }

    public function insert_foda()
    {
        $datosinsert = array(
            'EMP_RUT' => '9.716.910-1',
            'AFO_ID' => 'FORTALEZA',
            'FO_DESCRIPCION' => 'fortaleza',
        );

        $insertarinfo= $this->Estrategia_model->insert_foda($datosinsert);

        echo $this->unit->run($insertarinfo,'is_int');
    }

    public function insert_pest()
    {
        $datosinsert = array(
            'FPE_ID' =>'POLITICO',
            'EMP_RUT' => '9.716.910-1',
            'ANP_DESCRIPCION' => 'factor politico',
        );

        $insertarinfo= $this->Estrategia_model->insert_pest($datosinsert);

        echo $this->unit->run($insertarinfo,'is_int');
    }

    public function insert_4p()
    {
        $datosinsert = array(
            'EMP_RUT' => '9.716.910-1',
            'CUP_ID' =>'PRECIO',
            'ACP_DESCRIPCION' => 'precio',
        );

        $insertarinfo= $this->Estrategia_model->insert_4p($datosinsert);

        echo $this->unit->run($insertarinfo,'is_int');
    }

    public function insert_obj()
    {
        $datosinsert = array(
            'EMP_RUT' => '9.716.910-1',
            'OE_DESCRIPCION' => 'obj estrategico',

        );

        $insertarinfo= $this->Estrategia_model->insert_obj_est($datosinsert);

        echo $this->unit->run($insertarinfo,'is_int');
    }

    public function insert_fce()
    {
        $datosinsert = array(
            'OE_ID' => 21,
            'FCE_FACTOR' => 'factor critico',

        );

        $insertarinfo= $this->Estrategia_model->insert_fce($datosinsert);

        echo $this->unit->run($insertarinfo,'is_int');

    }






}