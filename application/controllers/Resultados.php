<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Resultados extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('Resultados_model');
        $this->load->model('Criterios_model');
        $this->load->model('Usuarios_model');
        $this->load->model('Empresa_model');
        $this->load->library('session');
        $this->load->library('Pdf');
        $this->load->helper('url');
        date_default_timezone_set('America/Santiago');

    }

    public function index()
    {


        redirect('Usuarios/menu_diagnostico');

    }

    public function results(){

        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        if($this->session->userdata('normal')){
            $id = $this->session->userdata('normal')->IDUSER;
            $emp_rut = $this->Criterios_model->getIDempresa($id);
            $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
            if ($this->Usuarios_model->existe_empresa_y_analisis($id) && $this->Resultados_model->analisis_no_terminado($ana_id)){
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);
           


        $data['idana']=$ana_id;
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        $this->load->view('resultados/results',$data);
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
            
    }
    
        public function pdf_resultados($ana_id)
    {
        
        if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id)){
        $data['idana']=$ana_id;
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        
 $data['empresario'] = $this->Usuarios_model->getUsuarioxId($id);
        $data['empresa'] = $this->Empresa_model->getEmpresas($id);
        $data['fecha'] = $this->Resultados_model->getFecha2($ana_id);
  
        $this->load->view('resultados/pdf_resultados',$data);
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }
        public function pdf_reevaluacion($idana,$idana2)
    {
        
      {
        if($idana && $idana2){

        if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana,$idana2,$id) && $this->Resultados_model->analisis_no_terminado($idana)&& $this->Resultados_model->analisis_no_terminado($idana2)){
           
        $data['empresario'] = $this->Usuarios_model->getUsuarioxId($id);
        $data['empresa'] = $this->Empresa_model->getEmpresas($id);
        $data['idana1']=$idana;
        $data['idana2']=$idana2;
        $data['dominios1']=$this->Resultados_model->dominios_seleccionados($idana);
        $data['dominios2']=$this->Resultados_model->dominios_seleccionados($idana2);;
        $data['fecha1']=$this->Resultados_model->getFecha2($idana);
        $data['fecha2']=$this->Resultados_model->getFecha2($idana2);
        $this->load->view('resultados/pdf_reevaluacion',$data);

    }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
    }
    }

    public function diagnosticos()
    {
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        if($this->session->userdata('normal')){
            $id = $this->session->userdata('normal')->IDUSER;
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
        
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

       
        $ana_id=$this->Criterios_model->getana_id($id);
       $data['continua']=$this->Resultados_model->continua($ana_id);

        $data['diagnosticos']=$this->Criterios_model->mostrar_diagnosticos($id);

        $this->load->view('resultados/diagnosticos',$data);
    }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }

    public function ver_resultado($ana_id)
    {
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        if($this->session->userdata('normal')){
        $id=$this->session->userdata('normal')->IDUSER;
        if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id)){
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        $this->load->view('resultados/results',$data);
    }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }
//    
//    public function result_pdf()
//    {
//        $ana_id=4;
//        $data['idana']=$ana_id;
//        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
//        $this->load->view('resultados/result_pdf',$data);
//    }

    public function seleccionar_dominio($ana_id)
    {
        if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id)){
        
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        $this->load->view('resultados/seleccionar_dominio',$data);
         }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }

    }

    public function ver_respuestas1($ana_id)
    {
        
          if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,1)){
        
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);


        $data['lista']=$this->Criterios_model->preguntasxdominio(1,10);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(1, 10);
        $this->load->view('resultados/respuestas',$data);
    
       }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
}

    public function ver_respuestas2($ana_id)
    {
                  if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false   && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,2)){
        
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);


        $data['lista']=$this->Criterios_model->preguntasxdominio(11,33);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(11,33);
        $this->load->view('resultados/respuestas2',$data);
               }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas3($ana_id)
    {
        
        
              if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false   && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,3)){
    
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);


        $data['lista']=$this->Criterios_model->preguntasxdominio(34,53);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(34,53);
        $this->load->view('resultados/respuestas3',$data);
               }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas4($ana_id)
    {
        
                  if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false   && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,4)){
  
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);


        $data['lista']=$this->Criterios_model->preguntasxdominio(54,79);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(54,79);
        $this->load->view('resultados/respuestas4',$data);
               }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas5($ana_id)
    {
        
              if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false   && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,5)){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(80,86);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(80,86);
        $this->load->view('resultados/respuestas5',$data);
               }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas6($ana_id)
    {
        
              if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false   && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,6)){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);


        $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(87,92);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(87,92);
        $this->load->view('resultados/respuestas6',$data);
               }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function comparar()
    {
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);
        if($this->session->userdata('normal')){
        $id = $this->session->userdata('normal')->IDUSER;

             if ($this->Usuarios_model->existe_empresa_y_analisis($id) && $this->Criterios_model->mostrar_diagnosticos4($id)){
        
        $data['diagnosticos']=$this->Criterios_model->mostrar_diagnosticos4($id);

        $this->load->view('resultados/comparar',$data);
    }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }
    
    public function comparar_diagnosticos($idana,$idana2)
    {
        if($idana && $idana2){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana,$idana2,$id) && $this->Resultados_model->analisis_no_terminado($idana)&& $this->Resultados_model->analisis_no_terminado($idana2)){
           
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);
        $data['idana1']=$idana;
        $data['idana2']=$idana2;
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($idana);
        $data['dominios2']=$this->Resultados_model->dominios_seleccionados($idana2);;
        $data['fecha1']=$this->Resultados_model->getFecha($idana);
        $data['fecha2']=$this->Resultados_model->getFecha($idana2);
        $this->load->view('resultados/comparar_diagnosticos',$data);

    }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
    }
    
        public function medicion_impacto($idana1,$idana2)
    {
            
            if($idana1 && $idana2){
            
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

         if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana1,$idana2,$id) && $this->Resultados_model->analisis_no_terminado($idana1)&& $this->Resultados_model->analisis_no_terminado($idana2)){
        
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);
        
        $data['idana1']=$idana1;
        $data['idana2']=$idana2;
        $data['dominios1']=$this->Resultados_model->dominios_seleccionados($idana1);
        $data['dominios2']=$this->Resultados_model->dominios_seleccionados($idana2);

        $this->load->view('resultados/medicion_impacto',$data);
            }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
    }

    public function diagnosticos2()
    {
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

         if($this->session->userdata('normal')){
         $id = $this->session->userdata('normal')->IDUSER;
         
         if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

       

        $data['diagnosticos']=$this->Criterios_model->mostrar_diagnosticos3($id);

        $this->load->view('resultados/diagnosticos2',$data);
    }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }
    
        public function comparacion_criterios_DOM1($idana1,$idana2){
        
            if($idana1 && $idana2){
            
           if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana1,$idana2,$id) && $this->Resultados_model->analisis_no_terminado($idana1)&& $this->Resultados_model->analisis_no_terminado($idana2)){     
                
                       
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['lista']=$this->Criterios_model->preguntasxdominio(1,10);
        $data['respuestas1']=$this->Resultados_model->respuestas_contestadas($idana1);
        $data['respuestas2']=$this->Resultados_model->respuestas_contestadas($idana2);
        $data['dominio']='Gobierno empresarial y estrategia';
        $this->load->view('resultados/comparacion_criterios',$data);
        
     }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
        }
    
    
        public function comparacion_criterios_DOM2($idana1,$idana2){
        
                    if($idana1 && $idana2){
            
           if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana1,$idana2,$id) && $this->Resultados_model->analisis_no_terminado($idana1)&& $this->Resultados_model->analisis_no_terminado($idana2)){      
            
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);


        $data['lista']=$this->Criterios_model->preguntasxdominio(11,33);
        $data['respuestas1']=$this->Resultados_model->respuestas_contestadas($idana1);
        $data['respuestas2']=$this->Resultados_model->respuestas_contestadas($idana2);
        $this->load->view('resultados/comparacion_criterios_DOM2',$data);
        
             }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
        }
        
        public function comparacion_criterios_DOM3($idana1,$idana2){
        
                        if($idana1 && $idana2){
            
           if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana1,$idana2,$id) && $this->Resultados_model->analisis_no_terminado($idana1)&& $this->Resultados_model->analisis_no_terminado($idana2)){  
            
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

 
        $data['lista']=$this->Criterios_model->preguntasxdominio(34,53);
        $data['respuestas1']=$this->Resultados_model->respuestas_contestadas($idana1);
        $data['respuestas2']=$this->Resultados_model->respuestas_contestadas($idana2);
        $this->load->view('resultados/comparacion_criterios_DOM3',$data);
        
             }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
        
        }
        
        public function comparacion_criterios_DOM4($idana1,$idana2){
            
            
                  if($idana1 && $idana2){
            
           if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana1,$idana2,$id) && $this->Resultados_model->analisis_no_terminado($idana1)&& $this->Resultados_model->analisis_no_terminado($idana2)){  
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

      
        $data['lista']=$this->Criterios_model->preguntasxdominio(54,79);
        $data['respuestas1']=$this->Resultados_model->respuestas_contestadas($idana1);
        $data['respuestas2']=$this->Resultados_model->respuestas_contestadas($idana2);
        $this->load->view('resultados/comparacion_criterios_DOM4',$data);
        
             }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
        }
        
        //DOM_5 y DOM6 utilizan la misma vista del DOM1 porque es igual,
        //solo cambia el nombre del dominio
        
        public function comparacion_criterios_DOM5($idana1,$idana2){
            
                 if($idana1 && $idana2){
            
           if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana1,$idana2,$id )&& $this->Resultados_model->analisis_no_terminado($idana1)&& $this->Resultados_model->analisis_no_terminado($idana2)){  
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);


        $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(80,86);
        $data['respuestas1']=$this->Resultados_model->respuestas_contestadas($idana1);
        $data['respuestas2']=$this->Resultados_model->respuestas_contestadas($idana2);
        $data['dominio']='Procesos';
        $this->load->view('resultados/comparacion_criterios',$data);
        
             }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
        }
        
        public function comparacion_criterios_DOM6($idana1,$idana2){
            
                 if($idana1 && $idana2){
            
           if($this->session->userdata('normal')){
        
           $id= $this->session->userdata('normal')->IDUSER;
            
            if($this->Resultados_model->es_comparable($idana1,$idana2,$id) && $this->Resultados_model->analisis_no_terminado($idana1)&& $this->Resultados_model->analisis_no_terminado($idana2)){  
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(87,92);
        $data['respuestas1']=$this->Resultados_model->respuestas_contestadas($idana1);
        $data['respuestas2']=$this->Resultados_model->respuestas_contestadas($idana2);
        $data['dominio']='Medición, análisis y desempeño';
        $this->load->view('resultados/comparacion_criterios',$data);
        
         }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
        redirect('Welcome');
    }
    }else{
         redirect('Usuarios/menu_diagnostico');
    }
    
}

        public function diagnosticos_evaluacion_posterior(){
            
        if($this->session->userdata('normal')){    
            $id = $this->session->userdata('normal')->IDUSER;
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        
        $ana_id=$this->Criterios_model->getana_id($id);
       $data['continua']=$this->Resultados_model->continua($ana_id);
        $data['diagnosticos']=$this->Criterios_model->mostrar_diagnosticos($id);

        $this->load->view('resultados/diagnosticos_evaluacion_posterior',$data);
            
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
        }
        
        public function evaluacion_posterior($ana_id){
        
         if($ana_id){   
            
             if($this->session->userdata('normal')){    
            $id = $this->session->userdata('normal')->IDUSER;
            if ($this->Usuarios_model->existe_empresa_y_analisis($id) && $this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id)){
             
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        $data['fecha_id']=$this->Resultados_model->getFecha2($ana_id);
        $data['ana_id']=$ana_id;
        

        $this->load->view('resultados/evaluacion_posterior',$data);
            
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
        
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        
        }

}