<?php
class TestEmpresa extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Empresa_model');
        $this->load->library('unit_test');
        date_default_timezone_set('America/Santiago');
    }

    public function existe_rut()
    {
        $rut='21.165.833-9';

        $empresa=$this->Empresa_model->existerut($rut);

        echo $this->unit->run($empresa,1, 'existe rut empresa');
    }

    /*Test de Integración */

    public function insertar()
    {
        $datosinsert = array(
            'USU_ID' => 16,
            'EMP_RUT' => '21221473-6',
            'EMP_RAZON_SOCIAL' => 'futbol',
            'EMP_NOMBRE_FANTASIA' => 'futbol fc',
            'EMP_RUBRO'            => 'deporte',
            'EMP_TELEFONO'         => '433454454',
            'EMP_CORREO'           => 'futbolfc@mail.com',
            'EMP_PAGINA_WEB'       => 'futbolpasion.com',
            'EMP_ANTIGUEDAD'       => 2,
            'EMP_VTASMENSUALESPROM' => 200,
            'EMP_CTOSMENSUALESPROM' => 100,
            'EMP_UTILIDADMENSUALPROM' => 100,
            'EMP_NROEMPLEADOS'     => 10,

        );

        $insertarempresa = $this->Empresa_model->insertar($datosinsert);

        echo $this->unit->run($insertarempresa,'is_true');


    }

    public function update()
    {
        $id_usu=16;
        $rut_empresa='21221473-6';

        $datos= array(
            'EMP_RAZON_SOCIAL' => 'futbol y mas',
            'EMP_NOMBRE_FANTASIA' => 'futbol fc',
            'EMP_RUBRO'            => 'deporte',
            'EMP_TELEFONO'         => '433454454',
            'EMP_CORREO'           => 'futbolfc@mail.com',
            'EMP_PAGINA_WEB'       => 'futbolpasion.com',
            'EMP_ANTIGUEDAD'       => 2,
            'EMP_VTASMENSUALESPROM' => 200,
            'EMP_CTOSMENSUALESPROM' => 100,
            'EMP_UTILIDADMENSUALPROM' => 100,
            'EMP_NROEMPLEADOS'     => 10,
        );
        $actualizarinfo=$this->Empresa_model->update($id_usu,$rut_empresa,$datos);

        echo $this->unit->run($actualizarinfo,'is_true');
    }

}