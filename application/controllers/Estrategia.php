<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estrategia extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('Usuarios_model');
        $this->load->model('Resultados_model');
        $this->load->model('Criterios_model');
        $this->load->model('Estrategia_model');
        $this->load->model('Empresa_model');
        $this->load->library('session');
//        $this->load->library('email');
        $this->load->helper('url');
        date_default_timezone_set('America/Santiago');
    }

    public function index(){

  redirect('Estrategia/menu');


    }


    public function menu(){

        $config['activo'] = 'estrategia';
        $config['img'] = '9';
        $config['animacion'] = '1';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
               if ($this->Usuarios_model->existe_empresa($id)){
            
            $this->load->view('estructura/menu_superior_usuario',$config);   
            $rut=$this->Criterios_model->getIDempresa($id);
            $data['propuesta']=$this->Estrategia_model->get_propuesta($rut); 
            $data['mv']=$this->Estrategia_model->existe_mv($rut);
            $data['objetivos']=$this->Estrategia_model->existen_objetivos($rut);
            $this->load->view('estrategia/menu', $data);
        
        }else{
            redirect('Empresa/registro');
        }
        }else{
            redirect('Welcome/login');
        }

    }

    public function mision_vision()
    {
        $config['activo'] = 'estrategia';
        $config['img'] = '9';
        $config['animacion'] = '1';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
            
              if ($this->Usuarios_model->existe_empresa($id)){
            $this->load->view('estructura/menu_superior_usuario',$config);
            
            $rut=$this->Criterios_model->getIDempresa($id);
            $data['datos']=$this->Estrategia_model->get_mision_vision($rut);
            $data['existe']=$this->Estrategia_model->existe_mv($rut);
           
            $this->load->view('estrategia/mision_vision',$data);
        }
        else{
            redirect('Empresa/registro');
        }
        }
        else{
            redirect('Welcome/login');
        }
    }

    public function insertar_mision_vision()
    {
        if ($_POST) {

if($this->session->userdata('normal')){
            $mision = $this->input->post('mision');
            $vision = $this->input->post('vision');

            $id= $this->session->userdata('normal')->IDUSER;

            $rut=$this->Criterios_model->getIDempresa($id);

            $datosinsert = array(
                'EMP_RUT' => $rut,
                'MV_MISION' => $mision,
                'MV_VISION' => $vision,


            );
            $datosinsert = $this->security->xss_clean($datosinsert);

            $insertarinfo= $this->Estrategia_model->insert_mision_vision($datosinsert);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/menu');
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }


    public function editar_mision_vision()
    {
        if ($_POST) {

if($this->session->userdata('normal')){
            $mision = $this->input->post('mision');
            $vision = $this->input->post('vision');

            $id= $this->session->userdata('normal')->IDUSER;

            $rut=$this->Criterios_model->getIDempresa($id);

            $datosinsert = array(
                'EMP_RUT' => $rut,
                'MV_MISION' => $mision,
                'MV_VISION' => $vision,


            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->actualizar_mision_vision($datosinsert,$rut);




            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/menu');
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }
    
        public function propuesta_de_valor()
    {
        $config['activo'] = 'estrategia';
        $config['img'] = '9';
        $config['animacion'] = '1';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
            
            $id = $this->session->userdata('normal')->IDUSER;
            
            if ($this->Usuarios_model->existe_empresa($id)){
            
            $this->load->view('estructura/menu_superior_usuario',$config);

            
            $rut=$this->Criterios_model->getIDempresa($id);
            $data['propuesta']=$this->Estrategia_model->get_propuesta($rut);
            $data['existe']=$this->Estrategia_model->existe_propuesta($rut);
            $this->load->view('estrategia/propuesta_de_valor',$data);
        }
        else{
            redirect('Empresa/registro');
        }
        }
        else{
            redirect('Welcome/login');
        }
    }
    
    public function insertar_propuesta_de_valor()
    {
        if ($_POST) {

 if($this->session->userdata('normal')){
            $propuesta = $this->input->post('propuesta');
            

            $id= $this->session->userdata('normal')->IDUSER;

            $emp_rut=$this->Criterios_model->getIDempresa($id);

            $datosinsert = array(
                'EMP_RUT' => $emp_rut,
                'PV_PROPUESTA' => $propuesta,
            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->insert_propuesta($datosinsert);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/menu');
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }

        } else {
            redirect('Estrategia/menu');
        }
    }
    

     public function update_propuesta()
    {
        if ($_POST) {

 if($this->session->userdata('normal')){
            $propuesta = $this->input->post('propuesta');
  
            $id= $this->session->userdata('normal')->IDUSER;

            $emp_rut=$this->Criterios_model->getIDempresa($id);

             $datosinsert = array(

                'PV_PROPUESTA' => $propuesta,
            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->update_propuesta($datosinsert,$emp_rut);




            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Actualizar');
                redirect('Estrategia/menu');
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/menu');
            }

        } else {
            redirect('Welcome');
        }

        } else {
            redirect('Estrategia/menu');
        }
    }

    public function insertar_foda()
    {
        if ($_POST) {

 if($this->session->userdata('normal')){
            if($this->input->post('fortaleza')){
            $descripcion = $this->input->post('fortaleza');
            $afo_id='FORTALEZA';
            }else{
                 if($this->input->post('debilidad')){
                     $descripcion = $this->input->post('debilidad');
                     $afo_id='DEBILIDAD';
                 }else{
                     if($this->input->post('oportunidad')){
                         $descripcion = $this->input->post('oportunidad');
                     $afo_id='OPORTUNIDAD';
                     } else{
                         if($this->input->post('amenaza')){
                             $descripcion = $this->input->post('amenaza');
                             $afo_id='AMENAZA';
                         }
                     }
                 }
            }

            $id= $this->session->userdata('normal')->IDUSER;

            $rut=$this->Criterios_model->getIDempresa($id);

            $datosinsert = array(
                'EMP_RUT' => $rut,
                'AFO_ID' => $afo_id,
                'FO_DESCRIPCION' => $descripcion,


            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->insert_foda($datosinsert);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/ver_foda');
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }
    public function editar_foda()
    {
        if ($_POST) {

 if($this->session->userdata('normal')){
            if($this->input->post('fortaleza') && $this->input->post('fo_id')){
            $descripcion = $this->input->post('fortaleza');
            $fo_id = $this->input->post('fo_id');
            $afo_id='FORTALEZA';
            }else{
                 if($this->input->post('debilidad') && $this->input->post('fo_id')){
                     $descripcion = $this->input->post('debilidad');
                     $fo_id = $this->input->post('fo_id');
                     $afo_id='DEBILIDAD';
                 }else{
                     if($this->input->post('oportunidad') && $this->input->post('fo_id')){
                         $descripcion = $this->input->post('oportunidad');
                         $fo_id = $this->input->post('fo_id');
                        $afo_id='OPORTUNIDAD';
                     } else{
                         if($this->input->post('amenaza') && $this->input->post('fo_id')){
                             $descripcion = $this->input->post('amenaza');
                             $fo_id = $this->input->post('fo_id');
                             $afo_id='AMENAZA';
                         }
                     }
                 }
            }

            $id= $this->session->userdata('normal')->IDUSER;

            $rut=$this->Criterios_model->getIDempresa($id);

            $datosinsert = array(
                'FO_DESCRIPCION' => $descripcion,


            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->update_foda($datosinsert,$rut,$afo_id,$fo_id);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/ver_foda');
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }

    

     
    

    public function eliminar_foda($id_foda,$atributo)
    {
        
        if($id_foda && $atributo){
        if($this->session->userdata('normal')) {

            $id = $this->session->userdata('normal')->IDUSER;

            $rut=$this->Criterios_model->getIDempresa($id);

            if($this->Estrategia_model->consultar_usuario($id_foda,$rut)!=false){

            if($atributo=='FORTALEZA'){

                $this->Estrategia_model->eliminar_foda($id_foda,'FORTALEZA');

                $this->session->set_flashdata('registrostatus', 'Fortaleza eliminada');

                redirect('Estrategia/ver_foda');
            }else if($atributo=='AMENAZA'){

                $this->Estrategia_model->eliminar_foda($id_foda,'AMENAZA');

                $this->session->set_flashdata('registrostatus', 'Amenaza eliminada');

                redirect('Estrategia/ver_foda');
            }else if($atributo=='OPORTUNIDAD'){

                $this->Estrategia_model->eliminar_foda($id_foda,'OPORTUNIDAD');

                $this->session->set_flashdata('registrostatus', 'Oportunidad eliminada');

                redirect('Estrategia/ver_foda');
            }
            else if($atributo=='DEBILIDAD'){

                $this->Estrategia_model->eliminar_foda($id_foda,'DEBILIDAD');

                $this->session->set_flashdata('registrostatus', 'Debilidad eliminada');

                redirect('Estrategia/ver_foda');
            }
        }else redirect('Estrategia/menu');
        }else{

            redirect('Welcome/login');
        }
        }else{

            redirect('Estrategia/menu');
        }
    }

    public function ver_foda()
    {
        $config['activo'] = 'estrategia';
        $config['img'] = '9';
        $config['animacion'] = '1';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
             $id = $this->session->userdata('normal')->IDUSER;
             if ($this->Usuarios_model->existe_empresa($id)){
                 
            $this->load->view('estructura/menu_superior_usuario',$config);
           
            $rut=$this->Criterios_model->getIDempresa($id);
            $data['foda']=$this->Estrategia_model->ver_foda($rut);
            $data['existe_fortaleza']=$this->Estrategia_model->existe_foda($rut,'FORTALEZA');
            $data['existe_debilidad']=$this->Estrategia_model->existe_foda($rut,'DEBILIDAD');
            $data['existe_oportunidad']=$this->Estrategia_model->existe_foda($rut,'OPORTUNIDAD');
            $data['existe_amenaza']=$this->Estrategia_model->existe_foda($rut,'AMENAZA');
            
            $this->load->view('estrategia/ver_foda',$data);
        }
        else{
            redirect('Empresa/registro');
        }
        }
        else{
            redirect('Welcome/login');
        }
    }
    
    public function pest(){
        $config['activo'] = 'estrategia';
        $config['img'] = '9';
        $config['animacion'] = '1';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){

              $id = $this->session->userdata('normal')->IDUSER;
         if ($this->Usuarios_model->existe_empresa($id)){
            $this->load->view('estructura/menu_superior_usuario',$config);

          
            $rut=$this->Criterios_model->getIDempresa($id);
            $data['politico']= $this->Estrategia_model->get_pest($rut,'POLITICO');
            $data['economico']= $this->Estrategia_model->get_pest($rut,'ECONOMICO');
            $data['scultural'] = $this->Estrategia_model->get_pest($rut,'SOCIOCULTURAL');
            $data['tecnologico'] = $this->Estrategia_model->get_pest($rut,'TECNOLOGICO');
            $data['existe_politico']= $this->Estrategia_model->existe_pest($rut,'POLITICO');
            $data['existe_economico']= $this->Estrategia_model->existe_pest($rut,'ECONOMICO');
            $data['existe_scultural'] = $this->Estrategia_model->existe_pest($rut,'SOCIOCULTURAL');
            $data['existe_tecnologico'] = $this->Estrategia_model->existe_pest($rut,'TECNOLOGICO');
            $this->load->view('estrategia/pest',$data);
        }
        else{
            redirect('Empresa/registro');
        }
        }
        else{
            redirect('Welcome/login');
        }
        
    }
    
    
        public function insertar_pest()
    {
        if ($_POST) {
            
            if($this->session->userdata('normal')){

            $id= $this->session->userdata('normal')->IDUSER;
            $rut=$this->Criterios_model->getIDempresa($id);
            
            if($this->input->post('politico')){
            $descripcion = $this->input->post('politico');
            $fpe_id='POLITICO';
            }else{
                 if($this->input->post('economico')){
                     $descripcion = $this->input->post('economico');
                     $fpe_id='ECONOMICO';
                 }else{
                     if($this->input->post('scultural')){
                         $descripcion = $this->input->post('scultural');
                     $fpe_id='SOCIOCULTURAL';
                     } else{
                         if($this->input->post('tecnologico')){
                             $descripcion = $this->input->post('tecnologico');
                             $fpe_id='TECNOLOGICO';
                         }
                     }
                 }
            }
                   
            $datosinsert = array(
                'FPE_ID' =>$fpe_id,
                'EMP_RUT' => $rut,
                'ANP_DESCRIPCION' => $descripcion,
            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->insert_pest($datosinsert);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
             redirect('Estrategia/pest'); 
               
             
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/menu');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }
    
    
        public function editar_pest()
    {
        if ($_POST) {
            
            if($this->session->userdata('normal')){

            $id= $this->session->userdata('normal')->IDUSER;
            $rut=$this->Criterios_model->getIDempresa($id);
            
            if($this->input->post('politico')){
            $descripcion = $this->input->post('politico');
            $fpe_id='POLITICO';
            }else{
                 if($this->input->post('economico')){
                     $descripcion = $this->input->post('economico');
                     $fpe_id='ECONOMICO';
                 }else{
                     if($this->input->post('scultural')){
                         $descripcion = $this->input->post('scultural');
                     $fpe_id='SOCIOCULTURAL';
                     } else{
                         if($this->input->post('tecnologico')){
                             $descripcion = $this->input->post('tecnologico');
                             $fpe_id='TECNOLOGICO';
                         }
                     }
                 }
            }
                   
            $datosinsert = array(
                'ANP_DESCRIPCION' => $descripcion,
            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->update_pest($datosinsert,$rut,$fpe_id);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
             redirect('Estrategia/pest'); 
               
             
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/menu');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }
    
    public function analisis_4p(){
        $config['activo'] = 'estrategia';
        $config['img'] = '9';
        $config['animacion'] = '1';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
            $id = $this->session->userdata('normal')->IDUSER;
            if ($this->Usuarios_model->existe_empresa($id)){
            $this->load->view('estructura/menu_superior_usuario',$config);
         
            
            $rut=$this->Criterios_model->getIDempresa($id);   

        $data['producto']= $this->Estrategia_model->get_4p($rut,'PRODUCTO');
        $data['existe_producto']= $this->Estrategia_model->existe_4p($rut,'PRODUCTO');
        $data['precio']= $this->Estrategia_model->get_4p($rut,'PRECIO');
        $data['existe_precio']= $this->Estrategia_model->existe_4p($rut,'PRECIO');
        $data['plaza']= $this->Estrategia_model->get_4p($rut,'PLAZA');
        $data['existe_plaza']= $this->Estrategia_model->existe_4p($rut,'PLAZA');
        $data['promocion']= $this->Estrategia_model->get_4p($rut,'PROMOCION');
        $data['existe_promocion']= $this->Estrategia_model->existe_4p($rut,'PROMOCION');
       
          $this->load->view('estrategia/analisis_4p',$data);
        }
        else{
            redirect('Empresa/registro');
        }
        }
        else{
            redirect('Welcome/login');
        }
        
    }    
      
    
    public function insertar_4p()
    {
        if ($_POST) {
  if($this->session->userdata('normal')){
            $id= $this->session->userdata('normal')->IDUSER;
            $rut=$this->Criterios_model->getIDempresa($id);
            
            if($this->input->post('producto')){
            $descripcion = $this->input->post('producto');
            $cup_id='PRODUCTO';
            }else{
                 if($this->input->post('precio')){
                     $descripcion = $this->input->post('precio');
                     $cup_id='PRECIO';
                 }else{
                     if($this->input->post('plaza')){
                         $descripcion = $this->input->post('plaza');
                     $cup_id='PLAZA';
                     } else{
                         if($this->input->post('promocion')){
                             $descripcion = $this->input->post('promocion');
                             $cup_id='PROMOCION';
                         }
                     }
                 }
            }
                   
            $datosinsert = array(
                'EMP_RUT' => $rut,
                'CUP_ID' =>$cup_id,
                'ACP_DESCRIPCION' => $descripcion,
            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->insert_4p($datosinsert);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
             redirect('Estrategia/analisis_4p'); 
               
             
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/menu');
            }

        } else {
            redirect('Welcome');
        }

        } else {
            redirect('Estrategia/menu');
        }
    }
    
    
    public function editar_4p()
    {
        if ($_POST) {
if($this->session->userdata('normal')){
            $id= $this->session->userdata('normal')->IDUSER;
            $rut=$this->Criterios_model->getIDempresa($id);
            
            if($this->input->post('producto')){
            $descripcion = $this->input->post('producto');
            $cup_id='PRODUCTO';
            }else{
                 if($this->input->post('precio')){
                     $descripcion = $this->input->post('precio');
                     $cup_id='PRECIO';
                 }else{
                     if($this->input->post('plaza')){
                         $descripcion = $this->input->post('plaza');
                     $cup_id='PLAZA';
                     } else{
                         if($this->input->post('promocion')){
                             $descripcion = $this->input->post('promocion');
                             $cup_id='PROMOCION';
                         }
                     }
                 }
            }
                   
            $datosinsert = array(
                'ACP_DESCRIPCION' => $descripcion,
            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->update_4p($datosinsert,$rut,$cup_id);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
             redirect('Estrategia/analisis_4p'); 
               
             
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/menu');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }

    public function insertar_obj_est()
    {
        if ($_POST) {

if($this->session->userdata('normal')){
            $objetivo = $this->input->post('objetivo');
            $id= $this->session->userdata('normal')->IDUSER;
            $rut=$this->Criterios_model->getIDempresa($id);
            $datosinsert = array(
                'EMP_RUT' => $rut,
                'OE_DESCRIPCION' => $objetivo,

            );
             $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->insert_obj_est($datosinsert);
            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/ver_objetivos');
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }
    
    
    
    public function editar_objetivo_estrategico()
    {
        if ($_POST) {

if($this->session->userdata('normal')){
            $objetivo = $this->input->post('objetivo');
            $oe_id = $this->input->post('oe_id');

            $id= $this->session->userdata('normal')->IDUSER;

            $rut=$this->Criterios_model->getIDempresa($id);

            $datosinsert = array(
                'OE_DESCRIPCION' => $objetivo,

            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->update_objetivos_estrategicos($datosinsert,$oe_id,$rut);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/ver_objetivos');
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }

        } else {
            redirect('Estrategia/menu');
        }
    }

    public function ver_objetivos()
    {
        $config['activo'] = 'estrategia';
        $config['img'] = '9';
        $config['animacion'] = '1';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
             $id = $this->session->userdata('normal')->IDUSER;
              if ($this->Usuarios_model->existe_empresa($id)){
            $this->load->view('estructura/menu_superior_usuario',$config);
           
            $rut=$this->Criterios_model->getIDempresa($id);
            $data['objetivos']=$this->Estrategia_model->objetivos($rut);
            $data['existe_objetivo']=$this->Estrategia_model->existen_objetivos($rut);
            $this->load->view('estrategia/ver_objetivos',$data);
        }
        else{
            redirect('Empresa/registro');
        }
        }
        else{
            redirect('Welcome/login');
        }
    }

    public function eliminar_objetivo($id_obj)
    {
        if ($id_obj){
        if($this->session->userdata('normal')) {

            $id = $this->session->userdata('normal')->IDUSER;

            $rut=$this->Criterios_model->getIDempresa($id);

            if($this->Estrategia_model->consultar_usuario2($id_obj,$rut)!=false){

                    $this->Estrategia_model->eliminar_objetivo($id_obj);

                    $this->session->set_flashdata('registrostatus', 'Objetivo eliminado');

                    redirect('Estrategia/ver_objetivos');

            }else redirect('Estrategia/menu');
        }else{

            redirect('Welcome/login');
        }
    }else redirect('estrategia/menu');
    }

    public function insertar_fce()
    {
        if ($_POST) {

if($this->session->userdata('normal')) {
            $oe_id = $this->input->post('objetivo_id');
            $fce_factor = $this->input->post('factor');

            $id= $this->session->userdata('normal')->IDUSER;

            $datosinsert = array(
                'OE_ID' => $oe_id,
                'FCE_FACTOR' => $fce_factor,

            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->insert_fce($datosinsert);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/ver_factores/'.$oe_id);
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }
    public function editar_fce()
    {
        if ($_POST) {

            if($this->session->userdata('normal')) {

            $oe_id = $this->input->post('objetivo_id');
            $fce_factor = $this->input->post('factor');
            $fce_id= $this->input->post('fce_id');

            $id= $this->session->userdata('normal')->IDUSER;

            $datosinsert = array(
                'FCE_FACTOR' => $fce_factor,

            );
            $datosinsert = $this->security->xss_clean($datosinsert);
            $insertarinfo= $this->Estrategia_model->update_fce($datosinsert,$fce_id,$oe_id);

            if ($insertarinfo) {
                $this->session->set_flashdata('Exito', 'Agregar');
                redirect('Estrategia/ver_factores/'.$oe_id);
            } else {

                $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                redirect('Estrategia/mision_vision');
            }

        } else {
            redirect('Welcome');
        }
        } else {
            redirect('Estrategia/menu');
        }
    }

    public function ver_factores($id_obj)
    {
        $config['activo'] = 'estrategia';
        $config['img'] = '9';
        $config['animacion'] = '1';
        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
        
   if ($this->Usuarios_model->existe_empresa($id)){
            $this->load->view('estructura/menu_superior_usuario',$config);
           
            $rut=$this->Criterios_model->getIDempresa($id);
            $data['objetivo']=$id_obj;
            $data['factores']=$this->Estrategia_model->factores($id_obj);
            $data['descripcion']=$this->Estrategia_model->get_descripcionxobjetivo($id_obj);
            $data['existe_factor']=$this->Estrategia_model->existen_fce($id_obj);

            if($this->Estrategia_model->consultar_usuario2($id_obj,$rut)!=false){
                $this->load->view('estrategia/ver_factores',$data);
            }else redirect('Estrategia/menu');
        }
        else{
            redirect('Empresa/registro');
        }
        }
        else{
            redirect('Welcome/login');
        }
    }

    public function eliminar_factor($id_factor,$id_obj)
    {
        if($id_factor && $id_obj){
        
        if($this->session->userdata('normal')) {

            $id = $this->session->userdata('normal')->IDUSER;

            $rut=$this->Criterios_model->getIDempresa($id);

            if($this->Estrategia_model->consultar_usuario3($rut,$id_factor)!=false){

                $this->Estrategia_model->eliminar_factor($id_factor);

                $this->session->set_flashdata('registrostatus', 'Factor eliminado');

                redirect('Estrategia/ver_factores/'.$id_obj);
            }else redirect('Estrategia/menu');

        }else{

            redirect('Welcome/login');
        }
        }else{

            redirect('Estrategia/menu');
        }
    }

     
    
}
?>