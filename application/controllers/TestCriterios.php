<?php


class TestCriterios extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Criterios_model');
        $this->load->library('unit_test');
        date_default_timezone_set('America/Santiago');
    }

    public function preguntasxdominio()
    {
        $preguntas=$this->Criterios_model->preguntasxdominio(1,92);

        echo $this->unit->run($preguntas,'is_array', 'preguntas x dominio');

    }

    public function respuestasxdominio()
    {
        $respuestas=$this->Criterios_model->respuestasxdominio(1,92);

        echo $this->unit->run($respuestas,'is_array', 'respuestas x dominio');

    }

    public function get_id_analisis()
    {
        $id_user=9;
        $rut_emp='21.165.833-9';

        $analisis=$this->Criterios_model->getIDanalisis($id_user,$rut_emp);

        echo $this->unit->run($analisis,'is_string', 'id analisis');

    }

    public function get_rut_empresa()
    {
        $id_user=10;

        $emp_rut=$this->Criterios_model->getIDempresa($id_user);

        echo $this->unit->run($emp_rut,'is_string', 'id empresa');

    }

    public function mostrar_diagnosticos()
    {
        $id_user=9;

        $diagnosticos=$this->Criterios_model->mostrar_diagnosticos($id_user);

        echo $this->unit->run($diagnosticos,'is_array', 'mostrar diagnosticos');

    }

    public function consultar_usuario()
    {
        $id_user=9;
        $id_ana=72;

        $analisis=$this->Criterios_model->consultar_usuario($id_ana,$id_user);

        echo $this->unit->run($analisis,'is_object');

    }

    public function dominio_contestado()
    {
        $id_dom=1;
        $id_ana=137;

        $analisis=$this->Criterios_model->dom_contestado($id_ana,$id_dom);

        echo $this->unit->run($analisis,'is_string');

    }

    public function presupuestoxdominio()
    {
        $id_dom=3;
        $id_ana=71;

        $presupuesto=$this->Criterios_model->existe_presupuesto($id_ana,$id_dom);

        echo $this->unit->run($presupuesto,'is_string');

    }

    public function existe_presupuesto()
    {
        $id_ana=71;

        $presupuesto=$this->Criterios_model->existe_presupuesto2($id_ana);

        echo $this->unit->run($presupuesto,'is_string');

    }

    public function presupuesto_total()
    {
        $id_ana=71;

        $presupuesto=$this->Criterios_model->presupuesto_total($id_ana);

        echo $this->unit->run($presupuesto,'is_string');

    }

    /*Test de Integración */

    public function inicio_diagnostico()
    {
        $datos_diag = array(
            'EMP_RUT' => '21.165.833-9',
            'USU_ID' => 9,
            'ANA_FECHA'=> date('Y-m-d H:i:s')
        );

        $diag = $this->Criterios_model->inicio_diagnostico($datos_diag);

        echo $this->unit->run($diag,'is_int');
    }

    public function ponderacionxdominio()
    {
        $datosinsert = array(
            'ANA_ID' => 138,
            'DOM_ID' => 6,
            'PONDERACION' => 100,

        );

        $insert = $this->Criterios_model->ponderacionxdominio($datosinsert);

        echo $this->unit->run($insert,'is_int');
    }

    public function resultadosxcriterio()
    {
        $ana_id=138;
        $domid=6;
        $puntaje=3;

        $this->Criterios_model->resultadosxdominio($ana_id,$domid,$puntaje);

        $datosinsert = array(
            'ID_RESPUESTA' => 324,
            'ANA_ID' => $ana_id,
            'CRIT_NUMERO' => 87,
            'PUNTAJEXCRITERIO' => 3,

        );


        $insert = $this->Criterios_model->resultadosxcriterio($datosinsert);

        echo $this->unit->run($insert,'is_int');
    }










}