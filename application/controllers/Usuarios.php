<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Usuarios extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('email');
        $this->load->model('Usuarios_model');
        $this->load->model('Resultados_model');
        $this->load->model('Criterios_model');
        $this->load->model('Empresa_model');
        $this->load->library('Pdf');
        $this->load->library('session');
//        $this->load->library('email');
        $this->load->helper('url');
        date_default_timezone_set('America/Santiago');
    }

    public function index() {


        $config['activo'] = 'perfil';
        $config['img'] = '9';
        $config['animacion'] = '1';
       $this->load->view('estructura/header');
       
       if($this->session->userdata('normal')){
           $id = $this->session->userdata('normal')->IDUSER;
           if ($this->Usuarios_model->existe_empresa($id)){
            $this->load->view('estructura/menu_superior_usuario',$config);
            
            
            $data['usuario']= $this->Usuarios_model->getUsuarioxid($id);
            $data['empresa'] = $this->Empresa_model->getEmpresas($id);
            $data['oan']= $this->Usuarios_model->otros_antecedentes($id);

            $this->load->view('usuarios/perfil',$data);
           }else{
               redirect('Empresa/registro');
           }
        }
        else{      
        redirect('Welcome');
        }



       
    }

    public function registro() {
           $config['activo'] = 'registro';
           $config['img'] = '2';
           $config['animacion'] = '0';
            
           $this->load->view('estructura/header');  
         $this->load->view('estructura/menu_superior', $config);
           
           
            $this->load->view('usuarios/registro');

        } 
        
        
        public function reset_password(){
            
          $config['activo'] = 'registro';
           $config['img'] = '2';
           $config['animacion'] = '0';
            
           $this->load->view('estructura/header');  
         $this->load->view('estructura/menu_superior', $config);
           
           
            $this->load->view('usuarios/reset_password');

        } 
        
       
        
        
        
        public function email_reset(){
            
if($_POST)
{
    //generar nueva contraseña
 function randomPassword(){ 
    $alphabet = "abcdefghjkmnopqrstuwxyzABCDEFGHJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); 
    $alphaLength = strlen($alphabet) - 1; 
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); 
 }
 // fin generar nueva contraseña
    $email=$this->input->post('correo');

        $userid= $this->Usuarios_model->existe_correo($email);
        $data=$this->Usuarios_model->getdata_correo($email);
        $fecha = date('Y-m-d H:i:s');
        
        if($userid==1)
        {
            require_once('assets/PHPMailer/PHPMailerAutoload.php');
 
$mail = new PHPMailer();
$mail->CharSet =  "utf-8";
$mail->IsSMTP(); // Set mailer to use SMTP
$mail->SMTPDebug = 1;  // Enable verbose debug output
$mail->SMTPAuth = true; // Enable SMTP authentication
$mail->Username = "mipe.up2017@gmail.com"; //Your Auth Email ID
$mail->Password = "diagnosticoubb"; //Your Password
$mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
$mail->Host = "smtp.gmail.com"; // SMTP 
$mail->Port = "587"; // TCP port to connect to
 
$mail->setFrom('info@mipeUp.cl', 'Mipe_Up');
$mail->AddAddress("$email" , "$data->USU_NOMBRES $data->USU_APELLIDOS"); // Add a recipient
 
 
$mail->Subject = "MipeUP Reestablecer Contraseña";
$password=randomPassword();
$mail->Body = 
"<div>
    Estimado ".$data->USU_NOMBRES." ".$data->USU_APELLIDOS."<br><br>
    Tu nueva contraseña es:<strong> ".$password."</strong><br><br>
    Recuerda actualizar tu contraseña una vez que entres al sistema
    en la sección <strong>Perfil->Editar información Personal.</strong>

</div>";
$mail->ContentType = "text/html";
 
if($mail->Send()){
// $str = "OK"; 
    
  $datos = array(
          'USU_PASSWORD' => sha1($password),
          'USU_FECHAMODIFICACION' => $fecha,
                    );
                    
    $actualizar = $this->Usuarios_model->actualizar($datos,$data->USU_ID);  
 
   $this->session->set_flashdata('Exito', 'Actualizar');
    redirect('Welcome/login');
}else{
     $this->session->set_flashdata('Fail', 'Actualizar');
     redirect('Welcome/login');
// $str = "ERR"; 
}

        }
        else
        {
           $this->session->set_flashdata('Fail', 'Actualizar');
           redirect('Usuarios/reset_password');
        }
}
redirect('Welcome');
            
        }
        
        
                public function menu_diagnostico(){
        
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '1';
       $this->load->view('estructura/header');
       if($this->session->userdata('normal')){
           
            $id = $this->session->userdata('normal')->IDUSER;
             if ($this->Usuarios_model->existe_empresa($id)){
                 
             
            $this->load->view('estructura/menu_superior_usuario',$config); 
          
            $emp_rut = $this->Criterios_model->getIDempresa($id);
            $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
            $data['ana_id']=$ana_id;
            $data['puede']= $this->Resultados_model->puedeComparar($id);
            $data['continua']=$this->Resultados_model->continua($ana_id);//si distinto de null,debe continuar                   
            $this->load->view('usuarios/menu_diagnostico',$data);
        }else{
            redirect('Empresa/registro');
        }   
       }else
        {
        redirect('Welcome/login');
        }
        
        
    }
                public function perfil(){
        
        $config['activo'] = 'perfil';
        $config['img'] = '9';
        $config['animacion'] = '1';
       $this->load->view('estructura/header');
  
       if($this->session->userdata('normal')){
                $id = $this->session->userdata('normal')->IDUSER;
           if ($this->Usuarios_model->existe_empresa($id)){
            $this->load->view('estructura/menu_superior_usuario',$config);
            

            $data['usuario']= $this->Usuarios_model->getUsuarioxid($id);
            $data['empresa'] = $this->Empresa_model->getEmpresas($id);
            $data['oan']= $this->Usuarios_model->otros_antecedentes($id);
      
            $this->load->view('usuarios/perfil',$data);
           }else{
               redirect('Empresa/registro');
           }
        }
        else{      
        redirect('Welcome');
        }
        
        
    }
    

    
        
    
    public function infoadicional(){
        
           $config['activo'] = 'perfil';
           //img = 9 -> sin banner
           $config['img'] = '9';
           $config['animacion'] = '0';
            
           
           $this->load->view('estructura/header');  
        
            
         if($this->session->userdata('normal')){
             
             $id=$this->session->userdata('normal')->IDUSER;
               if ($this->Usuarios_model->existe_empresa($id)){
             
           $this->load->view('estructura/menu_superior_usuario',$config);
            $this->load->view('usuarios/info_adicional');
         }else{
             redirect('Empresa/registro');
         }
         
         }else{
             redirect('Welcome/login');
         }
        
    }
        
        
            public function editar() {
           $config['activo'] = 'perfil';
           //img = 9 -> sin banner
           $config['img'] = '9';
           $config['animacion'] = '0';
            
           
           $this->load->view('estructura/header');  
        
            
         if($this->session->userdata('normal')){
             
             $id=$this->session->userdata('normal')->IDUSER;
         
             if ($this->Usuarios_model->existe_empresa($id)){
             
             
                $this->load->view('estructura/menu_superior_usuario',$config);   
                 $data['usuario']=$this->Usuarios_model->getUsuarioxid($id);
             
            $this->load->view('usuarios/editar',$data);
         }else{
             redirect('Empresa/registro');
         }
         
            }else{
                redirect('Welcome/login');
            }
            }
            
             
            
            
        
                  public function editar_infoadicional() {
           $config['activo'] = 'perfil';
           //img = 9 -> sin banner
           $config['img'] = '9';
           $config['animacion'] = '0';
            
           
           $this->load->view('estructura/header');  
        
            
         if($this->session->userdata('normal')){
             
             $id=$this->session->userdata('normal')->IDUSER;
         
             if ($this->Usuarios_model->existe_empresa($id)){
                 
             
             $this->load->view('estructura/menu_superior_usuario',$config);
             $data['usuario']=$this->Usuarios_model->get_otrosantecedentes($id);  
            $this->load->view('usuarios/editar_infoadicional',$data);
         }else{
             redirect('Empresa/registro');
         }
        }else{
            redirect('Welcome/login');
        }  
                  }
        
        
        
        
        public function actualizar(){
            
             if ($_POST) {

                if($this->session->userdata('normal')){
                    
                $id = $this->session->userdata('normal')->IDUSER;
                $nombres = $this->input->post('nombres');
                $apellidos = $this->input->post('apellidos');
                $correo = $this->input->post('correo');    
                $fecha = date('Y-m-d H:i:s');

                    $datos = array(
                        'USU_NOMBRES' => $nombres,
                        'USU_APELLIDOS' => $apellidos,
                        'USU_CORREO' => $correo,
                         'USU_FECHAMODIFICACION' => $fecha,
                    );
                    $datos = $this->security->xss_clean($datos);
                    $actualizar = $this->Usuarios_model->actualizar($datos,$id);




                    if ( $actualizar) {
                       $this->session->set_flashdata('Exito1', 'Actualizar');
                      redirect('Usuarios/perfil');
                    } else {

                       $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                        redirect('Usuarios');
                    }
                }else{
                    redirect('Welcome');
                }
                    
            } else {
                redirect('Usuarios');
            }
        
            
        }
        public function editar_password(){
            
             if ($_POST) {

                  if($this->session->userdata('normal')){
                 
                $id = $this->session->userdata('normal')->IDUSER;
                $fecha = date('Y-m-d H:i:s');
                $password = sha1($this->input->post('password'));
                $password1 = $this->input->post('password1');
                $password2 = $this->input->post('password2');

                
                if($usuario = $this->Usuarios_model->check_pass($id, $password)){
                    
                     if($password1==$password2){

                    $datos = array(
                         'USU_PASSWORD' => sha1($password1),
                         'USU_FECHAMODIFICACION' => $fecha,
                    );
                         $datos = $this->security->xss_clean($datos);
                    $actualizar = $this->Usuarios_model->actualizar($datos,$id);
                    
                    if ( $actualizar) {
                       $this->session->set_flashdata('Exito', 'Actualizar');
                      redirect('Usuarios/perfil');
                    } else {

                       $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                        redirect('Usuarios');
                    }
                    
                    
                    
                    }else{       
                    $this->session->set_flashdata('Fail2', 'Actualizar');
                    redirect('Usuarios/editar');
                    }
                            
                }else{
                    $this->session->set_flashdata('Fail1', 'Actualizar');
                    redirect('Usuarios/editar');
                }
                  }else{
                      redirect('Welcome');
                  }
            } else {
                redirect('Usuarios');
            }
        
            
        }
        
        
        public function actualizar_infoadicional(){
            
        if ($_POST) {

             if($this->session->userdata('normal')){    
            
                $nestudios = $this->input->post('nestudios');
                $anosrubro = $this->input->post('anosrubro');
                $anosindependiente = $this->input->post('anosindependiente');
                $regresado = $this->input->post('regresado');    
                $nemprendimientos = $this->input->post('nemprendimientos'); 
                $relacion = $this->input->post('relacion');     
                $motivaciones = $this->input->post('motivaciones');
                $id= $this->session->userdata('normal')->IDUSER;
                
                    $datosinsert = array(
                        'USU_ID' => $id,
                        'OAN_NIVELESTUDIOS' => $nestudios,
                        'OAN_ANOSENELRUBRO' => $anosrubro,
                        'OAN_ANOSCOMOINDEPENDIENTE' => $anosindependiente,
                        'OAN_REGRESADOCOMOINDEPENDIENTE' => $regresado,
                       'OAN_NUMEROEMPRENDIMIENTOS' => $nemprendimientos,
                        'OAN_RELACIONDEEMPRENDIMIENTOS' => $relacion,
                        'OAN_MOTIVACIONES' => $motivaciones

                    );
                 $datosinsert = $this->security->xss_clean($datosinsert);
                    $actualizarinfo= $this->Usuarios_model->actualizar_infoadicional($datosinsert,$id);




                    if ($actualizarinfo) {
                       $this->session->set_flashdata('Exito1', 'Actualizar');
                      redirect('Usuarios/perfil');
                    } else {

                       $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                        redirect('Usuarios');
                    }
                        } else {
                redirect('Welcome');
            } 
                    
            } else {
                redirect('Usuarios');
            }
        
            
        }

        
        
    public function insertar() {
        if ($_POST) { 
                $nombres = $this->input->post('nombres');
                $apellidos = $this->input->post('apellidos');
                $rut = $this->input->post('rut');
                $correo = $this->input->post('correo');    
                $fecha = date('Y-m-d H:i:s');
                $password = $this->input->post('password');
             


                if ($this->Usuarios_model->existerut($rut) == 0) {
                
                    $datosinsert = array(
                        'USU_RUT' => $rut,
                        'USU_NOMBRES' => $nombres,
                        'USU_APELLIDOS' => $apellidos,
                        'USU_CORREO' => $correo,
                        'USU_PASSWORD' => sha1($password),
                       'USU_FECHAREGISTRO' => $fecha,
                        'USU_ESTADO' => 1,

                    );
                    $datosinsert = $this->security->xss_clean($datosinsert);
                    $insertarusuario = $this->Usuarios_model->insertar($datosinsert);




                    if ($insertarusuario) {
                        
    //envia correo confirmando creacion de la cuenta                    
require_once('assets/PHPMailer/PHPMailerAutoload.php');
 
$mail = new PHPMailer();
$mail->CharSet =  "utf-8";
$mail->IsSMTP(); // Set mailer to use SMTP
$mail->SMTPDebug = 1;  // Enable verbose debug output
$mail->SMTPAuth = true; // Enable SMTP authentication
$mail->Username = "mipe.up2017@gmail.com"; //Your Auth Email ID
$mail->Password = "diagnosticoubb"; //Your Password
$mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
$mail->Host = "smtp.gmail.com"; // SMTP 
$mail->Port = "587"; // TCP port to connect to
 
$mail->setFrom('info@mipeUp.cl', 'Mipe_Up');
$mail->AddAddress("$correo" , "$nombres $apellidos"); 
 
 
$mail->Subject = "Gracias por registrarte en MipeUP";

$mail->Body = 
"<div>
    Estimado ".$nombres." ".$apellidos."<br><br>
    Tu cuenta en MipeUp ha sido creada exitosamente. <br>
    

</div>";
$mail->ContentType = "text/html";
$mail->Send();
           
                       $this->session->set_flashdata('Exito2', 'Agregar');
                      redirect('Welcome/login');
                    } else {

                       $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                        redirect('Usuarios');
                    }
                } else {
                   $this->session->set_flashdata('Fail', 'El rut ingresado ya lo posee otro usuario. Compruebe los datos e intente nuevamente');
                    redirect('Usuarios');
                }
                
            } else {
                redirect('Usuarios');
            }
        
    }
    
    
    
    public function insertarInfoAdicional(){
        
         if ($_POST) {

                
             if($this->session->userdata('normal')){ 
             
                $nestudios = $this->input->post('nestudios');
                $anosrubro = $this->input->post('anosrubro');
                $anosindependiente = $this->input->post('anosindependiente');
                $regresado = $this->input->post('regresado');    
                $nemprendimientos = $this->input->post('nemprendimientos'); 
                $relacion = $this->input->post('relacion');     
                $motivaciones = $this->input->post('motivaciones');
                $id= $this->session->userdata('normal')->IDUSER;

                
                    $datosinsert = array(
                        'USU_ID' => $id,
                        'OAN_NIVELESTUDIOS' => $nestudios,
                        'OAN_ANOSENELRUBRO' => $anosrubro,
                        'OAN_ANOSCOMOINDEPENDIENTE' => $anosindependiente,
                        'OAN_REGRESADOCOMOINDEPENDIENTE' => $regresado,
                       'OAN_NUMEROEMPRENDIMIENTOS' => $nemprendimientos,
                        'OAN_RELACIONDEEMPRENDIMIENTOS' => $relacion,
                        'OAN_MOTIVACIONES' => $motivaciones

                    );
                 $datosinsert = $this->security->xss_clean($datosinsert);
                    $insertarinfo= $this->Usuarios_model->insertarInfoAdicional($datosinsert);




                    if ($insertarinfo) {
                       $this->session->set_flashdata('Exito2', 'Agregar');
                      redirect('Usuarios/perfil');
                    } else {

                       $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                        redirect('Usuarios');
                    }
       
            } else {
                redirect('Welcome');
            }
            } else {
                redirect('Usuarios');
            }
        
    }
        
        
    

   public function checkrut() {
       
       if($_POST){
        $rut = $this->input->post("rut");
        $result_codigo = $this->Usuarios_model->existerut($rut);
        echo $result_codigo;
    }else{
        redirect('Welcome');
    }
   }

    public function checkcorreo() {
        
        if($_POST){
        $correo = $this->input->post("correo");
        $result_codigo = $this->Usuarios_model->existecorreo($correo);



        echo $result_codigo;
        }else
            redirect ('Welcome');
        
    }
    public function checkcorreoEditar() {
        
        if($_POST){
        $correo = $this->input->post("correo");
        $actual = $this->session->userdata('normal')->CORREO;
        $result_codigo = $this->Usuarios_model->existecorreo2($correo,$actual);



        echo $result_codigo;
        }else
            redirect ('Welcome');
        
    }
    
    
        public function pdf_ficha_usuario()
    {
        
        if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
        
             if ($this->Usuarios_model->existe_empresa($id) && $this->Usuarios_model->otros_antecedentes($id)){
            
        $data['empresa'] = $this->Empresa_model->getEmpresas($id);
        $data['oan'] = $this->Usuarios_model->get_otros_antecedentes($id);
        $data['userdata'] = $this->Usuarios_model->get_user_data($id);
  
        $this->load->view('usuarios/pdf_ficha_usuario',$data);
  
        }else{
            redirect('Usuarios/perfil');
        }
        }else{
            redirect('Welcome');
        }
    }
    
     public function pdf_ficha_empresa()
    {
        
        if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
        
             if ($this->Usuarios_model->existe_empresa($id)){
            
        $data['empresa'] = $this->Empresa_model->getEmpresas($id);
        $data['userdata'] = $this->Usuarios_model->get_user_data($id);
   $data['oan'] = $this->Usuarios_model->get_otros_antecedentes($id);
        $this->load->view('usuarios/pdf_ficha_empresa',$data);
  
        }else{
            redirect('Usuarios/perfil');
        }
        }else{
            redirect('Welcome');
        }
    }
   

}
