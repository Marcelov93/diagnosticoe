<?php


class TestUsuarios extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Usuarios_model');
        $this->load->library('unit_test');
        date_default_timezone_set('America/Santiago');
    }

    public function prueba_login()
    {

        $correo='luis@mail.com';
        $password=sha1('123456');

        $login=$this->Usuarios_model->login($correo,$password);

        echo $this->unit->run($login,'is_object', 'login');

    }

    public function existe_empresa()
    {
        $id_user=9;

        $empresa=$this->Usuarios_model->existe_empresa($id_user);

        echo $this->unit->run($empresa,'is_object', 'existe empresa');

    }

    public function obtener_usuarioxid()
    {
        $id_user=9;

        $usuario=$this->Usuarios_model->getUsuarioxid($id_user);

        echo $this->unit->run($usuario,'is_object', 'obtener usuario');

    }

    public function obtener_antecedentes()
    {
        $id_user=9;

        $antecedentes=$this->Usuarios_model->get_otrosantecedentes($id_user);

        echo $this->unit->run($antecedentes,'is_object', 'obtener antecedentes');

    }

    public function existe_rut()
    {
        $rut='18.137.840-9';

        $usuario=$this->Usuarios_model->existerut($rut);

        echo $this->unit->run($usuario,1, 'existe rut');

    }

    public function obtener_usuarioxcorreo()
    {
        $correo='luis@mail.com';

        $usuario=$this->Usuarios_model->getUsuarioxcorreo($correo);

        echo $this->unit->run($usuario,'is_object', 'obtener usuario por correo');

    }

    public function datosxcorreo()
    {
        $correo='luis@mail.com';

        $usuario=$this->Usuarios_model->getdata_correo($correo);

        echo $this->unit->run($usuario,'is_object', 'obtener datos por correo');
    }

    public function existe_correo()
    {
        $correo='luis@mail.com';

        $usuario=$this->Usuarios_model->existe_correo($correo);

        echo $this->unit->run($usuario,1, 'existe correo');
    }

    public function check_pass()
    {
        $id=9;
        $password=sha1('123456');

        $pass=$this->Usuarios_model->check_pass($id,$password);

        echo $this->unit->run($pass,1, 'login');
    }

    /*Pruebas de Integración*/

    public function insertar_usuario()
    {
        $datosinsert = array(
            'USU_RUT' => '19.783.890-6',
            'USU_NOMBRES' => 'Cristiano',
            'USU_APELLIDOS' => 'Ronaldo',
            'USU_CORREO' => 'criss@ronaldo.com',
            'USU_PASSWORD' => sha1('654321'),
            'USU_FECHAREGISTRO' => date('Y-m-d H:i:s'),
            'USU_ESTADO' => 1,

        );

        $insertarusuario = $this->Usuarios_model->insertar($datosinsert);

        echo $this->unit->run($insertarusuario,'is_int');
    }

    public function insertar_info_ad()
    {
        $datosinsert = array(
            'USU_ID' => 9,
            'OAN_NIVELESTUDIOS' => 'universitario',
            'OAN_ANOSENELRUBRO' => 0,
            'OAN_ANOSCOMOINDEPENDIENTE' => 0,
            'OAN_REGRESADOCOMOINDEPENDIENTE' => 'no',
            'OAN_NUMEROEMPRENDIMIENTOS' => 0,
            'OAN_RELACIONDEEMPRENDIMIENTOS' => 'no',
            'OAN_MOTIVACIONES' => 'ninguna'

        );

        $insertarusuario = $this->Usuarios_model->insertarInfoAdicional($datosinsert);

        echo $this->unit->run($insertarusuario,'is_int');


    }

    public function actualizar_user()
    {
        $id_user=17;

        $datos = array(
            'USU_NOMBRES' => 'Arturo Erasmo',
            'USU_APELLIDOS' => 'Vidal Vidal',
            'USU_CORREO' => 'kingarthur@mail.com',
            'USU_FECHAMODIFICACION' => date('Y-m-d H:i:s'),
        );

        $actualizar = $this->Usuarios_model->actualizar($datos,$id_user);

        echo $this->unit->run($actualizar,'is_true');
    }

    public function update_info_ad()
    {
        $id_user=9;
        $datosinsert = array(
            'OAN_NIVELESTUDIOS' => 'universitario',
            'OAN_ANOSENELRUBRO' => 0,
            'OAN_ANOSCOMOINDEPENDIENTE' => 0,
            'OAN_REGRESADOCOMOINDEPENDIENTE' => 'no',
            'OAN_NUMEROEMPRENDIMIENTOS' => 0,
            'OAN_RELACIONDEEMPRENDIMIENTOS' => 'no',
            'OAN_MOTIVACIONES' => 'todas'

        );

        $insertarusuario = $this->Usuarios_model->actualizar_infoadicional($datosinsert,$id_user);

        echo $this->unit->run($insertarusuario,'is_true');

    }




}