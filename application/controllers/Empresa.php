<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Empresa extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('Empresa_model');
        $this->load->model('Usuarios_model');
        $this->load->library('session');
        $this->load->helper('url');
        date_default_timezone_set('America/Santiago');

    }

    public function index() {

        redirect('Usuarios/perfil');
        
    }

    public function registro() {

        $config['activo'] = '';
        $config['img'] = '9';
        $config['animacion'] = '0';


        $this->load->view('estructura/header');

         $id=$this->session->userdata('normal')->IDUSER;
        if($this->session->userdata('normal') && $this->Usuarios_model->existe_empresa($id)==false){
           
            $this->load->view('estructura/menu_superior_usuario2',$config);
       
                $this->load->view('empresa/registro');
        }else{
                redirect('usuarios/perfil');
            }

        }
    

    public function insertar() {
        if ($_POST) {

            if ($this->session->userdata('normal')){    
            
            $id_usu=$this->input->post('idusu');
            $rut_empresa=$this->input->post('rutempresa');
            $razon_social=$this->input->post('razonsocial');
            $nombre_fantasia=$this->input->post('nombrefantasia');
            $rubro=$this->input->post('rubro');
            $telefono=$this->input->post('telefono');
            $correo=$this->input->post('correo');
            $pagina_web=$this->input->post('paginaweb');
            $antiguedad=$this->input->post('antiguedad');
            $vtas_mensuales_prom=$this->input->post('vtasmensualesprom');
            $ctos_mensuales_prom=$this->input->post('ctosmensualesprom');
            $utilidad_mensual_prom=$this->input->post('utilidadmensualprom');
            $nro_empleados=$this->input->post('nroempleados');

            if ($this->Empresa_model->existerut($rut_empresa) == 0) {

                $datosinsert = array(
                    'USU_ID' => $id_usu,
                    'EMP_RUT' => $rut_empresa,
                    'EMP_RAZON_SOCIAL' => $razon_social,
                    'EMP_NOMBRE_FANTASIA' => $nombre_fantasia,
                   'EMP_RUBRO'            => $rubro,
                   'EMP_TELEFONO'         => $telefono,
                   'EMP_CORREO'           => $correo,
                   'EMP_PAGINA_WEB'       => $pagina_web,
                   'EMP_ANTIGUEDAD'       => $antiguedad,
                   'EMP_VTASMENSUALESPROM' => $vtas_mensuales_prom,
                   'EMP_CTOSMENSUALESPROM' => $ctos_mensuales_prom,
                   'EMP_UTILIDADMENSUALPROM' => $utilidad_mensual_prom,
                   'EMP_NROEMPLEADOS'     => $nro_empleados,

                );
                $datosinsert = $this->security->xss_clean($datosinsert);
                $insertarempresa = $this->Empresa_model->insertar($datosinsert);




                if ($insertarempresa!=false) {
                       $this->session->set_flashdata('Exito2', 'Agregar');
                    redirect('Usuarios/perfil');
                } else {

                       $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                    redirect('Usuarios/perfil');
                }

            } else {

                redirect('Usuarios');
            }
            } else {

                redirect('Welcome');
            }

            }else{
                redirect('Welcome');
            }

    }

    public function checkrut() {
        $rut = $this->input->post("rutempresa");
        $result_codigo = $this->Empresa_model->existerut($rut);



        echo $result_codigo;
    }

    public function mostrar(){

        $config['activo'] = 'perfil';
        $config['img'] = '9';
        $config['animacion'] = '0';

        
        if($this->session->userdata('normal')){

        $id=$this->session->userdata('normal')->IDUSER;
        
        if ($this->Usuarios_model->existe_empresa($id)){
        
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['empresa']=$this->Empresa_model->mostrarEmpresas($id);
        $this->load->view('empresa/editar',$data);
        }else{
            redirect('Empresa/registro');
        }
        }else{
            redirect('Welcome');
        }
            
    }

    public function update()
    {
        if ($_POST) {

            if($this->session->userdata('normal')){
            
            $id_usu = $this->input->post('idusu');
            $rut_empresa = $this->input->post('rutempresa');
            $razon_social = $this->input->post('razonsocial');
            $nombre_fantasia = $this->input->post('nombrefantasia');
            $rubro = $this->input->post('rubro');
            $telefono = $this->input->post('telefono');
            $correo = $this->input->post('correo');
            $pagina_web = $this->input->post('paginaweb');
            $antiguedad = $this->input->post('antiguedad');
            $vtas_mensuales_prom = $this->input->post('vtasmensualesprom');
            $ctos_mensuales_prom = $this->input->post('ctosmensualesprom');
            $utilidad_mensual_prom = $this->input->post('utilidadmensualprom');
            $nro_empleados = $this->input->post('nroempleados');

           
            $datos= array(
            'USU_ID' => $id_usu,
            'EMP_RAZON_SOCIAL' => $razon_social,
            'EMP_NOMBRE_FANTASIA' => $nombre_fantasia,
            'EMP_RUBRO' => $rubro,
            'EMP_TELEFONO' => $telefono,
            'EMP_CORREO' => $correo,
            'EMP_PAGINA_WEB' => $pagina_web,
            'EMP_ANTIGUEDAD' => $antiguedad,
            'EMP_VTASMENSUALESPROM' => $vtas_mensuales_prom,
            'EMP_CTOSMENSUALESPROM' => $ctos_mensuales_prom,
            'EMP_UTILIDADMENSUALPROM' => $utilidad_mensual_prom,
            'EMP_NROEMPLEADOS' => $nro_empleados,

        );
                $datos = $this->security->xss_clean($datos);
             $actualizarinfo=$this->Empresa_model->update($id_usu,$rut_empresa,$datos);

           if ($actualizarinfo) {
                       $this->session->set_flashdata('Exito1', 'Actualizar');
                      redirect('Usuarios/perfil');
                    } else {

                       $this->session->set_flashdata('Fail', 'Se ha producido un error con la base de datos. Intente nuevamente');
                        redirect('Usuarios');
                    }
       
            } else {
                redirect('Welcome');
            }
            } else {
                redirect('Empresa');
            }
           
    }

}

        
        