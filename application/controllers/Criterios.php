<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Criterios extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('Criterios_model');
        $this->load->model('Resultados_model');
        $this->load->model('Usuarios_model');
        $this->load->model('Empresa_model');
        $this->load->library('Pdf');
        $this->load->library('session');
        $this->load->helper('url');
        date_default_timezone_set('America/Santiago');

    }

    public function index()
    {

        redirect('Usuarios/menu_diagnostico');

    }

    
    //Gobierno empresarial y estrategia
    public function DOM1(){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);
        if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
                
        $emp_rut = $this->Criterios_model->getIDempresa($id);       
        $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);        
        $data['lista']=$this->Criterios_model->preguntasxdominio(1,10);
        $data['respuestas']=$this->Criterios_model->respuestasxdominio(1,10);
        //si el dominio no está contestado, puede entrar a la vista
        if(($this->Criterios_model->dom_contestado($ana_id,1))==$ana_id){
        $this->load->view('criterios/dom1',$data);
        }else{
            redirect('Criterios/dominios_completados');
        }
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }
    
    
    
    //Administración y contabilidad
       public function DOM2(){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
       $emp_rut = $this->Criterios_model->getIDempresa($id);
       $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
       $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        $data['lista']=$this->Criterios_model->preguntasxdominio(11,33);
        $data['respuestas']=$this->Criterios_model->respuestasxdominio(11,33);
         //si el dominio no está contestado, puede entrar a la vista
        if(($this->Criterios_model->dom_contestado($ana_id,2))==$ana_id){
        $this->load->view('criterios/dom2',$data);
        }else{
            redirect('Criterios/dominios_completados');
        }
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
        
    }
   // Personas
           public function DOM3(){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

     if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
       $emp_rut = $this->Criterios_model->getIDempresa($id);
       $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
       $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);

        $data['lista']=$this->Criterios_model->preguntasxdominio(34,53);
        $data['respuestas']=$this->Criterios_model->respuestasxdominio(34,53);
        //si el dominio no está contestado, puede entrar a la vista
        if(($this->Criterios_model->dom_contestado($ana_id,3))==$ana_id){
        $this->load->view('criterios/dom3',$data);
        }else{
            redirect('Criterios/dominios_completados');
        }
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
        
    }
    //Modelo de negocios
              public function DOM4(){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

          if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
          $emp_rut = $this->Criterios_model->getIDempresa($id);
          $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
          $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);

        $data['lista']=$this->Criterios_model->preguntasxdominio(54,79);
        $data['respuestas']=$this->Criterios_model->respuestasxdominio(54,79);
        //si el dominio no está contestado, puede entrar a la vista
        if(($this->Criterios_model->dom_contestado($ana_id,4))==$ana_id){
        $this->load->view('criterios/dom4',$data);
        }else{
            redirect('Criterios/dominios_completados');
        }
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
        
    }
    //Procesos
              public function DOM5(){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

         if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
          $emp_rut = $this->Criterios_model->getIDempresa($id);
          $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
          $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);

        $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(80,86);
        $data['respuestas']=$this->Criterios_model->respuestasxdominio(80,86);
        //si el dominio no está contestado, puede entrar a la vista
        if(($this->Criterios_model->dom_contestado($ana_id,5))==$ana_id){
        $this->load->view('criterios/dom5',$data);
        }else{
            redirect('Criterios/dominios_completados');
        }
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
        
    }
    //Medición, análisis y desempeño
                  public function DOM6(){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

     if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
      $emp_rut = $this->Criterios_model->getIDempresa($id);
      $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
      $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);

        $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(87,92);
        $data['respuestas']=$this->Criterios_model->respuestasxdominio(87,92);
       //si el dominio no está contestado, puede entrar a la vista
        if(($this->Criterios_model->dom_contestado($ana_id,6))==$ana_id){
        $this->load->view('criterios/dom6',$data);
        }else{
            redirect('Criterios/dominios_completados');
        }
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
        
    }

    //muestra el primer dominio a desarrollar
                      public function progreso(){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);
        
       if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
        $emp_rut = $this->Criterios_model->getIDempresa($id);
        $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
        
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        $primero=$this->Resultados_model->primer_dominio($ana_id);

        if($primero==1) {
            $data['lista'] = $this->Criterios_model->preguntasxdominio(1, 10);
            $data['respuestas'] = $this->Criterios_model->respuestasxdominio(1, 10);
            
                    if(($this->Criterios_model->dom_contestado($ana_id,1))==$ana_id){
                    $this->load->view('criterios/dom1',$data);
                    }else{
                    redirect('Criterios/dominios_completados');
                    }
        }
          if($primero==2) {
              $data['lista'] = $this->Criterios_model->preguntasxdominio(11, 33);
              $data['respuestas'] = $this->Criterios_model->respuestasxdominio(11, 33);
              
                    if(($this->Criterios_model->dom_contestado($ana_id,2))==$ana_id){
                    $this->load->view('criterios/dom2',$data);
                    }else{
                    redirect('Criterios/dominios_completados');
                    }
          }
          if($primero==3) {
              $data['lista'] = $this->Criterios_model->preguntasxdominio(34, 53);
              $data['respuestas'] = $this->Criterios_model->respuestasxdominio(34, 53);
              
                    if(($this->Criterios_model->dom_contestado($ana_id,3))==$ana_id){
                    $this->load->view('criterios/dom3',$data);
                    }else{
                    redirect('Criterios/dominios_completados');
                    }
          }
          if($primero==4) {
              $data['lista'] = $this->Criterios_model->preguntasxdominio(54, 79);
              $data['respuestas'] = $this->Criterios_model->respuestasxdominio(54, 79);
              
                    if(($this->Criterios_model->dom_contestado($ana_id,4))==$ana_id){
                    $this->load->view('criterios/dom4',$data);
                    }else{
                    redirect('Criterios/dominios_completados');
                    }
          }
          if($primero==5) {
              
              $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(80,86);
              $data['respuestas'] = $this->Criterios_model->respuestasxdominio(80, 86);
              
                    if(($this->Criterios_model->dom_contestado($ana_id,5))==$ana_id){
                    $this->load->view('criterios/dom5',$data);
                    }else{
                    redirect('Criterios/dominios_completados');
                    }
          }
          if($primero==6) {
              $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(87,92);
              $data['respuestas'] = $this->Criterios_model->respuestasxdominio(87, 92);
              
                    if(($this->Criterios_model->dom_contestado($ana_id,6))==$ana_id){
                    $this->load->view('criterios/dom6',$data);
                    }else{
                    redirect('Criterios/dominios_completados');
                    }
          }
          }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function dominios_completados(){

        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

         if($this->session->userdata('normal')){
            
             $id = $this->session->userdata('normal')->IDUSER;
             
            if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
        
        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

     
        $emp_rut = $this->Criterios_model->getIDempresa($id);
        $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);

        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        $data['ana_id_inicial']=$this->Resultados_model->get_anaid_inicial($ana_id);
        $data['ana_id']=$ana_id;
        $this->load->view('criterios/completados',$data);
         }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }
    
    
    
    
     public function insertresultadosxcriterioydominio() {
        if ($_POST) {
            
            
if($this->session->userdata('normal')){
            
            $idrespuesta = explode(",",$this->input->post('id_respuesta'));
            $crit_numero = explode(",",$this->input->post('crit_numero'));
            $puntajexcriterio = explode(",",$this->input->post('puntajexcriterio'));
            $domid = $this->input->post('domid');
            $puntaje = $this->input->post('puntajetotal');
            
                      
            $id = $this->session->userdata('normal')->IDUSER;
            $emp_rut = $this->Criterios_model->getIDempresa($id);
            $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
           
             $insert2 = $this->Criterios_model->resultadosxdominio($ana_id,$domid,$puntaje);
            
            

  
                    for($i=0;$i<count($crit_numero);$i++){
    
                         $datosinsert = array(
                        'ID_RESPUESTA' => $idrespuesta[$i],
                        'ANA_ID' => $ana_id,
                        'CRIT_NUMERO' => $crit_numero[$i],
                        'PUNTAJEXCRITERIO' => $puntajexcriterio[$i],

                    );
                        
   
              $insert = $this->Criterios_model->resultadosxcriterio($datosinsert); 
               
                       
                        
                    }          
                 }else{
                     redirect('Welcome');
                 }
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        
    }


    public function ponderacionxdominio() {
        if ($_POST) {

          if($this->session->userdata('normal')){
            //crea diagnostico
            
            $fecha = date('Y-m-d H:i:s');
            $id = $this->session->userdata('normal')->IDUSER;
            $emp_rut = $this->Criterios_model->getIDempresa($id);          
            
            
            if($this->input->post('inicial_id')){
                $inicial_id = $this->input->post('inicial_id');
                $inicial_fecha = $this->input->post('inicial_fecha');

                $datos_diag = array(
                    'EMP_RUT' => $emp_rut,
                    'USU_ID' => $id,
                    'ANA_FECHA'=> $fecha,
                    'ANA_ID_INICIAL'=>$inicial_id,
                    'ANA_FECHA_INICIAL'=>$inicial_fecha,
                );
                
            }else{
                
            $datos_diag = array(
                    'EMP_RUT' => $emp_rut,
                    'USU_ID' => $id,
                    'ANA_FECHA'=> $fecha,

                ); 
                
            }
                          
                
            $diag = $this->Criterios_model->inicio_diagnostico($datos_diag);
           
            
            //obtiene datos de las ponderaciones
            $dom_id = explode(",",$this->input->post('dom_id'));
            $ponderacion = explode(",",$this->input->post('ponderacion'));
            $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
         

            for($i=0;$i<count($dom_id);$i++){

                $datosinsert = array(
                    'ANA_ID' => $ana_id,
                    'DOM_ID' => $dom_id[$i],
                    'PONDERACION' => $ponderacion[$i],

                );

                //
                $insert = $this->Criterios_model->ponderacionxdominio($datosinsert);

            }
            if($diag && $insert){
                $this->session->keep_flashdata('Exito', 'Actualizar');
                     
             
            }

        }else{
            redirecto('Welcome');
        }
        
        }else{
            redirect('Usuarios/menu_diagnostico');
        }






    }

    public function dominios(){

        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        if($this->session->userdata('normal')) {
            
            $id=$this->session->userdata('normal')->IDUSER;
            if ($this->Usuarios_model->existe_empresa($id)){
            $this->load->view('estructura/menu_superior_usuario', $config);
            $data['lista'] = $this->Criterios_model->mostrar_dominios();
            $emp_rut = $this->Criterios_model->getIDempresa($id);
            $ana_id = $this->Criterios_model->getIDanalisis($id,$emp_rut);
            $data['ana_id']=$ana_id;
            if($this->Resultados_model->continua($ana_id)){
                redirect('usuarios/menu_diagnostico');
            }               

            $this->load->view('criterios/dominios', $data);
        }else{
   
            redirect('Empresa/registro');
        }
        }else{
            redirect('Welcome/login');
        }
    }
    
    
    

    public function eliminar_diagnosticos()
    {
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        if($this->session->userdata('normal')) {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Usuarios_model->existe_empresa_y_analisis($id)){
            
        $this->load->view('estructura/menu_superior_usuario',$config);

        
        $ana_id=$this->Criterios_model->getana_id($id);
        $data['continua']=$this->Resultados_model->continua($ana_id);

        $data['diagnosticos']=$this->Criterios_model->mostrar_diagnosticos($id);

        $this->load->view('criterios/eliminar_diagnosticos',$data);
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome/login');
        }
    }

    public function eliminar($ana_id)
    {
        

        if($this->session->userdata('normal')) {

        $id = $this->session->userdata('normal')->IDUSER;

        if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false)
        {
             $this->Criterios_model->eliminar_criterios($ana_id);

            $this->Criterios_model->eliminar_dominios($ana_id);

            $this->Criterios_model->eliminar_analisis($ana_id);

            $this->session->set_flashdata('registrostatus', 'Diagnostico eliminado');

            redirect('Criterios/eliminar_diagnosticos');

            }else{ 
                redirect('Usuarios/perfil');
        }
        }else{
            redirect('Welcome/login');
        }

    }

    public function mejoramiento()
    {
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        if($this->session->userdata('normal')){
            
           

            $id = $this->session->userdata('normal')->IDUSER;
            
             if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
             $this->load->view('estructura/menu_superior_usuario',$config);
            $ana_id=$this->Criterios_model->getana_id($id);
            $data['continua']=$this->Resultados_model->continua($ana_id);

            $data['diagnosticos']=$this->Criterios_model->mostrar_diagnosticos($id);

            $this->load->view('criterios/diagnosticos_planmejora',$data);
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome/login');
        }
    }

    public function seleccionar_dominio($ana_id)
    {
        
         if($this->session->userdata('normal')){
           

            $id = $this->session->userdata('normal')->IDUSER;
            
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id) ){
            
             if ($this->Usuarios_model->existe_empresa_y_analisis($id)){
                 
             
        $config['activo'] = '';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);

        $data['existe']=$this->Criterios_model->existe_presupuesto2($ana_id);

        $this->load->view('criterios/seleccionar_dominio',$data);

    }else{
        redirect('Usuarios/menu_diagnostico');
    }
    }else{
       redirect('Usuarios/menu_diagnostico');
    }
    }else{
       redirect('Welcome');
    }
    }
    
    public function pdf_plan_mejora($ana_id)
    {
        
        if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id)){
        $data['idana']=$ana_id;
        $data['dominios']=$this->Resultados_model->dominios_seleccionados($ana_id);
        
        $data['empresario'] = $this->Usuarios_model->getUsuarioxId($id);
        $data['empresa'] = $this->Empresa_model->getEmpresas($id);
  $data['fecha'] = $this->Resultados_model->getFecha2($ana_id);
        $this->load->view('criterios/pdf_plan_mejora',$data);
        }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas1($ana_id)
    {
        
         if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,1)){

        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['lista']=$this->Criterios_model->preguntasxdominio(1,10);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(1, 10);
        $data['budget']=$this->Criterios_model->existe_presupuesto($ana_id,1);
        $this->load->view('criterios/respuestas',$data);
        
             }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
        
    }

    public function ver_respuestas2($ana_id)
    {
        
        
                 if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,2)){
                
            
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['lista']=$this->Criterios_model->preguntasxdominio(11,33);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(11,33);
        $data['budget']=$this->Criterios_model->existe_presupuesto($ana_id,2);
        $this->load->view('criterios/respuestas2',$data);
        
                     }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas3($ana_id)
    {
        
        
          if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,3)){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['lista']=$this->Criterios_model->preguntasxdominio(34,53);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(34,53);
        $data['budget']=$this->Criterios_model->existe_presupuesto($ana_id,3);
        $this->load->view('criterios/respuestas3',$data);
                     }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas4($ana_id)
    {
                 if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,4)){
  
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['lista']=$this->Criterios_model->preguntasxdominio(54,79);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(54,79);
        $data['budget']=$this->Criterios_model->existe_presupuesto($ana_id,4);
        $this->load->view('criterios/respuestas4',$data);
                     }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas5($ana_id)
    {
        
             if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,5)){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(80,86);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(80,86);
        $data['budget']=$this->Criterios_model->existe_presupuesto($ana_id,5);
        $this->load->view('criterios/respuestas5',$data);
                     }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function ver_respuestas6($ana_id)
    {
        
        
            if($this->session->userdata('normal'))
        {
            $id = $this->session->userdata('normal')->IDUSER;
            if($this->Criterios_model->consultar_usuario($ana_id,$id)!=false && $this->Resultados_model->analisis_no_terminado($ana_id) && $this->Resultados_model->tiene_respuestas($ana_id,6)){
        $config['activo'] = 'diagnostico';
        $config['img'] = '9';
        $config['animacion'] = '0';

        $this->load->view('estructura/header');
        $this->load->view('estructura/menu_superior_usuario',$config);

        $data['idana']=$ana_id;
        $data['lista']=$this->Criterios_model->preguntasxdominioSinsub(87,92);
        $data['respuestas']=$this->Resultados_model->respuestas_contestadas($ana_id);
        $data['respuestas2'] = $this->Criterios_model->respuestasxdominio(87,92);
        $data['budget']=$this->Criterios_model->existe_presupuesto($ana_id,6);
        $this->load->view('criterios/respuestas6',$data);
        
                     }else{
            redirect('Usuarios/menu_diagnostico');
        }
        }else{
            redirect('Welcome');
        }
    }

    public function insert_mejoramiento()
    {
        if ($_POST) {

             if($this->session->userdata('normal')){
            
            $accion = explode("-,",$this->input->post('accion'));
            $presupuesto = explode(",",$this->input->post('presupuesto'));
            $plazo = explode(",",$this->input->post('plazo'));
            $responsable = explode("-,",$this->input->post('responsable'));
            $crit_numero = explode(",",$this->input->post('crit_numero'));
            $terminado = explode(",",$this->input->post('terminado'));
            $idana=$this->input->post('idana');
            $id_dom=$this->input->post('dominio');


            $id = $this->session->userdata('normal')->IDUSER;

            $budget=0;
            for($i=0;$i<count($crit_numero);$i++) {

                $insert = $this->Criterios_model->insert_mejoramiento($idana, $crit_numero[$i], $accion[$i], $presupuesto[$i], $plazo[$i], $responsable[$i], $terminado[$i]);
                $budget=$budget+$presupuesto[$i];

            }

                $insert2=$this->Criterios_model->insert_presupuesto($idana,$id_dom,$budget);



        }else{
            redirect('Welcome');
        }
    }else{
        redirect('Usuarios/menu_diagnosticos');
    }




}
}