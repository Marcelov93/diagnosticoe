<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('session');
//        $this->load->library('email');
        $this->load->helper('url');
        $this->load->model('Usuarios_model');
    }

    public function index() {


        if ($this->session->userdata('admin')) {
            redirect('Admin');
        } else {
                              $this->load->view('login');

        }
    }

    public function recibirdatos() {


        if ($_POST) {
            $rut = $this->input->post('rut');
            $clave = sha1($this->input->post("clave"));
            

            if ($usuario = $this->Usuarios_model->login($rut, $clave)) {
                
                $iduser = $usuario->USU_ID;
                $rut = $usuario->USU_RUT;
                $nombres = $usuario->USU_NOMBRES;
                $apellidos = $usuario->USU_APELLIDOS;
                $correo = $usuario->USU_CORREO;
                $password = $usuario->USU_PASSWORD;
                $estado = $usuario->USU_ESTADO;
                
               // $tipo = $usuario->USU_TIPO;
                //$avatar = $usuario->AVATAR;


                $datosUsario = (object) array(
                            'IDUSER' => $iduser,
                            'RUT' => $rut,
                            'NOMBRES' => $nombres,
                            'APELLIDOS' => $apellidos,
                            'CORREO' => $correo,                               
                            'ESTADO' => $estado);

 $this->session->set_userdata('normal', $datosUsario);      
            }else {

                $this->session->set_flashdata('registrostatus', 'Fail');
                redirect('Welcome');
            }
        } else {
            redirect('Welcome');
        }
    }

    public function salir() {
        $this->session->sess_destroy();

        redirect('Welcome');
    }

}

