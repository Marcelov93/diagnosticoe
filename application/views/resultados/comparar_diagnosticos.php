<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<?php 

//PUNTAJES PRIMER DIAGNOSTICO
$i=0;
$suma=0;
foreach ($dominios as $dom){
    
         /*PUNTAJE PREGUNTAS OMITIDAS--------------- */        
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom->ANA_ID, $dom->DOM_ID);
        if(isset($puntajeOmitido)){                    
        $puntajeMaximo=$dom->DOM_PONDERACION-$puntajeOmitido;
        }else{
        $puntajeMaximo=$dom->DOM_PONDERACION;}
               
       /*!- PUNTAJE PREGUNTAS OMITIDAS------------ */
        
     
    $puntajes1[$i]=round(((($dom->PUNTAJEXDOMINIO)*100)/$puntajeMaximo),2);
    $nombre1[$i]=$dom->DOM_NOMBRE;

    $puntaje=$dom->PUNTAJEXDOMINIO;

    $ponderacion=$dom->PONDERACION/100;

    $suma=$suma+($puntaje*$ponderacion*100)/$puntajeMaximo;
    
    $nombres[$i]=$dom->DOM_NOMBRE;
    $i++;
}

//PUNTAJES SEGUNDO DIAGNOSTICO
$j=0;
$suma2=0;
foreach ($dominios2 as $dom1){
     
    
       /*PUNTAJE PREGUNTAS OMITIDAS--------------- */        
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom1->ANA_ID, $dom1->DOM_ID);
        if(isset($puntajeOmitido)){                    
        $puntajeMaximo=$dom1->DOM_PONDERACION-$puntajeOmitido;
        }else{
        $puntajeMaximo=$dom1->DOM_PONDERACION;}
               
       /*!- PUNTAJE PREGUNTAS OMITIDAS------------ */
        
    $puntajes2[$j]=round(((($dom1->PUNTAJEXDOMINIO)*100)/$puntajeMaximo),2);
    $nombre2[$j]=$dom1->DOM_NOMBRE;

    $puntaje2=$dom1->PUNTAJEXDOMINIO;

    $ponderacion2=$dom1->PONDERACION/100;

    $suma2=$suma2+($puntaje2*$ponderacion2*100)/$puntajeMaximo;
    
    $j++;
}


//FECHAS
foreach ($fecha1 as $f1){
    $fecha[0]=$f1->ANA_FECHA;
}
foreach ($fecha2 as $f2){
    $fecha[1]=$f2->ANA_FECHA;
}

?>

<div class="contenedor">
    <br>
    <div class="col-md-9"></div>
    <a href="<?=base_url()?>Resultados/pdf_reevaluacion/<?=$idana1?>/<?=$idana2?>" target="_blank"  class="btn btn-info"><i class="glyphicon glyphicon-print"></i> Imprimir resultados </a>
    <br>
    <h2>Comparación Por Dominio</h2>
<div class="contenedor-mostrar-empresa">
    
    <br>
    <br>

<table id="tabla" class="table table-striped">
    <thead class="inverse">
        <tr>
            
            <th><h3>Fecha Diagnostico</h3></th>
            <th ><h3 style="text-align: left" ><?=date("d-m-Y", strtotime($fecha[0]))?></h3></th>
            <th><h3 style="text-align: left"><?=date("d-m-Y", strtotime($fecha[1]))?></h3></th>          
        </tr>
    </thead>
    <tbody>
        


        <?php
         echo "<tr>";     
         for($j=0;$j<$i;$j++){
             
         echo  "<th>".$nombres[$j]." </th>";
//------------------------------------------------------
          if($puntajes1[$j]>67){
        echo '<td class=""><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span> '.$puntajes1[$j].' %</td>';
            }else if($puntajes1[$j]>33 &&$puntajes1[$j]<=67){
                echo '<td class=""><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span> '.$puntajes1[$j].' %</td>';
            }else if($puntajes1[$j]>=0 &&$puntajes1[$j]<=33){
                echo '<td class=""><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span> '.$puntajes1[$j].' %</td>';
            }
         
//---------------------------------------------------------  
                      if($puntajes2[$j]>67){
        echo '<td class=""><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span> '.$puntajes2[$j].' %</td>';
            }else if($puntajes2[$j]>33 &&$puntajes2[$j]<=67){
                echo '<td class=""><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span> '.$puntajes2[$j].' %</td>';
            }else if($puntajes2[$j]>=0 &&$puntajes2[$j]<=33){
                echo '<td class=""><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span> '.$puntajes2[$j].' %</td>';
            }

           echo "</tr>";
           }
        if ($i % 2 == 0){
            echo '<th><h3 style="color:black">Cumplimiento Total</h3></th>';
        }else{
            echo '<th><h3>Cumplimiento Total</h3></th>';
        
        }
        
           if((round($suma,2))>67){
        echo '<td class=""><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span><strong> '.round($suma,2).' %</strong></td>';
            }else if((round($suma,2))>33 && (round($suma,2))<=67){
                echo '<td class=""><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span><strong> '.round($suma,2).' %</strong></td>';
            }else if((round($suma,2))>=0 && (round($suma,2))<=33){
                echo '<td class=""><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span><strong> '.round($suma,2).' %</strong></td>';
            }
            
           if((round($suma2,2))>67){
        echo '<td class=""><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span><strong> '.round($suma2,2).' %</strong></td>';
            }else if((round($suma2,2))>33 && (round($suma2,2))<=67){
                echo '<td class=""><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span><strong> '.round($suma2,2).' %</strong></td>';
            }else if((round($suma2,2))>=0 && (round($suma2,2))<=33){
                echo '<td class=""><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span><strong> '.round($suma2,2).' %</strong></td>';
            }
            
           
        ?>   
</table>
    <br>
         <li class="list-group-item list-group-item-danger col-md-4"><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span>De 0 a 33 el dominio se encuentra desarrollado a nivel básico</li>
            <li class="list-group-item list-group-item-warning col-md-4"><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span>De 34 a 67 el dominio se encuentra desarrollado a nivel intermedio</li>
            <li class="list-group-item list-group-item-success col-md-4"><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>De 68 a 100 el dominio se encuentra desarrollado a nivel avanzado</li>
     
<!-- TABLA OCULTA PARA LLENAR GRAFICOS -->
    <table id="datatable" class="table table-striped">
    <thead>
        <tr>
            
            <th><h3>Fecha Diagnostico</h3></th>
            <th><h4><?=date("d-m-Y", strtotime($fecha[0]))?></h4></th>
            <th><h4><?=date("d-m-Y", strtotime($fecha[1]))?></h4></th>          
        </tr>
    </thead>
    <tbody>
        


        <?php
         echo "<tr>";     
         for($j=0;$j<$i;$j++){
             
         echo  "<th>".$nombres[$j]."</th>";
//------------------------------------------------------

          echo "<td>".$puntajes1[$j]."</td>";
         
//---------------------------------------------------------  

          echo "<td>".$puntajes2[$j]."</td>";

           echo "</tr>";
           }
        ?>
        
     
     

        
</table>

<!--    <h4 style="color:white;" class="col-md-8">Porcentaje de Cumplimiento Total: <?=round($suma,2)?>%</h4>
    <h4 style="color:white;">Porcentaje de Cumplimiento Total: <?=round($suma2,2)?>%</h4>
    <br>
    <br>-->

    
</div>
</div>
    
    <br>
    <br>
    
    <div id="container" class="contenedor-mostrar-empresa" style="min-width: 310px; height: 400px; margin: 0 auto">

    
<script>    
    $(function () { 
Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Comparación Diagnosticos'
    },
    yAxis: {
        allowDecimals: false,
        max: 100,
        min: 0,
        title: {
            text: 'Porcentaje de Cumplimiento'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }   
});   
    });
    
 </script>
    
</div>

<br>
<br>
<div class="contenedor-mostrar-empresa">

<?php

foreach($dominios as $dom){
$anaid1=$dom->ANA_ID;}
foreach($dominios2 as $dom2){
$anaid2=$dom2->ANA_ID;}
?>
    
    <div class="col-md-10">
        <a href="<?=base_url()?>Resultados/medicion_impacto/<?=$anaid1?>/<?=$anaid2?>" class="btn btn-danger col-md-12">MEDICIÓN DE IMPACTO</a>
    </div>

    <a id="back" class="btn btn-success col-md-2">VOLVER</a>
    
</div>
<br>
<br>

<script>
$(document).ready(function() {
       

$('#back').click(function(){
		parent.history.back();
		return false;
	});
        
        
 $('#datatable').hide(); 
});


</script>






