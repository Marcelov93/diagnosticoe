<style>
    body {
        width: 100%;
        height:100%;
        font-family: 'Open Sans', sans-serif;
        background-image: url("<?=base_url()?>assets/css/wood_1.png");
        /*        background-image: url("padded.png");*/
        /*        background-image: url("stardust.png");
                background-image: url("tweed.png");*/

    }

    .contenedor h2 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
    .contenedor h1 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }

    .contenedor-perfil{
        width:50%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }


</style>



<div class="contenedor">

    <br>
    <br>
    <br>
    <br>
    <h1>SELECCIONE LOS DIAGNÓSTICOS A COMPARAR</h1>
    <br>
    <br>
</div>


<div class="contenedor-perfil">

        <?php  $i=1 ?>
        <?php foreach ($diagnosticos as $value){?>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <a href="<?php echo base_url('Resultados/comparar_diagnosticos')."/".$value->ANA_ID_INICIAL."/".$value->ANA_ID; ?>"><li id="<?=$i?>" class="list-group-item" value="<?php echo $value->ANA_ID;  ?>"><?php echo $i?>.- <?php echo '<strong>Diagnóstico Inicial: '.date("d-m-Y", strtotime($value->ANA_FECHA_INICIAL)).'</strong>'  ?> 
                        <?php echo str_repeat('&nbsp;', 5);?><?php echo '<strong>Diagnóstico Posterior: '.date("d-m-Y", strtotime($value->ANA_FECHA)).'</strong>'  ?></li></a>
                </div>
            </div>
            <br>

            <?php  $i++;}
        $total=$i-1?>

        <br>
        <br>
    <div class="col-md-10"></div>

    <a id="back" class="btn btn-success col-md-2">VOLVER</a>

</div>



<script>

    $(document).ready(function() {

        $('#back').click(function(){
            parent.history.back();
            return false;
        });
    });

</script>

