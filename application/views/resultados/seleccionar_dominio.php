

<style>

    @import url(http://fonts.googleapis.com/css?family=Open+Sans);

    html { width: 100%; height:100%; overflow:auto; }

    body {
        width: 100%;
        height:100%;
        font-family: 'Open Sans', sans-serif;
        background-image: url("<?=base_url()?>assets/css/wood_1.png");
        /*        background-image: url("padded.png");*/
        /*        background-image: url("stardust.png");
                background-image: url("tweed.png");*/

    }


    .contenedor h2 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
    .contenedor h1 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }

    .contenedor2 h3 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
    .contenedor2 h2 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
    .contenedor2 h1 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }

    input[readonly] {
        background-color: white !important;
        cursor: text !important;
    }


    label{
        color:#F0FFFF;
    }



    .contenedor-mostrar-empresa{
        width:50%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }


    input:focus { box-shadow: inset 0 -5px 45px rgba(100,100,100,0.4), 0 1px 1px rgba(255,255,255,0.2); }

</style>

<div class="contenedor">
     <br>
    <br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <h2>SELECCIONE UN DOMINIO</h2>
        </div>

    </div>
    <br>
    <br>



    <!-- Main content -->

        <div class="contenedor-mostrar-empresa ">

            <!--<div class="col-md-12">-->
            <?php foreach ($dominios as $value){

                ?>

                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <a href="<?=base_url()?>Resultados/ver_respuestas<?=$value->DOM_ID.'/'.$idana?>" ><li class="list-group-item">
                                <i class="glyphicon glyphicon-chevron-right"> </i><strong> <?php echo $value->DOM_NOMBRE;  ?> </strong></li></a>
                    </div>

                </div>

                <br>
                <?php } ?>
            <br>

            <br>


            <div class="col-md-10 col-sm-10"></div>

            <div class="col-md-2 col-sm-2">
                <a id="back" class="btn btn-success col-md-12 col-sm-12">VOLVER</a>
            </div>

            <br>

            <br>
            <br>

            <br>

    <!--</div>-->
</div>

    <script>

        $(document).ready(function() {

            $('#back').click(function(){
                parent.history.back();
                return false;
            });
        });

    </script>