<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">


<div class="contenedor">
<!--    <ol class="breadcrumb">


        <?php $total=0;
        //el valor actual es el id del dominio
        $actual=1;?>
        <?php foreach ($dominios as $dom){

            ?>
            <li><?=$dom->DOM_NOMBRE?></li>

        <?php
            if($dominios[$total]->DOM_ID==$actual){
                $aux=$total;
            }

            $total=$total+1;}
        //si el valor actual es el ultimo o el unico
        if($dominios[$total-1]->DOM_ID==$actual){
            $siguiente='Criterios/dominios_completados';
        }else{
            //dominios[aux+1] es el siguiente dominio a responder
            $next=$dominios[$aux+1]->DOM_ID;
            $str="$next";
            $siguiente='Criterios/dom'.$str;

        }
        ?>



    </ol>-->
       <br>
    <br>
    <h1>Medición de Impacto</h1>
    <br>
    <h2>Dominio: <?=$dominio?></h2>
    <br>
    <br>



    <!-- Main content -->
    

    <div class="contenedor-mostrar-empresa">
<form id="dom1">
            <?php foreach ($lista as $value){ 
                $i=1;?>
       <!-- preguntas -->  
       
       <div class="pregunta">
        <div class="list-group col-md-12">
 <a id="resp"  class="list-group-item active">
     <p class="list-group-item-text">  <?php echo $value->CRIT_NUMERO;  ?>.- <?php echo $value->CRIT_PREGUNTA;  ?>  </p>

</a>
  </div>    
        <div class="list-group col-md-4">
 <a id="resp"  class="list-group-item list-group-item-warning active">
     <p class="list-group-item-text">  EVALUACION INICIAL</p>
</a>
                </div>    
        <div class="list-group col-md-4">
 <a id="resp"  class="list-group-item list-group-item-warning active">
     <p class="list-group-item-text">  EVALUACION POSTERIOR</p>
</a>
                </div>    
        <div class="list-group col-md-4">
 <a id="resp"  class="list-group-item list-group-item-warning active">
     <p class="list-group-item-text">  IMPACTO</p>
</a>
                </div>    

   
      <?php foreach ($respuestas1 as $resp1){
       ?>
   
   <!-- ----------------------------------------------  -->
   <?php if($value->CRIT_NUMERO==$resp1->CRIT_NUMERO){ ?>
   <?php
       
       if($resp1->RESP_PONDERACION==0){$titulo='NO DESARROLLADO';}else{
           if($resp1->RESP_PONDERACION==1){$titulo='ESCASAMENTE DESARROLLADO ';} else {
               if($resp1->RESP_PONDERACION==2){$titulo='PARCIALMENTE DESARROLLADO';}else{
                   if($resp1->RESP_PONDERACION==3){$titulo='AMPLIAMENTE DESARROLLADO';}
               }
}
       }
       ?>
   <!-- respuestas -->
   <div  class="list-group col-md-4" value="<?=$resp1->CRIT_NUMERO?>">
  <a id="<?=$resp1->ID_RESPUESTA;?>"  class="list-group-item" value="<?=$resp1->RESP_PONDERACION?>">
      <h4 class="list-group-item-heading"><?=$titulo?></h4>
      <br>
    <p class="list-group-item-text">
                    <?php echo $resp1->RESP_DESCRIPCION; ?>
</p>

                    
  </a>
</div>

<?php 
//si el contador no cambia significa que no se encontro respuesta
$i=$i+1;
$ponderacionInicial=$resp1->RESP_PONDERACION;

                   } ?>



                    
            
                <?php }//fin foreach respuestas1 ?>
   <?php
if ($i==1){
      echo '   <div  class="list-group col-md-4">
  <a class="list-group-item">
      <h4 class="list-group-item-heading">No evaluado</h4>              
  </a>
</div>';
}  ?>
      <!-- ----------------------------------------------  -->
   
      
     
 <!-- EVALUACION POSTERIOR -->
  <?php $j=1;?>
      <?php foreach ($respuestas2 as $resp2){
       ?>
   
   <!-- ----------------------------------------------  -->
   <?php if($value->CRIT_NUMERO==$resp2->CRIT_NUMERO){ ?>
   <?php
       
       if($resp2->RESP_PONDERACION==0){$titulo='NO DESARROLLADO';}else{
           if($resp2->RESP_PONDERACION==1){$titulo='ESCASAMENTE DESARROLLADO ';} else {
               if($resp2->RESP_PONDERACION==2){$titulo='PARCIALMENTE DESARROLLADO';}else{
                   if($resp2->RESP_PONDERACION==3){$titulo='AMPLIAMENTE DESARROLLADO';}
               }
}
       }
       ?>
   <!-- respuestas -->
   <div  class="list-group col-md-4" value="<?=$resp2->CRIT_NUMERO?>">
  <a id="<?=$resp2->ID_RESPUESTA;?>"  class="list-group-item" value="<?=$resp2->RESP_PONDERACION?>">
      <h4 class="list-group-item-heading"><?=$titulo?></h4>
      <br>
    <p class="list-group-item-text">
                    <?php echo $resp2->RESP_DESCRIPCION; ?>
</p>

                    
  </a>
</div>

<?php

//si el contador no cambia significa que no se encontro respuesta
$j=$j+1; 

$ponderacionPosterior=$resp2->RESP_PONDERACION;}
?>
                    
            
                <?php }//fin foreach respuestas2 ?>
   <?php
if ($j==1){
      echo '   <div  class="list-group col-md-4">
  <a class="list-group-item">
      <h4 class="list-group-item-heading">No evaluado</h4>              
  </a>
</div>';
}  ?>
 

 <!-- /EVALUACION POSTERIOR -->
 
 <!-- IMPACTO -->
 
 <?php
       if($j==1 || $i==1){
      echo' <div  class="list-group col-md-4">
           <a class="list-group-item list-group-item-info">
               <h4 class="list-group-item-heading">Una de las evaluaciones no fue realizada</h4>              
           </a>
       </div>';        
       }else{
           if($ponderacionInicial<$ponderacionPosterior){
               echo' <div  class="list-group col-md-4">
           <a class="list-group-item list-group-item-success active">
               <h4 class="list-group-item-heading">Mejoró</h4>              
           </a>
       </div>';        
           } else {
               if($ponderacionInicial==$ponderacionPosterior){
                   echo' <div  class="list-group col-md-4">
           <a class="list-group-item">
               <h4 class="list-group-item-heading">Se mantuvo</h4>              
           </a>
       </div>';
                   
               }else{
                   if($ponderacionInicial>$ponderacionPosterior){
                       echo' <div  class="list-group col-md-4">
           <a class="list-group-item list-group-item-danger active">
               <h4 class="list-group-item-heading">Empeoró</h4>              
           </a>
       </div>';
                       
                       
                   }
               }
}
       }
 
 ?>
 
 
 
 
 <!-- /IMPACTO -->

 
 
 
 
 
         <?php }//fin foreach lista ?>          
       </div>

<!--        <div class="col-sm-12 col-md-12">
            <button type="submit" name="btnenviar" id="btnenviar" class="btn botonesgestion " style="width: 100% " >Continuar</button>
            <br>
            <br>
        </div>-->


                
    </form>


    </div>
    <!-- /.content -->

</div>


<br>
<br>
<div class="contenedor">

    
    <div class="col-md-10"></div>
    <a id="back" class="btn btn-success col-md-2">VOLVER</a>

</div>
    <br>
<br>

<script>
$(document).ready(function(){
	$('#back').click(function(){
		parent.history.back();
		return false;
	});
});    
</script>
    