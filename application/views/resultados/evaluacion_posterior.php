<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">


<style>

    .contenedor-perfil{
        width:50%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;
        
    }
.contenedor-perfil h3 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
</style>



<div class="contenedor">

    <br>
    <br>
    <h1>Evaluación Posterior</h1>
    <br>
    <br>
</div>
<div class="contenedor-perfil">

    <a class="resultado" href="<?php echo base_url('Resultados/ver_resultado')."/".$ana_id?>"><li  class="opcion list-group-item">
            <i class="glyphicon glyphicon-chevron-right"> </i><strong> VER RESULTADOS ANALISIS INICIAL</strong></li></a>
    <br>
    <a class="respuestas"><li  class=" opcion list-group-item"><i class="glyphicon glyphicon-chevron-right"> </i><strong> VER RESPUESTAS CONTESTADAS EN ANALISIS INICIAL</strong></li></a>
    <br>
    <br>

 

   
    <form id="iniciar_diag">
    <div id="respuestas_inicial" class="col-md-12">
        <h3>Seleccione un dominio para ver respuestas</h3><br>
       <?php foreach ($dominios as $dom){
        
           echo'    <div class="list-group col-md-6">
 <a href="'.base_url().'Resultados/ver_respuestas'.$dom->DOM_ID.'/'.$ana_id.'"  class="list-group-item list-group-item-success active">
     <p class="list-group-item-text"> '.$dom->DOM_NOMBRE.'</p></a></div>';
        }?>
        
    </div>

 <script>
$('#respuestas_inicial').hide();
</script>   
<div class="contenedor">

    
    <div class="col-md-10">
    <button type="submit" id="iniciar" class="btn btn-warning col-md-12">INICIAR DIAGNOSTICO (Reevaluación)</button></div>
    <a id="back" class="btn btn-success col-md-2">VOLVER</a>


</div>
    </form>
</div>


<br>
<br>




<script>

$(document).ready(function() {
       
       
  $( ".respuestas" ).click(function(){    
  $('#respuestas_inicial').toggle();

});

$('#back').click(function(){
		parent.history.back();
		return false;
	});




$('#iniciar_diag').on('submit', function (e) {
     $(this).find(':input[type=submit]').prop('disabled', true);
    

var id_dom=[];
var pond=[];
var inicial_id=[];
var inicial_fecha='<?php echo $fecha_id?>';
<?php
foreach($dominios as $dom){
            
echo 'id_dom.push('.$dom->DOM_ID.');';
echo 'pond.push('.$dom->PONDERACION.');';
        
}
echo 'inicial_id='.$ana_id.';';
//echo 'inicial_fecha='.$fecha_id;
?>




var form_data = new FormData();



            form_data.append('dom_id',id_dom );
            form_data.append('ponderacion',pond );
            form_data.append('inicial_id',inicial_id );
            form_data.append('inicial_fecha',inicial_fecha);



            var url = "<?php echo base_url(); ?>Criterios/ponderacionxdominio";
            $.ajax({

                type: "POST",
                url: url,
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (php) {
                    localStorage.setItem('enviado', '1');

                    window.location.href = "<?= base_url() ?>Criterios/progreso";


                }
            });//ajax
            
     
            return false;

        });//onsubmit





    });//docready


</script>