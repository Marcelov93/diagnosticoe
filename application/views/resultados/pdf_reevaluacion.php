<?php


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);



// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Resultados comparativos reevaluación', "Resultados diagnóstico - mipeUp.cl\n ");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
$pdf->SetFont('helvetica', '', 11, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// Set some content to print





//PUNTAJES PRIMER DIAGNOSTICO
$i=1;
$suma=0;
foreach ($dominios1 as $dom){
    
         /*PUNTAJE PREGUNTAS OMITIDAS--------------- */        
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom->ANA_ID, $dom->DOM_ID);
        if(isset($puntajeOmitido)){                    
        $puntajeMaximo=$dom->DOM_PONDERACION-$puntajeOmitido;
        }else{
        $puntajeMaximo=$dom->DOM_PONDERACION;}
               
       /*!- PUNTAJE PREGUNTAS OMITIDAS------------ */
        
     
    $puntajes1[$i]=round(((($dom->PUNTAJEXDOMINIO)*100)/$puntajeMaximo),2);
    $nombre1[$i]=$dom->DOM_NOMBRE;

    $puntaje=$dom->PUNTAJEXDOMINIO;

    $ponderacion=$dom->PONDERACION/100;

    $suma=$suma+($puntaje*$ponderacion*100)/$puntajeMaximo;
    
    $nombres[$i]=$dom->DOM_NOMBRE;
    $i++;
}

//PUNTAJES SEGUNDO DIAGNOSTICO
$j=1;
$suma2=0;
foreach ($dominios2 as $dom1){
     
    
       /*PUNTAJE PREGUNTAS OMITIDAS--------------- */        
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom1->ANA_ID, $dom1->DOM_ID);
        if(isset($puntajeOmitido)){                    
        $puntajeMaximo=$dom1->DOM_PONDERACION-$puntajeOmitido;
        }else{
        $puntajeMaximo=$dom1->DOM_PONDERACION;}
               
       /*!- PUNTAJE PREGUNTAS OMITIDAS------------ */
        
    $puntajes2[$j]=round(((($dom1->PUNTAJEXDOMINIO)*100)/$puntajeMaximo),2);
    $nombre2[$j]=$dom1->DOM_NOMBRE;

    $puntaje2=$dom1->PUNTAJEXDOMINIO;

    $ponderacion2=$dom1->PONDERACION/100;

    $suma2=$suma2+($puntaje2*$ponderacion2*100)/$puntajeMaximo;
    
    $j++;
}









$html= '<h1>Resultados comparativos reevaluación</h1><br>';
$fechaNormal1 = date("d-m-Y", strtotime($fecha1));
$fechaNormal2 = date("d-m-Y", strtotime($fecha2));
$html .= '<strong>Fecha análisis inicial: ' .$fechaNormal1.'</strong><br>';
$html .= '<strong>Fecha análisis posterior: ' .$fechaNormal2.'</strong><br><br>';

$html .="<strong>EMPRESARIO: </strong>".$empresario->USU_NOMBRES.' '.$empresario->USU_APELLIDOS.'<br>';
$html .="<strong>EMPRESA: </strong>".$empresa->EMP_NOMBRE_FANTASIA."<br><br><br>";

$html .= '<table>';    
$html .= '<thead>
    <tr >
      <th width="50%"><h4>Area o Dominio</h4> </th>
      <th width="25%" align="center"><h4>Porcentaje cumplimiento inicial<br></h4></th>
      <th width="25%" align="center"><h4>Porcentaje cumplimiento posterior<br></h4></th>

    </tr>
  </thead>
  <tbody>';

for($i=1;$i<$j;$i++){


    $html .="<tr>";
    $html .='<td width="50%">'.$i.".-".$nombre1[$i]."</td>";



    $html .='<td width="25%" align="center">'.$puntajes1[$i]."%</td>";
    $html .='<td  width="25%" align="center">'.$puntajes2[$i]."%</td>";
        $html .="</tr>";
 
     
}

       $html .="<br><tr>";
        $html .='<td width="50%" > <strong>Porcentaje Cumplimiento TOTAL</strong></td>';
        $html .='<td width="25%" align="center">'.round($suma,2)."%</td>";
        $html .='<td width="25%" align="center">'.round($suma2,2)."%</td>";
        $html .="</tr>";
        
        
        
$html .= '</tbody></table>';    
$html .= '<br><br><hr>';



       
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('plan_de_mejora.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
