<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">


<style>

    .contenedor-perfil{
        width:50%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }

</style>

<div class="contenedor">
    <br>
    <br>
    <br>
    <br>
    <h1>SELECCIONE UN DIAGNÓSTICO PARA REALIZAR LA EVALUACION POSTERIOR</h1>
    <br>
    <br>
    <br>
</div>
<div class="contenedor-perfil">
    
    <?php  $i=1;
            
            ?>
    <?php foreach ($diagnosticos as $value){

    if($continua!=$value->ANA_ID ){ /*si hay un diagnostico sin completar, no lo muestra */
        ?>
    <a class="diag" value='<?=$value->ANA_ID?>' ><li class="opcion list-group-item"><i class="glyphicon glyphicon-chevron-right"> </i><strong> <?php echo $i.'.- '.date("d-m-Y", strtotime($value->ANA_FECHA))?> </strong></li></a>
    <br>
   
    
<div id="<?=$value->ANA_ID?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

     Modal content
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Atención !</h4>
      </div>
      <div class="modal-body">
        <p><i class="glyphicon glyphicon-chevron-right"> </i> En el análisis seleccionado se evaluaron los siguientes dominios: <!--<?=$value->ANA_ID?>--></p>
       <br>
        <?php 
        $dominios=$this->Resultados_model->dominios_seleccionados($value->ANA_ID);
        foreach($dominios as $dom){
            
            echo 
            '<a><li class="list-group-item">'.$dom->DOM_NOMBRE.'<span class="badge">Ponderación: '.$dom->PONDERACION .'%</span></li></a><br>';
            
        }?>
       
       <p><strong>*En este nuevo análisis se reevaluaran estos dominios con las mismas ponderaciones seleccionadas anteriormente</strong></p>
       <br>
       <p>¿Desea realizar una evaluación posterior de este análisis?</p>
      </div>
      <div class="modal-footer">
        <button onclick="javascript:window.location='<?=base_url('Resultados/evaluacion_posterior')."/".$value->ANA_ID?>'" type="button" class="btn btn-success" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>
    
    
    
    
    
    
    <?php  $i++;}} ?>
    
    
    
    

<div class="contenedor">

    
    <div class="col-md-10"></div>
    <a id="back" class="btn btn-success col-md-2">VOLVER</a>

</div>



</div>


<br>
<br>

 <script>

$(document).ready(function() {
       
  $( ".diag" ).click(function(){
  var id = $(this).attr("value");
  $('#'+id+'').modal('show');
});

$('#back').click(function(){
		parent.history.back();
		return false;
	});
});


 $('.opcion').hover(
function(){$(this).toggleClass('active');}
);  

</script>

