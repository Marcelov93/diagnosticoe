<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>




<!------------------------->

<?php $suma=0;

foreach ($dominios as $dom){
    
             /*PUNTAJE PREGUNTAS OMITIDAS--------------- */        
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom->ANA_ID, $dom->DOM_ID);
        if(isset($puntajeOmitido)){                    
        $puntajeMaximo=$dom->DOM_PONDERACION-$puntajeOmitido;
        }else{
        $puntajeMaximo=$dom->DOM_PONDERACION;}
               
       /*!- PUNTAJE PREGUNTAS OMITIDAS------------ */
    
    $puntaje=$dom->PUNTAJEXDOMINIO;

    $ponderacion=$dom->PONDERACION/100;

    $suma=$suma+($puntaje*$ponderacion*100)/$puntajeMaximo;

}

?>

<div class="contenedor">
    <br>
    <div class="col-md-9"></div>
    <a href="<?=base_url()?>Resultados/pdf_resultados/<?=$idana?>" target="_blank"  class="btn btn-info"><i class="glyphicon glyphicon-print"></i> Imprimir resultados </a>
    <h1>RESULTADOS</h1>
    <br>
    <br>
    <h2>Porcentaje de Cumplimiento Total: <?=round($suma,2)?>%</h2>
    <br>



        <div class="contenedor-mostrar-empresa">
            
            
<table class="table table-striped ">
  <thead>
    <tr >
      <th style="color:white">Area o Dominio </th>
      <th style="color:white">Porcentaje Cumplimiento</th>

    </tr>
  </thead>
  <tbody>

      <?php foreach ($dominios as $dom) {
          ?>

    <tr>
        <td ><strong><?=$dom->DOM_NOMBRE ?></strong></td>
        <?php
/*PUNTAJE PREGUNTAS OMITIDAS */
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom->ANA_ID, $dom->DOM_ID);
        if(isset($puntajeOmitido)){
        $puntajeMaximo=$dom->DOM_PONDERACION-$puntajeOmitido;
        $score=round((($dom->PUNTAJEXDOMINIO*100)/$puntajeMaximo),2);

            if($score>67){
        echo '<td class=""><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span> '.$score.'%</td>';
            }else if($score>33 &&$score<=67){
                echo '<td class=""><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span> '.$score.'%</td>';
            }else if($score>=0 &&$score<=33){
                echo '<td class=""><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span> '.$score.'%</td>';
            }
        }else{
            $score=round((($dom->PUNTAJEXDOMINIO*100)/$dom->DOM_PONDERACION),2);
            if($score>67){
                echo '<td class=""><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span> '.$score.'%</td>';
            }else if($score>33 &&$score<=67){
                echo '<td class=""><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span> '.$score.'%</td>';
            }else if($score>=0 &&$score<=33){
                echo '<td class=""><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span> '.$score.'%</td>';
            }
 }?>


    </tr>
      <?php } ?> 
  </tbody>
</table>
<br>

           <li class="list-group-item list-group-item-danger col-md-4"><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span>De 0 a 33 el dominio se encuentra desarrollado a nivel básico</li>
            <li class="list-group-item list-group-item-warning col-md-4"><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span>De 34 a 67 el dominio se encuentra desarrollado a nivel intermedio</li>
            <li class="list-group-item list-group-item-success col-md-4"><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>De 68 a 100 el dominio se encuentra desarrollado a nivel avanzado</li>
     
            <br>
            <br>
        </div>

    <br>
    <br>
</div>





<!------------------------->

<div class="contenedor-mostrar-empresa" id="container">
    
  yAxis: {}
    
<script>
$(function () { 

    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Resultados Analisis'
    },
    subtitle: {
        text: 'Dominios Analizados:'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        min: 0, max: 100,
        title: {
            text: 'Porcentaje de cumplimiento'
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> del Total<br/>'
    },

    series: [{
        name: 'Dominios',
        colorByPoint: true,
        data: [
           <?php foreach($dominios as $dom){ 
       /*PUNTAJE PREGUNTAS OMITIDAS */        
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom->ANA_ID, $dom->DOM_ID);
        if(isset($puntajeOmitido)){                    
        $puntajeMaximo=$dom->DOM_PONDERACION-$puntajeOmitido;
        }else{
        $puntajeMaximo=$dom->DOM_PONDERACION;}
               ?>
       /*!- PUNTAJE PREGUNTAS OMITIDAS */  
                        {
                                                
            name: '<?php echo $dom->DOM_NOMBRE?>',
            y: <?php echo (($dom->PUNTAJEXDOMINIO)*100)/$puntajeMaximo ?>,
            drilldown: '<?php echo $dom->DOM_NOMBRE?>'
        },
        <?php }?>

    ]
    }],

});
});



</script>
</div>

<br>
<br>
<div class="contenedor-mostrar-empresa">


        <div class="col-md-8 col-sm-8">
            <a href="<?php echo base_url('Resultados/seleccionar_dominio')."/".$idana ?>" class="btn btn-info col-md-12 col-sm-12">VER RESPUESTAS CONTESTADAS POR DOMINIO</a>
        </div>
     <div class="col-md-2 col-sm-2">
<a id="back" class="btn btn-success col-md-12 col-sm-12">VOLVER</a>
     </div>
    <div class="col-md-2 col-sm-2">
<a href="<?=base_url()?>Usuarios/menu_diagnostico" class="btn btn-danger col-md-12 col-sm-12">INICIO</a>
    </div>
</div>
<br>
<br>



<script>

$(document).ready(function() {

$('#back').click(function(){
		parent.history.back();
		return false;
	});
});

</script>


