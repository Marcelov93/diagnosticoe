<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">



<style>.contenedor-mostrar-empresa h2 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
.contenedor-mostrar-empresa h3 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }

</style>
<?php
//PUNTAJES PRIMER DIAGNOSTICO
$i=0;
$suma=0;
foreach ($dominios1 as $dom){
    
         /*PUNTAJE PREGUNTAS OMITIDAS--------------- */        
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom->ANA_ID, $dom->DOM_ID);
        if(isset($puntajeOmitido)){                    
        $puntajeMaximo=$dom->DOM_PONDERACION-$puntajeOmitido;
        }else{
        $puntajeMaximo=$dom->DOM_PONDERACION;}
               
       /*!- PUNTAJE PREGUNTAS OMITIDAS------------ */
        
        
    $puntajes1[$i]=round(((($dom->PUNTAJEXDOMINIO)*100)/$puntajeMaximo),2);
    $nombre1[$i]=$dom->DOM_NOMBRE;

    $puntaje=$dom->PUNTAJEXDOMINIO;

    $ponderacion=$dom->PONDERACION/100;

    $suma=$suma+($puntaje*$ponderacion*100)/$puntajeMaximo;

    $i++;
}

//PUNTAJES SEGUNDO DIAGNOSTICO
$j=0;
$suma2=0;
foreach ($dominios2 as $dom1){
     
    
       /*PUNTAJE PREGUNTAS OMITIDAS--------------- */        
        $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom1->ANA_ID, $dom1->DOM_ID);
        if(isset($puntajeOmitido)){                    
        $puntajeMaximo=$dom1->DOM_PONDERACION-$puntajeOmitido;
        }else{
        $puntajeMaximo=$dom1->DOM_PONDERACION;}
               
       /*!- PUNTAJE PREGUNTAS OMITIDAS------------ */
        
    $puntajes2[$j]=round(((($dom1->PUNTAJEXDOMINIO)*100)/$puntajeMaximo),2);
    $nombre2[$j]=$dom1->DOM_NOMBRE;

    $puntaje2=$dom1->PUNTAJEXDOMINIO;

    $ponderacion2=$dom1->PONDERACION/100;

    $suma2=$suma2+($puntaje2*$ponderacion2*100)/$puntajeMaximo;
    
    $j++;
}
?>





<div class="contenedor">
    <br>
    <br>
    <br>
    <h1>Medición del Impacto de la Gestión</h1>
    <br>
    <br>

</div>



<div class="contenedor-mostrar-empresa">


    <div class="col-md-3">
        <h2>Detalle Por Criterios</h2>
        <br>
        <br>
    <?php foreach ($dominios1 as $dom){?>


        <a href="<?=base_url()?>Resultados/comparacion_criterios_DOM<?=$dom->DOM_ID.'/'.$idana1.'/'.$idana2?>" ><li  class="list-group-item"><strong><?=$dom->DOM_NOMBRE?></strong> </li></a>
    <br>



        <?php  } ?>

</div>
    
    <div class="col-md-2"></div>
    <div class="col-md-7">
        <h2>Totales Por Area</h2>
        <br>

        
<table id="datatable" class="table table-striped ">
    <thead>
        <tr >
            
            <th><h4 style="color:white">Dominio</h4></th>
            <th><h4 style="color:white">Evaluación Inicial</h4></th>
            <th><h4 style="color:white">Evaluación Posterior</h4></th>
            <th><h4 style="color:white">Impacto Porcentual</h4></th>
            
        </tr>
    </thead>
    <tbody>
        


    <?php for($i=0;$i<count($dominios1);$i++){
        echo "<tr >";
         echo  "<th>$nombre1[$i]</th>";

                $score=$puntajes1[$i];

        if($score>67){
            echo '<td class=""><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span> '.$score.'% </td>';
        }else if($score>33 &&$score<=67){
            echo '<td class=""><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span> '.$score.' %</td>';
        }else if($score>=0 &&$score<=33){
            echo '<td class=""><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span> '.$score.' %</td>';
        }

            $score2=$puntajes2[$i];

        if($score2>67){
            echo '<td class=""><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span> '.$score2.' %</td>';
        }else if($score2>33 &&$score2<=67){
            echo '<td class=""><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span> '.$score2.' %</td>';
        }else if($score2>=0 &&$score2<=33){
            echo '<td class=""><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span> '.$score2.' %</td>';
        }

               
               $diferencia= $puntajes2[$i]-$puntajes1[$i];
               $diferencia = round($diferencia,2);
               if($diferencia>0){
               echo '<td><span class="badge badge-success"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></span> '.$diferencia.' %</td>';
               }
               if($diferencia==0){
               echo '<td><span class="badge badge-info"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span> '.$diferencia.' %</td>';
               }
               if($diferencia<0){
               echo '<td><span class="badge badge-danger"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></span> '.$diferencia.' %</td>';
               }
    
           echo "</tr>";
   }

        ?>
        
     
     

        
</table>

            <h4 style=" color:white; text-align:center" class="col-md-12">Evaluación</h4>
        <br>
        <br>
        <ul class="list-group">
            <li class="list-group-item list-group-item-danger col-md-4"><span class="badge badge-danger"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></span>De 0 a 33 el dominio se encuentra desarrollado a nivel básico</li>
            <li class="list-group-item list-group-item-warning col-md-4"><span class="badge badge-warning"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span>De 34 a 67 el dominio se encuentra desarrollado a nivel intermedio</li>
            <li class="list-group-item list-group-item-success col-md-4"><span class="badge badge-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>De 68 a 100 el dominio se encuentra desarrollado a nivel avanzado</li>

            <br>
            <br>
        </ul>
        <br>
        <br>
            <h4 style="color:white; text-align:center" class="col-md-12">Impacto</h4>
            <br>
            <br>
        <ul class="list-group">

            <li class="list-group-item list-group-item-danger col-md-4"><span class="badge badge-danger"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></span>Impacto en la gestión: empeoró los resultados</li>
            <li class="list-group-item list-group-item-info col-md-4"><span class="badge badge-info"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></span>No hubo impacto, los resultados se mantuvieron</li>
            <li class="list-group-item list-group-item-success col-md-4"><span class="badge badge-success"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></span>Impacto en la gestión: mejoró los resultados</li>

        </ul>
 
        <br>

    </div>
    </div>
    
<br>
<br>
<div class="contenedor-mostrar-empresa">

    
    <div class="col-md-10"></div>
    <a id="back" class="btn btn-success col-md-2">VOLVER</a>

</div>


<script>
$(document).ready(function(){
	$('#back').click(function(){
		parent.history.back();
		return false;
	});
        

        
});    
</script>
    



    <br>
<br>
