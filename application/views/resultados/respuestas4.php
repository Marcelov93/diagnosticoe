<style>

    @import url(http://fonts.googleapis.com/css?family=Open+Sans);

    html { width: 100%; height:100%; overflow:auto; }

    body {
        width: 100%;
        height:100%;
        font-family: 'Open Sans', sans-serif;
        background-image: url("<?=base_url()?>assets/css/wood_1.png");
        /*        background-image: url("padded.png");*/
        /*        background-image: url("stardust.png");
                background-image: url("tweed.png");*/

    }


    .contenedor h2 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
    .contenedor h1 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }


    .contenedor-mostrar-empresa{
        width:70%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }

    input:focus { box-shadow: inset 0 -5px 45px rgba(100,100,100,0.4), 0 1px 1px rgba(255,255,255,0.2); }

</style>

<div class="contenedor">

    <br>
    <br>
    <h2>Modelo de negocios</h2>
    <br>
    <br>

    <div class="contenedor-mostrar-empresa">

        <?php foreach ($lista as $valor){?>

            <?php foreach ($respuestas as $value){
                $i=1;
                if($value->CRIT_NUMERO==$valor->CRIT_NUMERO) {?>
                    <div class="list-group col-md-12">
                        <a id="resp"  class="list-group-item active">
                            <p class="list-group-item-text">  <?php echo $value->CRIT_NUMERO;  ?>.- <?php echo $value->CRIT_PREGUNTA;  ?>  </p>

                        </a>
                    </div>



                    <!-- respuestas -->
                    <?php foreach ($respuestas2 as $resp){
                        ?>
                        <?php if($value->CRIT_NUMERO==$resp->CRIT_NUMERO){
                            if($i==1){$titulo='NO DESARROLLADO';}else{
                                if($i ==2){$titulo='ESCASAMENTE DESARROLLADO ';} else {
                                    if($i==3){$titulo='PARCIALMENTE DESARROLLADO';}else{
                                        if($i==4){$titulo='AMPLIAMENTE DESARROLLADO';}
                                    }
                                }
                            }
                            ?>
                            <?php if($value->CRIT_NUMERO == 62){ ?>
                                <div  class="list-group col-md-3" >
                                    <?php if($value->ID_RESPUESTA==$resp->ID_RESPUESTA){ ?>
                                        <a class="list-group-item active">
                                            <p class="list-group-item-text">
                                                <?php echo $resp->RESP_DESCRIPCION; ?>
                                            </p>
                                        </a>
                                    <?php }
                                    else{?>
                                        <a class="list-group-item">
                                            <p class="list-group-item-text">
                                                <?php echo $resp->RESP_DESCRIPCION; ?>
                                            </p>
                                        </a>
                                    <?php } ?>
                                </div>
                            <?php }else{ ?>
                                <div  class="list-group col-md-3" >
                                    <?php if($value->ID_RESPUESTA==$resp->ID_RESPUESTA){ ?>
                                        <a class="list-group-item active">
                                            <h4 class="list-group-item-heading"><?=$titulo?></h4>
                                            <br>
                                            <p class="list-group-item-text">
                                                <?php echo $resp->RESP_DESCRIPCION; ?>
                                            </p>
                                        </a>
                                    <?php }
                                    else{?>
                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading"><?=$titulo?></h4>
                                            <br>
                                            <p class="list-group-item-text">
                                                <?php echo $resp->RESP_DESCRIPCION; ?>
                                            </p>
                                        </a>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php $i=$i+1;} ?>
                    <?php }  ?>
                <?php } ?>
            <?php }  ?>
        <?php } ?>


        <div class="col-md-10"></div>
        <a id="back" class="btn btn-success col-md-2">VOLVER</a>

    </div>

    <!-- Main content -->


    <br>
    <br>
    <!-- /.content -->

</div><!-- /.content-wrapper -->
<script>
    $(document).ready(function(){
        $('#back').click(function(){
            parent.history.back();
            return false;
        });
    });
</script>