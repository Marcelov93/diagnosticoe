           
	<div class="container">
			<div class="banner-info">
				<section class="slider">
					<div class="flexslider">
						<ul class="slides">
							<li>
								<div class="banner-info1">
									<h1>Many desktop publishing packages and Web page</h1>
									<p>Publishing packages and Web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many</p>
									
								</div>
							<li>
								<div class="banner-info1">
									<h1>Many desktop publishing packages and Web page</h1>
									<p>Publishing packages and Web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many</p>
									
								</div>
							</li>
							<li>
								<div class="banner-info1">
									<h1>Many desktop publishing packages and Web page</h1>
									<p>Publishing packages and Web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many</p>
									
								</div>
							</li>
						</ul>
					</div>
				</section>
			
							<!-- FlexSlider -->
									  <script defer="" ></script>
									  <script type="text/javascript">
										$(function(){
										 
										});
										$(window).load(function(){
										  $('.flexslider').flexslider({
											animation: "slide",
											start: function(slider){
											  $('body').removeClass('loading');
											}
										  });
										});
									  </script>
								<!-- FlexSlider -->
			</div>
		</div>


		</div>
	<!-- //Header -->
		<!-- //Banner -->
		<!-- agile -->
	<div class="agile">
		     <div class="container">
			 	<h3>About Us</h3>
				 <div class="about-top">
				       <div class="col-md-5 ab-text wow fadeInLeft animated" data-wow-delay=".5s">
							<img src="<?=base_url()?>assets/images/10.jpg" alt="Corporate image" class="img-responsive">
					   </div>
					   <div class="col-md-7 info wow fadeInRight animated" data-wow-delay=".5s">
					<div class="accordion">
						<div class="accordion-section">
							<h5><a class="accordion-section-title" href="#accordion-1">
								 vision Lorem Ipsum
							<i class="glyphicon glyphicon-chevron-down"></i>
							</a></h5>
							<div id="accordion-1" class="accordion-section-content" style="display: none;">								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
						</div>
					
						<div class="accordion-section">
							<h5><a class="accordion-section-title" href="#accordion-2">
								Consectetur adipiscing
							<i class="glyphicon glyphicon-chevron-down"></i>
							</a></h5>
							<div id="accordion-2" class="accordion-section-content" style="display: none;">							
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
						</div>
					
						<div class="accordion-section">
							<h5><a class="accordion-section-title" href="#accordion-3">
								Dolor sit amet
							<i class="glyphicon glyphicon-chevron-down"> </i> 
							</a></h5>
							<div id="accordion-3" class="accordion-section-content" style="display: none;">								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
						</div>	
						<div class="accordion-section">
							<h5><a class="accordion-section-title" href="#accordion-4">
								 Sed do eiusmod
							<i class="glyphicon glyphicon-chevron-down"> </i> 
							</a></h5>
							<div id="accordion-4" class="accordion-section-content" style="display: none;">								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
						</div>	
						<div class="accordion-section">
							<h5><a class="accordion-section-title" href="#accordion-5">
								 Incididunt ut labore
							<i class="glyphicon glyphicon-chevron-down"> </i> 
							</a></h5>
							<div id="accordion-5" class="accordion-section-content" style="display: none;">								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
						</div>
						<div class="accordion-section">
							<h5><a class="accordion-section-title" href="#accordion-6">
								 Adipiscing elit
							<i class="glyphicon glyphicon-chevron-down"> </i> 
							</a></h5>
							<div id="accordion-6" class="accordion-section-content" style="display: none;">								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
							</div>
						</div>							
					</div>	 
					<script>
							jQuery(document).ready(function() {
								function close_accordion_section() {
									jQuery('.accordion .accordion-section-title').removeClass('active');
									jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
								}

								jQuery('.accordion-section-title').click(function(e) {
									// Grab current anchor value
									var currentAttrValue = jQuery(this).attr('href');

									if(jQuery(e.target).is('.active')) {
										close_accordion_section();
									}else {
										close_accordion_section();

										// Add active class to section title
										jQuery(this).addClass('active');
										// Open up the hidden content panel
										jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
									}

									e.preventDefault();
								});
							});
				</script>

					   </div>
					     <div class="clearfix"></div>
					</div>
									
				 </div>
			 </div>
	<!-- //agile -->
		<!-- w3l -->
	<div class="w3l">
	<div class="container">		
		<div class="row">
			<div class="col-sm-12 text-center w3ls">
				<h3>Curabitur pretium </h3>
				<p class="p1 animated wow fadeInUp animated animated" data-wow-duration="1200ms" data-wow-delay="500ms">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.Simply dummy text of the printing and the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				
			</div>
			<div class="clearfix"></div>
		</div>
	</div>	
</div>
<!-- //w3l -->
<!-- agileits -->
	<div class="agileits">
		<div class="container">
			<h3>Projects</h3>
				<div class="col-md-4 agileits-left wow fadeInLeft animated" data-wow-delay=".5s">
					<div class="history-grid-image">
						<img src="<?=base_url()?>assets/images/6.jpg" class="img-responsive zoom-img" alt="">
					</div>
					<h4>Sit voluptatem</h4>
					<p>Perspiciatis unde omnis iste natus error sit voluptatem consectetur adipiscing elit, sed do eiusmod tempor </p>
				</div>
				<div class="col-md-4 agileits-left animated wow fadeInUp animated animated" data-wow-duration="1200ms" data-wow-delay="500ms">
					<div class="history-grid-image">
						<img src="<?=base_url()?>assets/images/7.jpg" class="img-responsive zoom-img" alt="">
					</div>
					<h4>Adipiscing elit</h4>
					<p>Perspiciatis unde omnis iste natus error sit voluptatem consectetur adipiscing elit, sed do eiusmod tempor </p>
				</div>
				<div class="col-md-4 agileits-left wow fadeInRight animated" data-wow-delay=".5s">
					<div class="history-grid-image">
						<img src="<?=base_url()?>assets/images/2.jpg" class="img-responsive zoom-img" alt="">
					</div>
					<h4>Eiusmod tempor</h4>
					<p>Perspiciatis unde omnis iste natus error sit voluptatem consectetur adipiscing elit, sed do eiusmod tempor </p>
				</div>
					<div class="clearfix"></div>
			</div>
		</div>
<!-- /agileits -->
<!-- wthree -->
<div class="wthree">
		<div class="container">
			<h3>Our Services</h3>
			<div class="services-grids">
				<div class="col-md-6 services-grid-left wow fadeInLeft animated" data-wow-delay=".5s">
					<div class="col-xs-4 services-grid-left1">
						<i class="hovicon effect-2 sub-a"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span></i>
					</div>
					<div class="col-xs-8 services-grid-left2">
						<h4>Responsive</h4>
						<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil 
							molestiae consequatur.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 services-grid-right wow fadeInRight animated" data-wow-delay=".5s">
					<div class="col-xs-4 services-grid-left1">
						<i class="hovicon effect-2 sub-a"><span class="glyphicon glyphicon-cog pp" aria-hidden="true"></span></i>
					</div>
					<div class="col-xs-8 services-grid-left2">
						<h4>Web Development</h4>
						<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil 
							molestiae consequatur.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="services-grids">
				<div class="col-md-6 services-grid-left wow fadeInLeft animated" data-wow-delay=".5s">
					<div class="col-xs-4 services-grid-left1">
						<i class="hovicon effect-2 sub-a"><span class="glyphicon glyphicon-signal" aria-hidden="true"></span></i>
					</div>
					<div class="col-xs-8 services-grid-left2">
						<h4>Web Marketing</h4>
						<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil 
							molestiae consequatur.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 services-grid-right wow fadeInRight animated" data-wow-delay=".5s">
					<div class="col-xs-4 services-grid-left1">
						<i class="hovicon effect-2 sub-a"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></i>
					</div>
					<div class="col-xs-8 services-grid-left2">
						<h4>Web Security </h4>
						<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil 
							molestiae consequatur.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //wthree -->
	<!-- Footer -->
	<div class="footer">
		<div class="container">

			<div class="footer-info">
				<div class="col-md-3 col-sm-3 footer-info-grid newsletter wow fadeInLeft animated" data-wow-delay=".5s">
					<h4>NEWSLETTER</h4>
					<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>

					<form action="#" method="post" class="newsletter">
						<input class="email" type="email" placeholder="Your email...">
						<input type="submit" class="submit" value="">
					</form>
				</div>
				<div class="col-md-3 col-sm-3 footer-info-grid address animated wow fadeInUp animated animated" data-wow-duration="1200ms" data-wow-delay="500ms">
					<h4>ADDRESS</h4>
					<address>
						<ul>
							<li>Quis autem vel</li>
							<li>Sydney, Australia</li>
							<li>Telephone : +2 (354) 456-78950</li>
							<li>Email : <a class="mail" href="mailto:mail@example.com">info@example.com</a></li>
						</ul>
					</address>
				</div>
				<div class="col-md-3 col-sm-3 footer-info-grid links animated wow fadeInUp animated animated" data-wow-duration="1200ms" data-wow-delay="500ms">
					<h4>Flickr Posts</h4>
					<ul>
						<li><img src="<?=base_url()?>assets/images/1.jpg" class="img-responsive" alt=" "></li>
						<li><img src="<?=base_url()?>assets/images/2.jpg" class="img-responsive" alt=" "></li>
						<li><img src="<?=base_url()?>assets/images/3.jpg" class="img-responsive" alt=" "></li>
						<li><img src="<?=base_url()?>assets/images/4.jpg" class="img-responsive" alt=" "></li>
						<li><img src="<?=base_url()?>assets/images/5.jpg" class="img-responsive" alt=" "></li>
						<li><img src="<?=base_url()?>assets/images/6.jpg" class="img-responsive" alt=" "></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 footer-info-grid social wow fadeInRight animated" data-wow-delay=".5s">
					<h4>Follow Us</h4>
					<ul>
						<li><a href="#"><span class="fa"> </span></a></li>
						<li><a href="#"><span class="tw"> </span></a></li>
						<li><a href="#"><span class="g"> </span></a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="copyright animated wow fadeInUp animated animated" data-wow-duration="1200ms" data-wow-delay="500ms">
				<p>&copy; 2016 Arbitrary . All Rights Reserved | Design by <a href="http://w3layouts.com/"> W3layouts </a></p>
			</div>

		</div>
	</div>
	<!-- //Footer -->
	</body>
