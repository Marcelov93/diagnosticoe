
<link rel="stylesheet"  href="<?=base_url()?>assets/css/formularios.css" type="text/css" media="all" />

<style>
.contenedor-perfil{
    width:40%;
    max-width:1000px;
    margin:auto;
    overflow:hidden;

}
textarea {
    resize: none;
}
</style>
<div class="contenedor noselect">
    <?php
    if($this->session->flashdata('Exito'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos han sido guardados correctamente!</strong></p>
        </div>
        <br>
        <?php
    }
    ?>
    
    <br>
    <br>

<div class="col-md-9"></div>
    <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda <i class="glyphicon glyphicon-question-sign"></i></button>
    <h1>FACTORES PEST</h1>
    
    <br>
    <br>
    <br>
</div>
<div class="contenedor-perfil noselect">



    <a><li  class="list-group-item politico"><i class="glyphicon glyphicon-chevron-right"></i><strong> FACTORES POLÍTICOS</strong></li></a> <br>
    <a><li  class="list-group-item economico"><i class="glyphicon glyphicon-chevron-right"></i><strong> FACTORES ECONÓMICOS</strong></li></a> <br>
    <a><li  class="list-group-item scultural"><i class="glyphicon glyphicon-chevron-right"></i><strong> FACTORES SOCIO CULTURALES</strong></li></a> <br>
    <a><li  class="list-group-item tecnologico"><i class="glyphicon glyphicon-chevron-right"></i><strong> FACTORES TECNOLÓGICOS</strong></li></a> <br>

<div class="col-md-10"></div>
            <a id="back" href="<?=base_url()?>Estrategia/menu" class="btn btn-success col-md-2">VOLVER</a>
<!--modal prueba politico --> 
<div id="politico" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">FACTORES PEST </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_politico" action="<?=base_url()?>Estrategia/insertar_pest" method="post" class="formulario">
            <p><strong>Ingrese los Factores Políticos</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="12" name="politico" id="politico" required ></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
                     
          <form id="editar_politico" action="<?=base_url()?>Estrategia/editar_pest" method="post" class="formulario">
            <p><strong>Edite los Factores Políticos</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="12" name="politico" id="politico" required><?=$politico->ANP_DESCRIPCION?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
         <div id="ver_politico" class="formulario">
            <p><strong>Factores Políticos</strong></p>
            <br>
            <br>
                <h4><?=nl2br($politico->ANP_DESCRIPCION)?></h4>
            
            <br>
            <div class="modal-footer">
                    <button id="editarPolitico" type="button" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </div>
      </div>
 
    
    </div>

  </div>
</div>     
<!-- fin modal prueba politico -->     
  
<!--modal --> 
<div id="economico" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">FACTORES PEST </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_economico" action="<?=base_url()?>Estrategia/insertar_pest" method="post" class="formulario">
            <p><strong>Ingrese los Factores Económicos</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="12" name="economico" id="economico" required ></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
                     
          <form id="editar_economico" action="<?=base_url()?>Estrategia/editar_pest" method="post" class="formulario">
            <p><strong>Edite los Factores Económicos</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="12" name="economico" id="economico" required ><?=$economico->ANP_DESCRIPCION?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
         <div id="ver_economico" class="formulario">
            <p><strong>Factores Económicos</strong></p>
            <br>
            <br>
                <h4><?=nl2br($economico->ANP_DESCRIPCION)?></h4>
            
            <br>
            <div class="modal-footer">
                    <button id="editarEconomico" type="button" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </div>
      </div>
 
    
    </div>

  </div>
</div>     
<!-- fin modal -->     
<!--modal --> 
<div id="scultural" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">FACTORES PEST </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_scultural" action="<?=base_url()?>Estrategia/insertar_pest" method="post" class="formulario">
            <p><strong>Ingrese los Factores Socio-Culturales</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="12" name="scultural" id="scultural" required ></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
                     
          <form id="editar_scultural" action="<?=base_url()?>Estrategia/editar_pest" method="post" class="formulario">
            <p><strong>Edite los Factores Socio-Culturales</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="12" name="scultural" id="scultural" required><?=$scultural->ANP_DESCRIPCION?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
         <div id="ver_scultural" class="formulario">
            <p><strong>Factores Socio-Culturales</strong></p>
            <br>
            <br>
                <h4><?=nl2br($scultural->ANP_DESCRIPCION)?></h4>
            
            <br>
            <div class="modal-footer">
                    <button id="editarScultural" type="button" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </div>
      </div>
 
    
    </div>

  </div>
</div>    
<!-- fin modal -->     
<!--modal --> 
<div id="tecnologico" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">FACTORES PEST </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_tecnologico" action="<?=base_url()?>Estrategia/insertar_pest" method="post" class="formulario">
            <p><strong>Ingrese los Factores Tecnológicos</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="12" name="tecnologico" id="tecnologico"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
                     
          <form id="editar_tecnologico" action="<?=base_url()?>Estrategia/editar_pest" method="post" class="formulario">
            <p><strong>Edite los Factores Tecnológicos</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="12" name="tecnologico" id="tecnologico" required><?=$tecnologico->ANP_DESCRIPCION?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
         <div id="ver_tecnologico" class="formulario">
            <p><strong>Factores Tecnológicos</strong></p>
            <br>
            <br>
                <h4><?=nl2br($tecnologico->ANP_DESCRIPCION)?></h4>
            
            <br>
            <div class="modal-footer">
                    <button id="editarTecnologico" type="button" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </div>
      </div>
 
    
    </div>

  </div>
</div>  
<!-- fin modal -->
    <div id="ayuda" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Definiciones</h4>
                </div>
                <div class="modal-body">
                    El análisis PEST identifica los factores del entorno general que van a afectar a la empresa.

                    <br>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong> Factor Político:</strong><br>  Factores relacionados con la regulación legislativa de un gobierno.
                        Ejemplo: Legislación antimonopólico, Leyes de protección del medio ambiente y a la salud, Políticas impositivas,
                        Regulación del comercio exterior,Regulación sobre el empleo, Promoción de la actividad empresarial, Estabilidad gubernamental. </p>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Factor Económico:</strong><br> Factores de índole económica que afectan al mercado en su conjunto (a unos sectores más que a otros).
                        De entre ellos, podemos mencionar: ciclo económico, Evolución del PNB, Tipos de interés, Oferta monetaria,
                        Evolución de los precios, Tasa de desempleo, Ingreso disponible, Disponibilidad y distribución de los recursos,
                        Nivel de desarrollo. </p>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Factor Socio-Cultural:</strong><br> Configuración de los integrantes del mercado y su influencia en el entorno.
                        Véase variables como la evolución demográfica, Distribución de la renta, Movilidad social,
                        Cambios en el estilo de vida, Actitud consumista, Nivel educativo, Patrones culturales y la Religión. </p>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Factor Tecnológico:</strong><br>  Estado de desarrollo tecnológico y sus aportes en la actividad empresarial.
                        Depende de su estado la cifra en gasto público en investigación, Preocupación gubernamental y de industria por la tecnología, Grado de obsolescencia,
                        Madurez de las tecnologías convencionales, Desarrollo de nuevos productos, Velocidad de transmisión de la tecnología. </p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
                </div>
            </div>

        </div>
    </div>
    
    
</div>


<script>
$(document).ready (function(){
$(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});

$('#insertar_politico').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
$('#insertar_economico').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
$('#insertar_scultural').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
$('#insertar_tecnologico').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});


if(<?=$existe_politico?>==1){
        $('#insertar_politico').hide();
        $('#editar_politico').hide();
    }else{
        $('#ver_politico').hide();
         $("#editar_politico").hide();
    }

$("#editarPolitico").click(function() {
     $("#editar_politico").show(500);
 $("#ver_politico").hide();

});
if(<?=$existe_economico?>==1){
        $('#insertar_economico').hide();
        $('#editar_economico').hide();
    }else{
        $('#ver_economico').hide();
         $("#editar_economico").hide();
    }

$("#editarEconomico").click(function() {
     $("#editar_economico").show(500);
 $("#ver_economico").hide();

});
if(<?=$existe_scultural?>==1){
        $('#insertar_scultural').hide();
        $('#editar_scultural').hide();
    }else{
        $('#ver_scultural').hide();
         $("#editar_scultural").hide();
    }

$("#editarScultural").click(function() {
     $("#editar_scultural").show(500);
 $("#ver_scultural").hide();

});
if(<?=$existe_tecnologico?>==1){
        $('#insertar_tecnologico').hide();
        $('#editar_tecnologico').hide();
    }else{
        $('#ver_tecnologico').hide();
         $("#editar_tecnologico").hide();
    }

$("#editarTecnologico").click(function() {
     $("#editar_tecnologico").show(500);
 $("#ver_tecnologico").hide();

});



 $( ".politico" ).click(function(){

            $('#politico').modal('show');
        });  

 $( ".economico" ).click(function(){

            $('#economico').modal('show');
        });  

 $( ".scultural" ).click(function(){

            $('#scultural').modal('show');
        });  

 $( ".tecnologico" ).click(function(){

            $('#tecnologico').modal('show');
        });  
        

 });
 </script>