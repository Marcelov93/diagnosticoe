
<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>

    .formulario {

        width:60%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }

    p{
        color: black;
    }
    
    textarea {
    resize: none;
}
body.modal-open {
    overflow: visible;
}
</style>

<script>
    var base_url = "<?php Print(base_url()); ?>";


</script>



<div class="contenedor">
   <?php
    if($this->session->flashdata('registrostatus'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos se han eliminado correctamente!</strong></p>
        </div>
        <br>
        <?php
    }else if($this->session->flashdata('Exito'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos han sido guardados correctamente!</strong></p>
        </div>
        <br>
        <?php
    }
    ?>
   
    <br>
    <br>
 <div class="col-md-10"></div>
    <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda <i class="glyphicon glyphicon-question-sign"></i></button>
    <h2>ANÁLISIS FODA</h2>
       

    <br>
    <br>


    <div class="formulario">

<div class="row">
<div class="col-md-12">
    <h3>Fortalezas:</h3>
    <hr>
        <table id="tabla_fortaleza" class="table">
            <thead  >
            <tr class="active">
                   <th width="90%"><p>Descripción</p></th>
                    <th width="5%"><p>Editar</p></th>
                    <th width="5%"><p>Eliminar</p></th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($foda as $value) {
            if($value->AFO_ID=='FORTALEZA'){?>

            <tr class="active">
                <td><p><?php echo $value->FO_DESCRIPCION?></p></td>
                <td><a value="<?=$value->FO_ID?>" class="edit_fortaleza" data-toggle="modal" data-target="#<?=$value->FO_ID?>"><span  class="glyphicon glyphicon-pencil" </span></a></td>
            
               <td><a data-toggle="modal" data-target="#modal<?=$value->FO_ID?>"><span class="glyphicon glyphicon-trash" </span></a></td>
            
            
               <!--modal eliminar-->
                     <div id="modal<?=$value->FO_ID?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        Modal content
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Atención !</h4>
                            </div>
                            <div class="modal-body">

                                
                                <p><strong>Está seguro que desea eliminar esta fortaleza?</strong></p><br>
                                <p><?=nl2br( $value->FO_DESCRIPCION)?></p>

                            </div>
                            <div class="modal-footer">
                                <button onclick="javascript:window.location='<?php echo base_url('Estrategia/eliminar_foda')."/".$value->FO_ID."/".$value->AFO_ID ?>'" type="button" class="btn btn-danger" data-dismiss="modal">ELIMINAR</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>

                    </div>
                </div>
               <!--modal-->
         
            </tr>

            <?php }?>
            </tbody>
            <?php }?>
            <tr>
                <td><button id="add_fortaleza" type="button" class="btn btn-success" >
                        <span class="glyphicon glyphicon-plus" ></span> Agregar</button></td>
            </tr>
        </table>
    <button id="add_fortaleza0" type="button" class="btn btn-success" >
                        <span class="glyphicon glyphicon-plus" ></span> Agregar</button>
</div>

        <div class="col-md-12">
             <h3>Debilidades:</h3>
    <hr>
            <table id="tabla_debilidad" class="table">
                <thead >
                <tr  class="active">
                    <th width="90%"><p>Descripción</p></th>
                    <th width="5%"><p>Editar</p></th>
                    <th width="5%"><p>Eliminar</p></th>

                </tr>
                </thead>
                <tbody>

                <?php foreach ($foda as $value) {
                if($value->AFO_ID=='DEBILIDAD'){?>

                    <tr class="active">
                        <td><p><?php echo $value->FO_DESCRIPCION?></p></td>
                       <td><a value="<?=$value->FO_ID?>"  data-toggle="modal" data-target="#<?=$value->FO_ID?>"><span  class="glyphicon glyphicon-pencil" </span></a></td>
                         <td><a data-toggle="modal" data-target="#modal<?=$value->FO_ID?>"><span class="glyphicon glyphicon-trash" </span></a></td>
            
            
               <!--modal eliminar-->
                     <div id="modal<?=$value->FO_ID?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        Modal content
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Atención !</h4>
                            </div>
                            <div class="modal-body">

                                
                                <p><strong>Está seguro que desea eliminar esta debilidad?</strong></p><br>
                                <p><?=nl2br( $value->FO_DESCRIPCION)?></p>

                            </div>
                            <div class="modal-footer">
                                <button onclick="javascript:window.location='<?php echo base_url('Estrategia/eliminar_foda')."/".$value->FO_ID."/".$value->AFO_ID ?>'" type="button" class="btn btn-danger" data-dismiss="modal">ELIMINAR</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>

                    </div>
                </div>
               <!--modal-->

                    </tr>
                <?php }?>
                </tbody>
                <?php }?>
                <tr>
                <td><button id="add_debilidad" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" ></span> Agregar</button></td>
            </tr>
            </table>
    <button id="add_debilidad0" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" ></span> Agregar</button>
        </div>
</div>
<div class="row">

        <div class="col-md-12">
             <h3>Oportunidades:</h3>
    <hr>
            <table id="tabla_oportunidad"  class="table">
                <thead >
                <tr class="active">
                    <th width="90%"><p>Descripción</p></th>
                    <th width="5%"><p>Editar</p></th>
                    <th width="5%"><p>Eliminar</p></th>

                </tr>
                </thead>
                <tbody>

                <?php foreach ($foda as $value) {
                if($value->AFO_ID=='OPORTUNIDAD'){?>

                    <tr class="active">
                        <td><p><?php echo $value->FO_DESCRIPCION?></p></td>
                         <td><a value="<?=$value->FO_ID?>"  data-toggle="modal" data-target="#<?=$value->FO_ID?>"><span  class="glyphicon glyphicon-pencil" </span></a></td>
                         <td><a data-toggle="modal" data-target="#modal<?=$value->FO_ID?>"><span class="glyphicon glyphicon-trash" </span></a></td>
            
            
               <!--modal eliminar-->
                     <div id="modal<?=$value->FO_ID?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        Modal content
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Atención !</h4>
                            </div>
                            <div class="modal-body">

                                
                                <p><strong>Está seguro que desea eliminar esta oportunidad?</strong></p><br>
                                <p><?=nl2br( $value->FO_DESCRIPCION)?></p>

                            </div>
                            <div class="modal-footer">
                                <button onclick="javascript:window.location='<?php echo base_url('Estrategia/eliminar_foda')."/".$value->FO_ID."/".$value->AFO_ID ?>'" type="button" class="btn btn-danger" data-dismiss="modal">ELIMINAR</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>

                    </div>
                </div>
               <!--modal-->
                       
                    </tr>
                <?php }?>
                </tbody>
                <?php }?>
                <tr>
                <td><button id="add_oportunidad" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" ></span> Agregar</button></td>
            </tr>
            </table>
    <button id="add_oportunidad0" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" ></span> Agregar</button>
        </div>

        <div class="col-md-12">
             <h3>Amenazas:</h3>
    <hr>
            <table  id="tabla_amenaza" class="table">
                <thead  >
                <tr class="active">
                    <th width="90%"><p>Descripción</p></th>
                    <th width="5%"><p>Editar</p></th>
                    <th width="5%"><p>Eliminar</p></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($foda as $value) {
                if($value->AFO_ID=='AMENAZA'){?>

                    <tr class="active">
                        <td><p><?php echo $value->FO_DESCRIPCION?></p></td>
                        <td><a value="<?=$value->FO_ID?>" class="edit_fortaleza" data-toggle="modal" data-target="#<?=$value->FO_ID?>"><span  class="glyphicon glyphicon-pencil" </span></a></td>
                         <td><a data-toggle="modal" data-target="#modal<?=$value->FO_ID?>"><span class="glyphicon glyphicon-trash" </span></a></td>
            
            
               <!--modal eliminar-->
                     <div id="modal<?=$value->FO_ID?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        Modal content
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Atención !</h4>
                            </div>
                            <div class="modal-body">

                                
                                <p><strong>Está seguro que desea eliminar esta amenaza?</strong></p><br>
                                <p><?=nl2br( $value->FO_DESCRIPCION)?></p>

                            </div>
                            <div class="modal-footer">
                                <button onclick="javascript:window.location='<?php echo base_url('Estrategia/eliminar_foda')."/".$value->FO_ID."/".$value->AFO_ID ?>'" type="button" class="btn btn-danger" data-dismiss="modal">ELIMINAR</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>

                    </div>
                </div>
               <!--modal-->
                    </tr>
                <?php }?>
                </tbody>
                <?php }?>
                <tr>
                <td><button id="add_amenaza" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" ></span> Agregar</button></td>
            </tr>
            </table>
    <button id="add_amenaza0" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" ></span> Agregar</button>
        </div>

</div>

        <div class="col-md-10"></div>

        <a href="<?php echo base_url('Estrategia/menu') ?>" class="btn btn-success col-md-2">VOLVER</a>





<!--insertar fortaleza-->
<div id="agregar_fortaleza" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Ingresar Fortaleza </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_fortaleza" action="<?=base_url()?>Estrategia/insertar_foda" method="post" >
            <p><strong>Ingrese una Fortaleza</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="4" name="fortaleza" id="fortaleza"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
      
      </div>
 
    
    </div>

  </div>
</div>     
<!-- fin insertar fortaleza -->
<!--editar fortaleza-->
<?php foreach ($foda as $fortaleza){
if($fortaleza->AFO_ID=='FORTALEZA'){
    $base_url= base_url();
echo '<div id="'.$fortaleza->FO_ID.'" class="modal fade" role="dialog">
  <div class="modal-dialog ">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button
       <h4 class="modal-title">EDITAR FORTALEZA </h4>
                </div>
                 <div class="modal-body">                   
          <form id="update_fortaleza" action="'.$base_url.'Estrategia/editar_foda" method="post">
            <p><strong>Edite la fortaleza</strong></p>
            <br>
            <br>
            <input type="hidden" value="'.$fortaleza->FO_ID.'" name="fo_id" id="fo_id">
            <textarea class="form-control" rows="4" name="fortaleza" id="fortaleza" required>'.$fortaleza->FO_DESCRIPCION.'</textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
      </div>
 
    
    </div>

  </div>
</div> ';
}}?>

<!--insertar debilidad-->
<div id="agregar_debilidad" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Ingresar Debilidad </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_debilidad" action="<?=base_url()?>Estrategia/insertar_foda" method="post" >
            <p><strong>Ingrese una Debilidad</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="4" name="debilidad" id="debilidad" required ></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
      
      </div>
 
    
    </div>

  </div>
</div>     
<!-- fin insertar debilidad -->
<!--editar debilidad-->
<?php foreach ($foda as $debilidad){
if($debilidad->AFO_ID=='DEBILIDAD'){
    $base_url= base_url();
echo '<div id="'.$debilidad->FO_ID.'" class="modal fade" role="dialog">
  <div class="modal-dialog ">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button
       <h4 class="modal-title">EDITAR DEBILIDAD </h4>
                </div>
                 <div class="modal-body">                   
          <form id="update_debilidad" action="'.$base_url.'Estrategia/editar_foda" method="post">
            <p><strong>Edite la debilidad</strong></p>
            <br>
            <br>
            <input type="hidden" value="'.$debilidad->FO_ID.'" name="fo_id" id="fo_id">
            <textarea class="form-control" rows="4" name="debilidad" id="debilidad" required>'.$debilidad->FO_DESCRIPCION.'</textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
      </div>
 
    
    </div>

  </div>
</div> ';
}}?>
<!-- fin editar debilidad-->

<!--insertar oportunidad-->
<div id="agregar_oportunidad" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Ingresar Oportunidad </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_oportunidad" action="<?=base_url()?>Estrategia/insertar_foda" method="post" >
            <p><strong>Ingrese una Oportunidad</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="4" name="oportunidad" id="oportunidad"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
      
      </div>
 
    
    </div>

  </div>
</div>     
<!-- fin insertar oportunidad -->
<!--editar oportunidad-->
<?php foreach ($foda as $oportunidad){
if($oportunidad->AFO_ID=='OPORTUNIDAD'){
    $base_url= base_url();
echo '<div id="'.$oportunidad->FO_ID.'" class="modal fade" role="dialog">
  <div class="modal-dialog ">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button
       <h4 class="modal-title">EDITAR DEBILIDAD </h4>
                </div>
                 <div class="modal-body">                   
          <form id="update_oportunidad" action="'.$base_url.'Estrategia/editar_foda" method="post">
            <p><strong>Edite la debilidad</strong></p>
            <br>
            <br>
            <input type="hidden" value="'.$oportunidad->FO_ID.'" name="fo_id" id="fo_id">
            <textarea class="form-control" rows="4" name="oportunidad" id="oportunidad" required>'.$oportunidad->FO_DESCRIPCION.'</textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
      </div>
 
    
    </div>

  </div>
</div> ';
}}?>
<!--insertar amenaza-->
<div id="agregar_amenaza" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Ingresar Amenaza </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_amenaza" action="<?=base_url()?>Estrategia/insertar_foda" method="post" >
            <p><strong>Ingrese una amenaza</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="4" name="amenaza" id="amenaza"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
      
      </div>
 
    
    </div>

  </div>
</div>     
<!-- fin insertar amenaza -->
<!--editar amenaza-->
<?php foreach ($foda as $amenaza){
if($amenaza->AFO_ID=='AMENAZA'){
    $base_url= base_url();
echo '<div id="'.$amenaza->FO_ID.'" class="modal fade" role="dialog">
  <div class="modal-dialog ">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button
       <h4 class="modal-title">EDITAR AMENAZA </h4>
                </div>
                 <div class="modal-body">                   
          <form id="update_amenaza" action="'.$base_url.'Estrategia/editar_foda" method="post">
            <p><strong>Edite la amenaza</strong></p>
            <br>
            <br>
            <input type="hidden" value="'.$amenaza->FO_ID.'" name="fo_id" id="fo_id">
            <textarea class="form-control" rows="4" name="amenaza" id="amenaza" required>'.$amenaza->FO_DESCRIPCION.'</textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
      </div>
 
    
    </div>

  </div>
</div> ';
}}?>

    </div>
    <br>
    <br>
    <div id="ayuda" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Definiciones</h4>
                </div>
                <div class="modal-body">
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Fortalezas:</strong> <br> son las capacidades especiales con que cuenta la empresa,
                        y que le permite tener una posición privilegiada frente a la competencia.
                        Recursos que se controlan, capacidades y habilidades que se poseen, actividades que se desarrollan positivamente, etc.</p>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Oportunidades:</strong><br> son aquellos factores que resultan positivos, favorables, explotables,
                        que se deben descubrir en el entorno en el que actúa la empresa, y que permiten obtener ventajas competitivas. </p>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Debilidades:</strong><br>  son aquellos factores que provocan una posición desfavorable frente a la competencia,
                        recursos de los que se carece, habilidades que no se poseen, actividades que no se desarrollan positivamente, etc.</p>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Amenazas:</strong><br> son aquellas situaciones que provienen del entorno y que pueden llegar a atentar incluso contra la permanencia de la organización. </p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
                </div>
            </div>

        </div>
    </div>
</div>

<script>


  
    
    $(document).ready (function(){

  $('#insertar_fortaleza').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
  $('#insertar_oportunidad').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
  $('#insertar_debilidad').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
  $('#insertar_amenaza').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
          
        $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});
 $( "#add_fortaleza, #add_fortaleza0" ).click(function(){

            $('#agregar_fortaleza').modal('show');
        }); 
 $( "#add_debilidad, #add_debilidad0" ).click(function(){

            $('#agregar_debilidad').modal('show');
        }); 
 $( "#add_oportunidad, #add_oportunidad0" ).click(function(){

            $('#agregar_oportunidad').modal('show');
        }); 
 $( "#add_amenaza, #add_amenaza0" ).click(function(){

            $('#agregar_amenaza').modal('show');
        }); 
        
        if(<?=$existe_fortaleza?> == 0){
        $('#tabla_fortaleza').hide();
    }else{
    $("#add_fortaleza0").hide();
    }
        if(<?=$existe_debilidad?> == 0){
        $('#tabla_debilidad').hide();
    }else{
     $("#add_debilidad0").hide();
    }
        if(<?=$existe_oportunidad?> == 0){
        $('#tabla_oportunidad').hide();
    }else{
     $("#add_oportunidad0").hide();
    }
        
        if(<?=$existe_amenaza?> == 0){
        $('#tabla_amenaza').hide();
    }else{
     $("#add_amenaza0").hide();
    }  

     
        
        
 }); 

</script>

