
<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>

    .formulario {

        width:60%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }

    textarea {
    resize: none;
}
    
</style>

<script>
    var base_url = "<?php Print(base_url()); ?>";


</script>



<div class="contenedor">
 
    <br>
    <br>
    <!--INGRESAR MISIONVISION -->
    <form id="insertar" action="<?php echo base_url();?>Estrategia/insertar_mision_vision" method="post" class="formulario">


        <h3>INGRESAR MISION Y VISION</h3>
        <div class="col-md-10"></div>
        <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda <i class="glyphicon glyphicon-question-sign"></i></button>

        <br>

        <div class="col-sm-12 col-md-12">



            <label for="mision">Misi&oacute;n</label>
            <br>
            <br>
            <textarea class="form-control" rows="6" name="mision" id="mision"  required></textarea>

            <br>

        </div>


        <div class="col-sm-12 col-md-12">

            <label for="vision">Visi&oacute;n</label>
            <br>
            <br>
            <textarea class="form-control" rows="6" name="vision" id="vision"  required></textarea>


            <br>
            <br>
        </div>


        <div class="col-sm-12 col-md-12" >
            <button type="submit" class="btn btn-primary btn-block btn-large">Guardar</button>

            <br>
            <br>
            <div class="col-md-10"></div>
            <a id="back1" class="btn btn-success col-md-2">VOLVER</a>

        </div>


    </form>
    <!--FIN INGRESAR MISIONVISION -->
    
    <!--EDITAR MISIONVISION -->
       <form id="editar" action="<?php echo base_url();?>Estrategia/editar_mision_vision" method="post" class="formulario">


<h3>EDITAR MISION Y VISION</h3><br>
        <div class="col-sm-12 col-md-12">



            <label for="mision">Misi&oacute;n</label>
            <br>
            <br>
            <textarea class="form-control" rows="6" name="mision" id="mision" required><?=$datos->MV_MISION?></textarea>

            <br>

        </div>


        <div class="col-sm-12 col-md-12">

            <label for="vision">Visi&oacute;n</label>
            <br>
            <br>
            <textarea class="form-control" rows="6" name="vision" id="vision" required><?=$datos->MV_VISION?></textarea>


            <br>
            <br>
        </div>


        <div class="col-sm-12 col-md-12" >
            <button type="submit" class="btn btn-primary btn-block btn-large">Actualizar</button>

            <br>
            <br>

        </div>
        <div class="col-md-10"></div>
        <a id="backeditar" class="btn btn-success col-md-2">VOLVER</a>

    </form>
    <!-- FIN EDITAR MISIONVISION -->
    
    <!--VER MISIONVISION -->
    
       <div id="ver" class="formulario">

        <div class="col-sm-12 col-md-12">

            
            <br>
            <br>
            <p><h3>MISIÓN</h3></p><br>
            <p><strong><blockquote  style="color:white"><?=nl2br($datos->MV_MISION)?></blockquote></strong></p>
            <br>
           
              <p><h3>VISIÓN</h3></p><br>
             <p><strong><blockquote  style="color:white"><?=nl2br($datos->MV_VISION)?></blockquote></strong></p>
            <br>
            
            <br>
            <br>


        </div>

               <div class="col-sm-12 col-md-12" >
            <button id="editarB" class="btn btn-primary btn-block btn-large">ACTUALIZAR</button>

          
            <br>
            <div class="col-md-10"></div>
        <a id="back2" class="btn btn-success col-md-2">VOLVER</a>
        <br>
        </div>

    </div>
    <br>
    <!--FIN VER MISIONVISION -->

    <div id="ayuda" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Definiciones</h4>
                </div>
                <div class="modal-body">
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Misión:</strong> <br>Define principalmente cual es la labor o actividad en el mercado,
                    además se puede completar haciendo referencia al público hacia el que va dirigido y con la singularidad,
                    particularidad o factor diferencial, mediante la cual desarrolla su labor o actividad.
                    Para definir la misión de la empresa, ayudará responder algunas de las siguientes preguntas:
                    ¿Qué hacemos?, ¿cuál es nuestro negocio?, ¿a qué nos dedicamos?, ¿cuál es nuestra razón de ser?,
                    ¿quiénes son nuestro público objetivo?, ¿cuál es nuestro ámbito geográfico de acción?,
                    ¿cuál es nuestra ventaja competitiva?, ¿qué nos diferencia de nuestros competidores?</p>
                    <br>

                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Visión:</strong><br> La visión define las metas que pretendemos conseguir en el futuro.
                        Estas metas tienen que ser realistas y alcanzables, puesto que la propuesta de visión tiene un carácter inspirador y motivador.
                        Para la definición de  la visión de nuestra empresa, nos ayudará responder a las siguientes preguntas: ¿Qué quiero lograr?,
                        ¿dónde quiero estar en el futuro?, ¿para quién lo haré?, ¿ampliaré mi zona de actuación?
                    </p>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
                </div>
            </div>

        </div>
    </div>

</div>


<script>



    $(document).ready(function() {

$('#insertar').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});

if(<?=$existe?>==1){
        $('#insertar').hide();
        $('#editar').hide();
    }else{
        $('#ver').hide();
         $("#editar").hide();
    }

$("#editarB").click(function() {
     $("#editar").show(500);
 $("#ver").hide();

});


        $('#back1').click(function(){
            parent.history.back();
            return false;
        });
        $('#back2').click(function(){
           parent.history.back();
            return false;
        });
        $('#backeditar').click(function(){
           $("#editar").hide();
           $('#ver').show(500);
        });
    });

</script>

