
<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>

    .formulario {

        width:60%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }

    p{
        color: black;
    }
        textarea {
    resize: none;
}
body.modal-open {
    overflow: visible;
}


</style>

<script>
    var base_url = "<?php Print(base_url()); ?>";


</script>



<div class="contenedor">
    <?php
    if($this->session->flashdata('registrostatus'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos se han eliminado correctamente!</strong></p>
        </div>
        <br>
        <?php
    }else if($this->session->flashdata('Exito'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos han sido guardados correctamente!</strong></p>
        </div>
        <br>
        <?php
    }
    ?>
    <br>
    <br>
    <h2>OBJETIVOS ESTRATÉGICOS</h2>
    <div class="col-md-10"></div>
    <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda <i class="glyphicon glyphicon-question-sign"></i></button>
    <br>
    <br>
    <br>


    <div class="formulario">
        <h3 id="sin_objetivos" style="color:white; text-align: left">Ingrese un Objetivo<hr></h3>
        <table id="tabla_objetivos" class="table table-responsive">
            <thead>
            <tr class="active">
                <th width="65%"><p>Descripción</p></th>
               
                <th width="5%"><p>Editar</p></th>
                <th width="5%"><p>Eliminar</p></th>
                 <th style="text-align:center" width="25%"><p>Factores críticos de éxito (Ver / Agregar) </p></th>

            </tr>
            </thead>
            <tbody>

            <?php foreach ($objetivos as $value) {?>

            <tr class="active">
                <td><p><?=$value->OE_DESCRIPCION?></p></td>
            
                
                <td align="center"><a data-toggle="modal" data-target="#modal<?=$value->OE_ID?>"><span class="glyphicon glyphicon-pencil"> </span></a></td>
                <td align="center"><a data-toggle="modal" data-target="#<?=$value->OE_ID?>"><span class="glyphicon glyphicon-trash"></span></a></td>
               <td align="center"><a href="<?php echo base_url('Estrategia/ver_factores')."/".$value->OE_ID ?>"><span class="glyphicon glyphicon-new-window" ></span></a></td>
               
                <div id="<?=$value->OE_ID?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                      
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Atención !</h4>
                            </div>
                            <div class="modal-body">

                                <?php
                                $tiene=$this->Estrategia_model->tiene_factores($value->OE_ID);

                                    if($tiene){
                                        echo '<div class="alert alert-danger" role="alert">
                                                <h5>El objetivo tiene asociados factores cr&iacute;ticos de &eacute;xito</h5>
                                            </div>';
                                    }
                                ?>
                                <p>Está seguro que desea eliminar?</p>

                            </div>
                            <div class="modal-footer">
                                <button onclick="javascript:window.location='<?php echo base_url('Estrategia/eliminar_objetivo')."/".$value->OE_ID; ?>'" type="button" class="btn btn-danger" data-dismiss="modal">ELIMINAR</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </tr>
            </tbody>
            <?php }?>
             <tr>
                <td><button id="add_objetivo" type="button" class="btn btn-success" data-toggle="modal" data-target="#agregar_objetivo" >
                        <span class="glyphicon glyphicon-plus" ></span> Agregar Objetivo</button></td>
            </tr>
        </table>

<button id="add_objetivo0" type="button" class="btn btn-success" data-toggle="modal" data-target="#agregar_objetivo" >
                        <span class="glyphicon glyphicon-plus" ></span> Agregar Objetivo</button>
                        
        <div class="col-md-10"></div>

        <a href="<?php echo base_url('Estrategia/menu') ?>" class="btn btn-success col-md-2">VOLVER</a>


        
        
 <!--insertar objetivo-->
<div id="agregar_objetivo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Ingresar Objetivo Estratégico </h4>
                </div>
                 <div class="modal-body">
         <form id="insertar_objetivo" action="<?php echo base_url();?>Estrategia/insertar_obj_est" method="post">
          
            <br>
            <textarea class="form-control" rows="4" name="objetivo" id="objetivo" required ></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
      
      </div>
 
    
    </div>

  </div>
</div>     
<!-- fin insertar objetivo -->
<!--editar objetivo-->
<?php foreach ($objetivos as $obj){

    $base_url= base_url();
echo '<div id="modal'.$obj->OE_ID.'" class="modal fade" role="dialog">
  <div class="modal-dialog ">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button
       <h4 class="modal-title">EDITAR OBJETIVO </h4>
                </div>
                 <div class="modal-body">                   
          <form id="update_objetivo" action="'.$base_url.'Estrategia/editar_objetivo_estrategico" method="post">
            <br>
            <br>
            <input type="hidden" value="'.$obj->OE_ID.'" name="oe_id" id="oe_id">
            <textarea class="form-control" rows="4" name="objetivo" id="objetivo" required>'.$obj->OE_DESCRIPCION.'</textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
      </div>
 
    
    </div>

  </div>
</div> ';
}?>       
        
    </div>
<br>
    <br>

    <div id="ayuda" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Definición</h4>
                </div>
                <div class="modal-body">

                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong> Objetivos Estratégicos:</strong><br> Se denomina objetivos estratégicos a las metas y
                        estrategias planteadas por la organización para lograr determinadas metas
                        y a largo plazo la posición de la organización en un mercado específico, es decir, son los resultados que la
                        empresa espera alcanzar en un tiempo mayor a un año, realizando acciones que le permitan cumplir con su misión,
                        inspirados en la visión.

                    </p>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong> Factor de Éxito:</strong><br>  es algo que debe ocurrir (o debe no ocurrir) para conseguir un objetivo. </p>
                    <br>
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong> Factor Crítico de Éxito:</strong><br>  Un Factor de Éxito se dice que es crítico cuando es necesario su cumplimiento para los objetivos de la organización. </p>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
                </div>
            </div>

        </div>
    </div>
</div>


<script>


    $(document).ready(function() {

$('#agregar_objetivo').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled',true);
});


        $(".alert-dismissible").fadeTo(2500, 500).slideUp(500, function(){
            $(".alert-dismissible").alert('close');
        });


       if(<?=$existe_objetivo?> == 0){
        $('#tabla_objetivos').hide();
        
    }else{
    $("#add_objetivo0").hide();
    $('#sin_objetivos').hide();
    }

    });

</script>
