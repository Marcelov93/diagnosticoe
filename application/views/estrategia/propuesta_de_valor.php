
<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>

    .formulario {

        width:60%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }
    textarea {
    resize: none;
}
</style>

<script>
    var base_url = "<?php Print(base_url()); ?>";


</script>



<div class="contenedor">
    <br>
    <br>
    <br>
      
        <!--VER propuesta -->
    
       <div id="ver" class="formulario">

        <div class="col-sm-12 col-md-12">

            
            <br>
            <br>
            <p><h3>PROPUESTA DE VALOR</h3></p><br>
            <p><strong><blockquote  style="color:white"><?=nl2br($propuesta->PV_PROPUESTA)?></blockquote></strong></p>
            <br>
         
            <br>
            <br>


        </div>

               <div class="col-sm-12 col-md-12" >
            <button id="editarB" class="btn btn-primary btn-block btn-large">ACTUALIZAR</button>

          
            <br>
            <div class="col-md-10"></div>
        <a id="back2" class="btn btn-success col-md-2">VOLVER</a>
        <br>
        </div>

    </div>
    <br>
    <!--FIN VER propuesta -->
    <!-- insertar propuesta-->
    <form id="insertar" action="<?php echo base_url();?>Estrategia/insertar_propuesta_de_valor" method="post" class="formulario">
 <div class="col-md-10"></div>
        <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda <i class="glyphicon glyphicon-question-sign"></i></button>
 <p><h3>INGRESAR PROPUESTA</h3></p>
       

        <br>

        <div class="col-sm-12 col-md-12">



            <label for="propuesta">Propuesta de valor</label>
            <br>
            <br>
            <textarea class="form-control" rows="4" name="propuesta" id="propuesta"  required></textarea>

            <br>

        </div>


               <div class="col-sm-12 col-md-12" >
            <button type="submit" class="btn btn-primary btn-block btn-large">Guardar</button>

            <br>
            <br>
            <div class="col-md-10"></div>
            <a id="back1" class="btn btn-success col-md-2">VOLVER</a>

        </div>


    </form>
    
       <!--fin insertar propuesta-->
    
    <!--editar propuesta -->
     <form id="editar" action="<?php echo base_url();?>Estrategia/update_propuesta" method="post" class="formulario">


<p><h3>EDITAR PROPUESTA</h3></p><br>
        <div class="col-sm-12 col-md-12">



            <label for="mision">Propuesta de Valor</label>
            <br>
            <br>
            <textarea class="form-control" rows="5" name="propuesta" id="propuesta" required><?=$propuesta->PV_PROPUESTA?></textarea>

            <br>

        </div>

        <div class="col-sm-12 col-md-12" >
            <button type="submit" class="btn btn-primary btn-block btn-large">Actualizar</button>

            <br>
            <br>

        </div>
        <div class="col-md-10"></div>
        <a id="backeditar" class="btn btn-success col-md-2">VOLVER</a>

    </form>
    <br>
    <!--fin editar propuesta -->

    <div id="ayuda" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Definici&oacute;n</h4>
                </div>
                <div class="modal-body">
                    <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong>Propuesta de valor:</strong><br> La propuesta de valor debe responder qu&eacute; se va a ofrecer y para qui&eacute;n.
                        Aqu&iacute; algunas preguntas de ayuda:
                        Que valor proporcionamos a nuestros clientes?
                        Que problema de nuestros clientes ayudamos a solucionar?
                        Que necesidades de los clientes satisfacemos?
                        Que paquetes de productos o servicios ofrecemos a cada segmento de mercado?
                        Que espera el cliente, para superarle las expectativas?</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
                </div>
            </div>

        </div>
    </div>

</div>


<script>

 
    $(document).ready(function() {

$('#insertar').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});

if(<?=$existe?>==1){
        $('#insertar').hide();
        $('#editar').hide();
    }else{
        $('#ver').hide();
         $("#editar").hide();
    }

$("#editarB").click(function() {
     $("#editar").show(500);
 $("#ver").hide();

});


        $('#back1').click(function(){
            parent.history.back();
            return false;
        });
        $('#back2').click(function(){
           parent.history.back();
            return false;
        });
        $('#backeditar').click(function(){
           $("#editar").hide();
           $('#ver').show(500);
        });
    });


</script>

