
<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>

    .formulario {

        width:60%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }

    p{
        color: black;
    }
            textarea {
    resize: none;
}
body.modal-open {
    overflow: visible;
}

h3:first-child {
  text-align: left;
}
</style>

<script>
    var base_url = "<?php Print(base_url()); ?>";


</script>



<div class="contenedor">
   <?php
    if($this->session->flashdata('registrostatus'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos se han eliminado correctamente!</strong></p>
        </div>
        <br>
        <?php
    }else if($this->session->flashdata('Exito'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos han sido guardados correctamente!</strong></p>
        </div>
        <br>
        <?php
    }
    ?>
    <br>
    <br>
    <h2>FACTORES CRÍTICOS DE ÉXITO</h2>
    <br>
    <br>


    <div class="formulario">

        <h4 style="color:white">Objetivo Estratégico: "<?=$descripcion->OE_DESCRIPCION?>"</h4><hr>
        <table id="tabla_factores" class="table">
            <thead>
            <tr class="active">
                <th width="90%"><p>Factor</p></th>
                <th width="5%"><p>Editar</p></th>
                <th width="5%"><p>Eliminar</p></th>


            </tr>
            </thead>
            <tbody>

            <?php foreach ($factores as $value) {?>

            <tr class="active">
                <td><p><?=$value->FCE_FACTOR?></p></td>
                <td align="center"><a data-toggle="modal" data-target="#<?=$value->FCE_ID?>"><span class="glyphicon glyphicon-pencil" </span></a></td>
                <td align="center"><a data-toggle="modal" data-target="#modal<?=$value->FCE_ID?>"><span class="glyphicon glyphicon-trash" </span></a></td>
<!--modal eliminar-->
                     <div id="modal<?=$value->FCE_ID?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                 
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Atención !</h4>
                            </div>
                            <div class="modal-body">

                                
                                <p><strong>Está seguro que desea eliminar este factor?</strong></p><br>
                              

                            </div>
                            <div class="modal-footer">
                                <button onclick="javascript:window.location='<?php echo base_url('Estrategia/eliminar_factor')."/".$value->FCE_ID."/".$objetivo ?>'" type="button" class="btn btn-danger" data-dismiss="modal">ELIMINAR</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>

                    </div>
                </div>
               <!--modal-->
               
<!-- modal editar-->
<div id="<?=$value->FCE_ID?>" class="modal fade" role="dialog">
  <div class="modal-dialog ">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button
       <h4 class="modal-title">EDITAR FACTOR </h4>
                </div>
                 <div class="modal-body">                   
                     <form id="update_objetivo" action="<?=base_url()?>Estrategia/editar_fce" method="post">
            <br>
            <br>
            <input type="hidden" value="<?=$objetivo?>" name="objetivo_id" id="objetivo_id">
            <input type="hidden" value="<?=$value->FCE_ID?>" name="fce_id" id="fce_id">
            <textarea class="form-control" rows="4" name="factor" id="factor" required><?=$value->FCE_FACTOR?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
      </div>
 
    
    </div>

  </div>
</div>
<!--fin modal editar-->
               
            </tr>
            </tbody>
            <?php }?>
             <tr>
                <td><button id="add_factor" type="button" class="btn btn-success" data-toggle="modal" data-target="#agregar_factor" >
                        <span class="glyphicon glyphicon-plus" ></span> Agregar Factor</button></td>
            </tr>
        </table>

        <button id="add_factor0" type="button" class="btn btn-success" data-toggle="modal" data-target="#agregar_factor" >
                        <span class="glyphicon glyphicon-plus" ></span> Agregar Factor</button>

        <div class="col-md-10"></div>

        <a href="<?php echo base_url()?>Estrategia/ver_objetivos" class="btn btn-success col-md-2">VOLVER</a>


        
  <!--insertar factor-->
<div id="agregar_factor" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Ingresar Objetivo Estratégico </h4>
                </div>
                 <div class="modal-body">
        <form id="insertar_factor" action="<?php echo base_url()?>Estrategia/insertar_fce" method="post">
          
            <br>
            <input type="hidden" value="<?=$objetivo?>" name="objetivo_id" id="objetivo_id">
            <textarea class="form-control" rows="4" name="factor" id="factor"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
      
      </div>
 
    
    </div>

  </div>
</div>     
<!-- fin insertar factor -->

       
        
        
        
        
    </div>
    <br>
    <br>
</div>


<script>


    $(document).ready(function() {
$('#agregar_factor').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});

        $(".alert-dismissible").fadeTo(2500, 500).slideUp(500, function(){
            $(".alert-dismissible").alert('close');
        });
        
       if(<?=$existe_factor?> == 0){
        $('#tabla_factores').hide();
    }else{
    $("#add_factor0").hide();
    }
        
    });

</script>

