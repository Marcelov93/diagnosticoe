<link rel="stylesheet"  href="<?=base_url()?>assets/css/formularios.css" type="text/css" media="all" />

<style>
    .contenedor-perfil{
        width:40%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;
    }
textarea {
    resize: none;
}
    


</style>

<div class="contenedor">
<?php
    if($this->session->flashdata('Exito'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos han sido guardados correctamente!</strong></p>
        </div>
        <br>
        <?php
    }
    ?>
    <br>
    <br>
 <div class="col-md-9"></div>
    <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda <i class="glyphicon glyphicon-question-sign"></i></button>
    <h1>ANÁLISIS 4P</h1>
   
    <br>
    <br>
    <br>
</div>
<div class="contenedor-perfil">

    <a data-toggle="modal" data-target="#producto"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i><strong> PRODUCTO</strong></li></a> <br>
    <a data-toggle="modal" data-target="#precio"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i><strong>PRECIO</strong></li></a> <br>
    <a data-toggle="modal" data-target="#plaza"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i><strong>PLAZA</strong></li></a> <br>
    <a data-toggle="modal" data-target="#promocion"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i><strong>PROMOCIÓN</strong></li></a> <br>
   
    <br>
    <div class="col-md-10"></div>
    <a href="<?php echo base_url('Estrategia/menu') ?>" class="btn btn-success col-md-2">VOLVER</a>


<!--modal producto --> 
<div id="producto" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Análisis 4P </h4>
                </div>
                 <div class="modal-body">
             <form id="insertar_producto" action="<?php echo base_url();?>Estrategia/insertar_4p" method="post" class="formulario">

      

           <p><strong>Ingresar Producto</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="18" name="producto" id="producto"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
                     
          <form id="editar_producto" action="<?php echo base_url();?>Estrategia/editar_4p" method="post" class="formulario">

       

           <p><strong>Actualizar Producto</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="18" name="producto" id="producto"  required><?=$producto->ACP_DESCRIPCION?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
         <div id="ver_producto" class="formulario">
            <p><strong>Producto</strong></p>
            <br>
            <br>
                <h4><?=nl2br($producto->ACP_DESCRIPCION)?></h4>
            
            <br>
            <div class="modal-footer">
                    <button id="editarProducto" type="button" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </div>
      </div>
 
    
    </div>

  </div>
</div>     
</div>     
   
<!-- fin modal producto -->     
<!--modal precio --> 
<div id="precio" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Análisis 4P </h4>
                </div>
                 <div class="modal-body">
             <form id="insertar_precio" action="<?php echo base_url();?>Estrategia/insertar_4p" method="post" class="formulario">

      

           <p><strong>Ingresar Precio</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="18" name="precio" id="precio"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
                     
          <form id="editar_precio" action="<?php echo base_url();?>Estrategia/editar_4p" method="post" class="formulario">

       

           <p><strong>Actualizar Precio</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="18" name="precio" id="precio"  required><?=$precio->ACP_DESCRIPCION?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
         <div id="ver_precio" class="formulario">
            <p><strong>Precio</strong></p>
            <br>
            <br>
                <h4><?=nl2br($precio->ACP_DESCRIPCION)?></h4>
            
            <br>
            <div class="modal-footer">
                    <button id="editarPrecio" type="button" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </div>
      </div>
 
    
    </div>

  </div>
</div>     
</div>     
   
<!-- fin modal precio -->     
<!--modal plaza --> 
<div id="plaza" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Análisis 4P </h4>
                </div>
                 <div class="modal-body">
             <form id="insertar_plaza" action="<?php echo base_url();?>Estrategia/insertar_4p" method="post" class="formulario">

      

           <p><strong>Ingresar Plaza</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="18" name="plaza" id="plaza"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
                     
          <form id="editar_plaza" action="<?php echo base_url();?>Estrategia/editar_4p" method="post" class="formulario">

       

           <p><strong>Actualizar Plaza</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="18" name="plaza" id="plaza"  required><?=$plaza->ACP_DESCRIPCION?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
         <div id="ver_plaza" class="formulario">
            <p><strong>Plaza</strong></p>
            <br>
            <br>
                <h4><?=nl2br($plaza->ACP_DESCRIPCION)?></h4>
            
            <br>
            <div class="modal-footer">
                    <button id="editarPlaza" type="button" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </div>
      </div>
 
    
    </div>

  </div>
</div>     
</div>     
   
<!-- fin modal plaza -->     
<!--modal promocion --> 
<div id="promocion" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

       <h4 class="modal-title">Análisis 4P </h4>
                </div>
                 <div class="modal-body">
             <form id="insertar_promocion" action="<?php echo base_url();?>Estrategia/insertar_4p" method="post" class="formulario">

      

           <p><strong>Ingresar Promoción</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="18" name="promocion" id="promocion"  required></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Guardar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </form>
                     
          <form id="editar_promocion" action="<?php echo base_url();?>Estrategia/editar_4p" method="post" class="formulario">

       

           <p><strong>Actualizar Promoción</strong></p>
            <br>
            <br>
            <textarea class="form-control" rows="18" name="promocion" id="promocion"  required><?=$promocion->ACP_DESCRIPCION?></textarea>

            <br>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Actualizar</button>  
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            
          </form>
                     
         <div id="ver_promocion" class="formulario">
            <p><strong>Promoción</strong></p>
            <br>
            <br>
                <h4><?=nl2br($promocion->ACP_DESCRIPCION)?></h4>
            
            <br>
            <div class="modal-footer">
                    <button id="editarPromocion" type="button" class="btn btn-info">Actualizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
         </div>
      </div>
 
    
    </div>

  </div>
</div>

<div id="ayuda" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Definiciones</h4>
            </div>
            <div class="modal-body">

                Las actividades de marketing son esenciales para llevar productos y servicios de la empresa a sus consumidores.
                Las "4P" reflejan una fórmula simple para abordar sus elementos centrales.
                <br>
                <br>

                <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong> Producto:</strong><br> ¿Qué vende exactamente? ¿Qué beneficios ofrece a sus clientes?
                    Qué características definen su producto o servicio? Considere no solo el qué, sino el cómo: envase, nombre, forma de entrega, atención, tiempos, etc.

                </p>
                <br>
                <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong> Precio:</strong><br> ¿Qué valor tiene lo que ofrece a sus clientes? Cuánto vale algo similar en el mercado?
                    ¿Su producto va a ser exclusivo o económico? La fijación del precio de un producto no es solo el resultado de sus costos más la ganancia esperada,
                    sino un complejo proceso que impacta en la imagen ante los clientes.  </p>
                <br>
                <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong> Plaza:</strong><br> Las definiciones sobre canal de ventas y formas de comercialización impactan en su negocio.
                    No es lo mismo el marketing mayorista, minorista o de venta hacia el gobierno y organismos públicos.
                    Venta directa, distribuidores, venta online y franquicias son opciones comerciales que puede evaluar y que implican distintos acercamientos a los clientes.
                </p>
                <br>
                <p align="justify"><i class="glyphicon glyphicon-chevron-right"></i><strong> Promoción:</strong> <br>¿De qué forma va a dar a conocer su producto o servicio? ¿Dónde están sus clientes?
                    De acuerdo a los medios que utilice, los mensajes que elija y la inversión que realice,
                    puede alcanzar a distintos públicos. Hay que tener en cuenta formas de comunicación tradicionales,
                    como los avisos, pero también las promociones y descuentos, y las campañas de fidelización.  </p>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
            </div>
        </div>

    </div>
</div>

</div>     
   
<!-- fin modal plaza -->     
      
    
    
</div>
</div>
<br>
<br>



<script>
    
$(document).ready (function(){
$(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});

$('#insertar_producto').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
$('#insertar_precio').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
$('#insertar_plaza').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});
$('#insertar_promocion').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});

if(<?=$existe_precio?>==1){
        $('#insertar_precio').hide();
        $('#editar_precio').hide();
    }else{
        $('#ver_precio').hide();
         $("#editar_precio").hide();
    }

$("#editarPrecio").click(function() {
     $("#editar_precio").show(500);
 $("#ver_precio").hide();

});
if(<?=$existe_producto?>==1){
        $('#insertar_producto').hide();
        $('#editar_producto').hide();
    }else{
        $('#ver_producto').hide();
         $("#editar_producto").hide();
    }

$("#editarProducto").click(function() {
     $("#editar_producto").show(500);
 $("#ver_producto").hide();

});

if(<?=$existe_plaza?>==1){
        $('#insertar_plaza').hide();
        $('#editar_plaza').hide();
    }else{
        $('#ver_plaza').hide();
         $("#editar_plaza").hide();
    }

$("#editarPlaza").click(function() {
     $("#editar_plaza").show(500);
 $("#ver_plaza").hide();

});
if(<?=$existe_promocion?>==1){
        $('#insertar_promocion').hide();
        $('#editar_promocion').hide();
    }else{
        $('#ver_promocion').hide();
         $("#editar_promocion").hide();
    }

$("#editarPromocion").click(function() {
     $("#editar_promocion").show(500);
 $("#ver_promocion").hide();

});





 });
</script>
