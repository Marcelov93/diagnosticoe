<link rel="stylesheet"  href="<?=base_url()?>assets/css/estilo.css" type="text/css" media="all" />



<div class="contenedor">
    <?php
    if($this->session->flashdata('Exito'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong> Los datos han sido guardados correctamente!</strong></p>
        </div>
        <br>
        <?php
    }
    ?>
    <br>
    <br>
    <br>
    <h1>PLANIFICACIÓN ESTRATÉGICA</h1>
    <br>
    <br>
</div>
<div class="contenedor-perfil">



  
    <a href="<?=base_url()?>Estrategia/mision_vision"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i>
            <strong>MISIÓN Y VISIÓN</strong></li></a>
    <br>
  
    <a href="<?=base_url()?>Estrategia/propuesta_de_valor"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i>
            <strong>PROPUESTA DE VALOR</strong></li></a><br>

<!--    <a href="<?=base_url()?>Estrategia/obj_est"><li  class="list-group-item">OBJETIVOS ESTRATÉGICOS</li></a>
    <br>
    <a class="factores" href="<?=base_url()?>Estrategia/add_factores"><li  class="list-group-item">FACTORES CRÍTICOS DE ÉXITO</li></a>-->
   
 <a href="<?=base_url()?>Estrategia/ver_foda"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i>
         <strong>ANÁLISIS FODA</strong></li></a>
    <br>
    <a href="<?=base_url()?>Estrategia/pest"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i>
            <strong>ANÁLISIS PEST</strong></li></a>
    <br>
    <a href="<?=base_url()?>Estrategia/ver_objetivos"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i>
            <strong>OBJETIVOS ESTRATÉGICOS</strong></li></a>
    <br>


    <a href="<?=base_url()?>Estrategia/analisis_4p"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i>
            <strong>ANÁLISIS 4 P</strong></li></a> <br>
     


    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Atención !</h4>
                </div>
                <div class="modal-body">
                    <p>Primero debe agregar por lo menos un Objetivo Estratégico.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                </div>
            </div>

        </div>
    </div>
    
</div>


<script>
$(document).ready (function(){
$(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});

    if(<?=$objetivos?>==0){

        $('a[href*="<?=base_url()?>Estrategia/add_factores"]').attr('href' , '#');

    }

    if( <?=$objetivos?> == 0){

        $( ".factores" ).click(function(){

            $('#myModal1').modal('show');
        });

 }
});
 </script>