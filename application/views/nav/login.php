<style>
    
    @import url(http://fonts.googleapis.com/css?family=Open+Sans);
.btn { display: inline-block; *display: inline; *zoom: 1; padding: 4px 10px 4px; margin-bottom: 0; font-size: 13px; line-height: 18px; color: #333333; text-align: center;text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75); vertical-align: middle; background-color: #f5f5f5; background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6); background-image: -ms-linear-gradient(top, #ffffff, #e6e6e6); background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6)); background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6); background-image: -o-linear-gradient(top, #ffffff, #e6e6e6); background-image: linear-gradient(top, #ffffff, #e6e6e6); background-repeat: repeat-x; filter: progid:dximagetransform.microsoft.gradient(startColorstr=#ffffff, endColorstr=#e6e6e6, GradientType=0); border-color: #e6e6e6 #e6e6e6 #e6e6e6; border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); border: 1px solid #e6e6e6; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); cursor: pointer; *margin-left: .3em; }
.btn:hover, .btn:active, .btn.active, .btn.disabled, .btn[disabled] { background-color: #e6e6e6; }
.btn-large { padding: 9px 14px; font-size: 15px; line-height: normal; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; }
.btn:hover { color: #333333; text-decoration: none; background-color: #e6e6e6; background-position: 0 -15px; -webkit-transition: background-position 0.1s linear; -moz-transition: background-position 0.1s linear; -ms-transition: background-position 0.1s linear; -o-transition: background-position 0.1s linear; transition: background-position 0.1s linear; }
.btn-primary, .btn-primary:hover { text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25); color: #ffffff; }
.btn-primary.active { color: rgba(255, 255, 255, 0.75); }
.btn-primary { background-color: #4a77d4; background-image: -moz-linear-gradient(top, #6eb6de, #4a77d4); background-image: -ms-linear-gradient(top, #6eb6de, #4a77d4); background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#6eb6de), to(#4a77d4)); background-image: -webkit-linear-gradient(top, #6eb6de, #4a77d4); background-image: -o-linear-gradient(top, #6eb6de, #4a77d4); background-image: linear-gradient(top, #6eb6de, #4a77d4); background-repeat: repeat-x; filter: progid:dximagetransform.microsoft.gradient(startColorstr=#6eb6de, endColorstr=#4a77d4, GradientType=0);  border: 1px solid #3762bc; text-shadow: 1px 1px 1px rgba(0,0,0,0.4); box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.5); }
.btn-primary:hover, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled] { filter: none; background-color: #4a77d4; }
.btn-block { width: 100%; display:block; }

* { -webkit-box-sizing:border-box; -moz-box-sizing:border-box; -ms-box-sizing:border-box; -o-box-sizing:border-box; box-sizing:border-box; }

html { width: 100%; height:100%; overflow:hidden; }

body { 
    width: 100%;
    height:100%;
    font-family: 'Open Sans', sans-serif;
    background-image: url("<?=base_url()?>assets/css/banner.jpg");
        background-size: cover;
}
.login {
    width:20%;
    max-width:1000px;
    margin:auto;
    overflow:hidden;
}

    .titulo {
        width:40%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;
    }


.titulo h1 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }

input { 
    width: 100%; 
    margin-bottom: 10px; 
    background: rgba(0,0,0,0.3);
    border: none;
    outline: none;
    padding: 10px;
    font-size: 13px;
    color: #fff;
    text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
    border: 2px solid rgba(0,0,0,0.3);
    border-radius: 4px;
    box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
    -webkit-transition: box-shadow .5s ease;
    -moz-transition: box-shadow .5s ease;
    -o-transition: box-shadow .5s ease;
    -ms-transition: box-shadow .5s ease;
    transition: box-shadow .5s ease;
        
}


::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:    white;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
   color:    white;
   opacity:  1;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
   color:   white;
   opacity:  1;
}
:-ms-input-placeholder { /* Internet Explorer 10-11 */
   color:   white;
}
::-ms-input-placeholder { /* Microsoft Edge */
   color:    white;
}
input:focus { box-shadow: inset 0 -5px 45px rgba(100,100,100,0.4), 0 1px 1px rgba(255,255,255,0.2); }
</style>
<?php
if($this->session->flashdata('registrostatus'))
{
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert" id="alerta2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <i class="glyphicon glyphicon-remove"> </i><strong> Usuario o contraseña incorrecta!</strong> Inténtelo nuevamente.
    </div>
    <br>
    <?php
}else{
   if($this->session->flashdata('Exito'))
{
    ?>
    <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <i class="glyphicon glyphicon-send"> </i><strong> Se ha enviado una nueva contraseña a tu correo!</strong>
    </div>
    <br>
    <?php
}else{
    
    if($this->session->flashdata('Fail'))
{
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert" id="alerta2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Hubo un error al enviar el correo!</strong> Intentalo más tarde.
    </div>
    <br>
    <?php
}else{
    if($this->session->flashdata('Exito2'))
{
    ?>
    <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <i class="glyphicon glyphicon-ok"> </i><strong> Tu cuenta ha sido creada exitosamente!</strong> ahora puedes iniciar sesión.
    </div>
    <br>
    <?php
}
    
}
    
}



}
?>



<div class="titulo">
    <br>
    <br>
    <br>
    <h1>DIAGNÓSTICO EMPRESARIAL</h1>
    <br>
    <br>
</div>
<div class="login">
    <form action="<?php echo base_url();?>Welcome/recibirdatos" method="post">
        
        <input type="email" name="correo" placeholder="Correo" id="correo" required="required" />
        <input type="password" name="password" placeholder="Contraseña" id="password" required="required" />
        
        <button type="submit" class="btn btn-primary btn-block btn-large">Iniciar Sesión <i class="glyphicon glyphicon-log-in"></i> </button>
        <br>

        <p style="color:white" class="message"><strong>Olvidaste tu contraseña?</strong> <a href="<?=base_url()?>Usuarios/reset_password"><strong>Click aquí</strong></a>
       
            <br>

    </form>
    <br>
    
    
    

</div>
        


<script>
    $(document).ready(function() {
   
        $(".alert-dismissible").fadeTo(3500, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});
});
</script>