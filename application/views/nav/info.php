<link rel="stylesheet"  href="<?=base_url()?>assets/css/estilo.css" type="text/css" media="all" />

<style>
    .contenedor-perfil{
        width:60%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }

</style>

<div class="contenedor">


</div>
<br>
<br>
<div class="contenedor-perfil">

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Bienvenido a MIPEUp</h3>
        </div>
        <div class="panel-body">
            Con MIPEUp puedes conocer la situaci&oacute;n actual de tu empresa, mediante un diagn&oacute;stico en los siguientes dominios:
            <br>
            <br>
            <ul class="list-group">
                <strong>
                <li class="list-group-item">Gobierno empresarial y estrategia</li>
                <li class="list-group-item">Administraci&oacute;n y Contabilidad</li>
                <li class="list-group-item">Personas</li>
                <li class="list-group-item">Modelo de negocios</li>
                <li class="list-group-item">Procesos</li>
                <li class="list-group-item">Medici&oacute;n, an&aacute;lisis y desempe&ntilde;o</li>
                    </strong>
                <br>
                Adem&aacute;s puedes realizar un plan de mejora para solucionar las deficiencias detectadas, y formular un plan estrat&eacute;gico para tu compa&ntilde;ia.
            </ul>
            <div class="col-md-6 col-sm-6">
                <a href="<?=base_url()?>" class="btn btn-success col-md-12 col-sm-12">IR A INICIO</a>
            </div>
            <div class="col-md-6 col-sm-6">
                <a href="<?=base_url()?>Usuarios/registro" class="btn btn-info col-md-12 col-sm-12">REGISTRARME</a>
            </div>


        </div>
    </div>

</div>