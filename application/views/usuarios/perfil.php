<link rel="stylesheet"  href="<?=base_url()?>assets/css/estilo.css" type="text/css" media="all" />

<style>
    
h3 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:left; }
h2 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
h1 { color: #fff; text-shadow: 0 0 10px rgba(0,0,0,0.3); letter-spacing:1px; text-align:center; }
</style>


<div class="contenedor">
   <?php
    if($this->session->flashdata('Exito'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong>La contraseña se ha actualizado correctamente!</strong></p>
        </div>
        <br>
        <?php
    }else{
        if($this->session->flashdata('Exito1'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
          <p> <i class="glyphicon glyphicon-saved"></i> <strong>Tus datos se han actualizado correctamente!</strong></p>
        </div>
        <br>
        <?php
    }else{
        if($this->session->flashdata('Exito2'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
         <p>  <i class="glyphicon glyphicon-saved"></i> <strong>Tus datos se han ingresado correctamente!</strong></p>
        </div>
        <br>
        <?php
    }else{
        if($this->session->flashdata('Fail'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><strong>Se ha producido un error con la base de datos. Intente nuevamente</strong></p>
        </div>
        <br>
        <?php
    }
    }
        
    }
    }
?>
    <br>

    <div class="col-md-10"></div>
    <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda <i class="glyphicon glyphicon-question-sign"></i></button>
  
    <br>
</div>
<div class="contenedor-perfil">

<table width="100%">
<tbody>
<tr>
    <td ><h3>Nombre:</h3></td>
    <td><h3><?=$usuario->USU_NOMBRES.' '.$usuario->USU_APELLIDOS?></h3></td>
</tr>
<tr>
    <td><h3>RUT:</h3></td>
<td><h3><?= $usuario->USU_RUT?></h3></td>
</tr>
<tr>
<td><h3> Empresa:</h3></td>
<td><h3><?=$empresa->EMP_NOMBRE_FANTASIA ?></h3></td>
</tr>
</tbody>
</table>
  <br>
  <br>
  <br>
  


    <a class="editar_info_personal" href="<?=base_url()?>Usuarios/editar"><li  class="list-group-item"><i class="glyphicon glyphicon-refresh"></i>
            <strong> ACTUALIZAR INFORMACIÓN PERSONAL</strong></li></a>
    <br>
    <a id="ingresar_info_adicional" href="<?=base_url()?>Usuarios/infoadicional"><li  class="list-group-item"><i class="glyphicon glyphicon-chevron-right"></i>
            <strong> INGRESAR INFORMACIÓN ADICIONAL</strong></li></a>
    <a id="editar_info_adicional" href="<?=base_url()?>Usuarios/editar_infoadicional"><li  class="list-group-item"><i class="glyphicon glyphicon-refresh"></i>
            <strong> ACTUALIZAR INFORMACIÓN ADICIONAL</strong></li></a>
    <br>
    <a class="editar_info_empresa" href="<?=base_url()?>Empresa/mostrar"><li  class="list-group-item"><i class="glyphicon glyphicon-refresh"></i>
            <strong> ACTUALIZAR INFORMACIÓN EMPRESA</strong></li></a>
    <br>
    <a id="pdf_ficha_usuario"  href="<?=base_url()?>Usuarios/pdf_ficha_usuario" target="_blank"><li  class="list-group-item list-group-item-success">
            <i class="glyphicon glyphicon-print"></i><strong> Imprimir Ficha Empresario</strong></li></a>
    <br>
    <a id="pdf_ficha_empresa" href="<?=base_url()?>Usuarios/pdf_ficha_empresa" target="_blank"><li  class="list-group-item list-group-item-success">
            <i class="glyphicon glyphicon-print"></i><strong> Imprimir Ficha Empresa</strong></li></a>
    <br>
   


<div id="ayuda" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Instrucciones:</h4>
                </div>
                <div class="modal-body">
                    <i class="glyphicon glyphicon-chevron-right"></i> Seleccione una de las opciones para ingresar o actualizar la información, según corresponda.
                    <br>
                    <br>
                   <i class="glyphicon glyphicon-chevron-right"></i> Para cambiar su contraseña, seleccione <strong>ACTUALIZAR INFORMACIÓN PERSONAL</strong> y dirijase a la sección <strong>Cambiar contraseña.</strong>
                    <br>
                    <br>
                   <i class="glyphicon glyphicon-chevron-right"></i> Seleccione <strong>Imprimir ficha Empresario</strong> o <strong>Imprimir ficha Empresa</strong> para imprimir o guardar en formato
                    pdf la información correspondiente.<br><br>
                    <i class="glyphicon glyphicon-asterisk"></i> Para habilitar la opción <strong>Imprimir ficha empresario</strong> debe ingresar
                    los datos en la opción  <strong>INGRESAR INFORMACIÓN ADICIONAL</strong> previamente.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
                </div>
            </div>

        </div>
    </div>



<!-- Modal Continuar -->
</div>
    

<script>
    
       $('#ingresar_info_adicional').hide();
    $('#editar_info_adicional').hide();
    $('#pdf_ficha_usuario').hide();
$(document).ready(function() {

        $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});

 if(<?=$oan?>){
     $('#editar_info_adicional').show();
     $('#pdf_ficha_usuario').show();
 }else{
     $('#ingresar_info_adicional').show();
 }

});
</script>





