<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">
<script>
    var base_url = "<?php Print(base_url()); ?>";


</script>

<style>
    textarea{ 
	width: 100%; 
	margin-bottom: 10px; 
	background: rgba(0,0,0,0.3);
	border: none;
	outline: none;
	padding: 10px;
	font-size: 13px;
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
	border: 1px solid rgba(0,0,0,0.3);
	border-radius: 4px;
	box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
	-webkit-transition: box-shadow .5s ease;
	-moz-transition: box-shadow .5s ease;
	-o-transition: box-shadow .5s ease;
	-ms-transition: box-shadow .5s ease;
	transition: box-shadow .5s ease;
        resize: none;
}
</style>

<div class="contenedor">
    <br>
    <br>
    <h2>Actualizar Información Adicional</h2>
    <br>
    <br>
    <form id="crearform" action="<?=base_url()?>Usuarios/actualizar_infoadicional" method="POST" class="formulario">



      
                    <div class="col-sm-12 col-md-12">


<div class="form-group">
  <label for="nestudios">Nivel de Estudios:</label>
  <select style="	width: 100%; 
	margin-bottom: 10px; 
	background: rgba(0,0,0,0.3);
	border: none;
	outline: none;
	font-size: 13px;
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
	border: 1px solid rgba(0,0,0,0.3);
	box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
	-webkit-transition: box-shadow .5s ease;
	-moz-transition: box-shadow .5s ease;
	-o-transition: box-shadow .5s ease;
	-ms-transition: box-shadow .5s ease;
	transition: box-shadow .5s ease;"
        class="form-control" name="nestudios" id="nestudios">
      <?php if ($usuario->OAN_NIVELESTUDIOS == 'Superior'){
      echo '<option selected>Superior</option>';         
      }else{
          echo '<option>Superior</option>';    
      }
      if ($usuario->OAN_NIVELESTUDIOS == 'Enseñansa Media'){
      echo '<option selected>Enseñansa Media</option>';         
      }else{
          echo '<option>Enseñansa Media</option>';    
      }

      if ($usuario->OAN_NIVELESTUDIOS == 'Enseñansa Básica'){
      echo '<option selected>Enseñansa Básica</option>';         
      }else{
          echo '<option>Enseñansa Básica</option>';    
      }
            if ($usuario->OAN_NIVELESTUDIOS == 'Sin Estudios'){
      echo '<option selected>Sin Estudios</option>';         
      }else{
          echo '<option>Sin Estudios</option>';    
      }
      
      ?>
  </select>
</div>


                    </div>
        
                    


                    <div class="col-sm-12 col-md-12">

                                <label for="anosrubro">Nro de años en el rubro</label>
                                <input type="number"  name="anosrubro" id="anosrubro"  value="<?=$usuario->OAN_ANOSENELRUBRO?>" max="99" min="0"  width="100%" required>



                    </div>



                   <div class="col-sm-12 col-md-12">

                                <label for="anosindependiente">Nro de años como independiente</label>
                                <input type="number" name="anosindependiente" id="anosindependiente" value="<?=$usuario->OAN_ANOSCOMOINDEPENDIENTE?>" max="99" min="0"  width="100%" required>




                    </div>
        
        <div class="col-sm-12 col-md-12">


<div class="form-group">
  <label for="regresado">Ha regresado a ser trabajador dependiente durante el periodo(s) de emprendimiento?</label>
  <select style="	width: 100%; 
	margin-bottom: 10px; 
	background: rgba(0,0,0,0.3);
	border: none;
	outline: none;
	font-size: 13px;
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
	border: 1px solid rgba(0,0,0,0.3);
	box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
	-webkit-transition: box-shadow .5s ease;
	-moz-transition: box-shadow .5s ease;
	-o-transition: box-shadow .5s ease;
	-ms-transition: box-shadow .5s ease;
	transition: box-shadow .5s ease;"
        class="form-control" name="regresado" id="regresado">
      <?php if ($usuario->OAN_REGRESADOCOMOINDEPENDIENTE == 'SI'){
      echo '<option selected>SI</option>';         
      }else{
          echo '<option>SI</option>';    
      }
      if ($usuario->OAN_REGRESADOCOMOINDEPENDIENTE == 'NO'){
      echo '<option selected>NO</option>';         
      }else{
          echo '<option>NO</option>';    
      }

      ?>
  </select>
</div>
</div>



                   <div class="col-sm-12 col-md-12">

                                <label for="nemprendimientos">Nro de emprendimientos realizados a la fecha</label>
                                <input type="number" name="nemprendimientos" id="nemprendimientos" value="<?=$usuario->OAN_NUMEROEMPRENDIMIENTOS?>" max="99" minlength="0"  width="100%" required>




                    </div>
 <div class="col-sm-12 col-md-12">
<div class="form-group">
  <label for="relacion">Si ha tenido emprendimientos anteriores, tienen relación con el actual?</label>
  <select style="	width: 100%; 
	margin-bottom: 10px; 
	background: rgba(0,0,0,0.3);
	border: none;
	outline: none;
	font-size: 13px;
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
	border: 1px solid rgba(0,0,0,0.3);
	box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
	-webkit-transition: box-shadow .5s ease;
	-moz-transition: box-shadow .5s ease;
	-o-transition: box-shadow .5s ease;
	-ms-transition: box-shadow .5s ease;
	transition: box-shadow .5s ease;"
        class="form-control" name="relacion" id="relacion">
      <?php if ($usuario->OAN_RELACIONDEEMPRENDIMIENTOS == 'SI'){
      echo '<option selected>SI</option>';         
      }else{
          echo '<option>SI</option>';    
      }
      if ($usuario->OAN_RELACIONDEEMPRENDIMIENTOS == 'NO'){
      echo '<option selected>NO</option>';         
      }else{
          echo '<option>NO</option>';    
      }

      ?>
  </select>
</div>
</div>
        

                            <div class="col-sm-12 col-md-12">

                                <label for="motivaciones">Cuáles fueron sus principales razones para emprender?</label>
                                <textarea name="motivaciones" id="motivaciones" rows="5" value="<?=$usuario->OAN_MOTIVACIONES?>" maxlength="500" width="100%" required><?=$usuario->OAN_MOTIVACIONES?></textarea>

                    </div>


        <div class="col-sm-12 col-md-12" >
                        <button type="submit" class="btn btn-primary btn-block btn-large">Actualizar</button>

                        <br>
                        <br>

                    </div>

        <br>
        <br>
        <div class="col-md-10"></div>
        <div class="col-md-2 col-sm-2">
            <a id="back" class="btn btn-success col-md-12 col-sm-12">VOLVER</a>
        </div>
        <br>
        <br>


    </form>
    <br>
</div>
<!--nuevo formulario-->

<script>

    $(document).ready(function() {

        $('#back').click(function(){
            parent.history.back();
            return false;
        });
    });

</script>