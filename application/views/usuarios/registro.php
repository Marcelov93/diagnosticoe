<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">
<style>
    body{
         background-image: url("<?=base_url()?>assets/css/banner.jpg");
         background-size: cover;
    }
    
    ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:    white;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
   color:    white;
   opacity:  1;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
   color:   white;
   opacity:  1;
}
:-ms-input-placeholder { /* Internet Explorer 10-11 */
   color:   white;
}
::-ms-input-placeholder { /* Microsoft Edge */
   color:    white;
}
</style>
<script>
    var base_url = "<?php Print(base_url()); ?>";

</script>




<div class="contenedor">
    <div class="alert alert-danger alert-dismissible"  role="alert" id="alerta">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Un momento!</strong> Las contraseñas no coinciden.
    </div>
    <div class="alert alert-danger alert-dismissible"  role="alert" id="alerta2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Un momento!</strong> Ya existe una cuenta registrada con ese correo.
    </div>
    <div class="alert alert-danger alert-dismissible"  role="alert" id="alerta3">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Un momento!</strong> Ingresa un RUT correcto!
    </div>
    <br>
    <br>
    <br>
    <h1>Registro</h1>
    <br>
    <br>
    <form id="crearform" action="<?=base_url()?>Usuarios/insertar"  method="POST" class="formulario">
        
        
         <div class="col-sm-12 col-md-12">
                    <div class="col-sm-12 col-md-4">
                      

                            
                                <label for="nombre">Nombres</label>
                                <input type="text"  name="nombres" id="nombres" placeholder="Nombres" maxlength="65" minlength="3"  width="100%" required> 

                        
                    </div> 


                    <div class="col-sm-12 col-md-4">
                    
                                <label for="apellidop">Apellidos</label>
                                <input type="text"  name="apellidos" id="apellidos" placeholder="Apellidos " maxlength="45" minlength="3"  width="100%" required> 

                      

                    </div> 



                    <div class="col-sm-12 col-md-4">
                      
                                <label for="rut">RUT</label>
                                <input type="text" name="rut" id="rut" placeholder="12345678-3" maxlength="15" minlength="3"  width="100%" required> 
                                <b><p id= "error" > </p> </b>

                   

                    </div>
                    </div>
  
      
        
<div class="col-sm-12 col-md-12">
                    <div class="col-sm-12 col-md-4">
                      
                                <label for="clave">Contraseña</label>
                                <input type="password"  name="password" id="password" placeholder="Ingrese contraseña" maxlength="25" minlength="6"  width="100%" required>
                               


                    </div>
                    <div class="col-sm-12 col-md-4">

                        <label for="clave">Repita contraseña</label>
                        <input type="password"  name="password2" id="password2" placeholder="Repita contraseña" maxlength="25" minlength="6"  width="100%" required>
                         <b><p id= "error2" style="font-size: 10px; text-align: left; font-weight: bold; color:red;"> </p> </b>
                    </div>


                    <div class="col-sm-12 col-md-4">
                     
                                <label for="correo">Correo</label>
                                <input type="email"  name="correo" id="correo" placeholder="Correo electrónico" maxlength="65" minlength="3"  width="100%" autocomplete="off" required>
                                <b><p id= "error3" style="font-size: 10px; text-align: left; font-weight: bold; color: red;"> </p> </b>
        <br><!--onblur="checkcorreo(value)"-->
        <br>
                    </div>
                    </div>
        
        <div class="col-sm-12 col-md-12" >
                        <button type="submit" id="btnenviar" class="btn btn-primary btn-block btn-large">Registrarse</button>



                    </div>

        
    </form>
    <br>
</div>
<!--nuevo formulario-->

    <!--verifica si el rut es correcto-->
    <script>
        $('#rut').Rut({
            on_error: function () {
                document.getElementById('rut').style.borderColor = "red";
                document.getElementById("error").textContent = "RUT incorrecto. Pruebe sin puntos ni guión";
                $("#btnenviar").prop("disabled", true);

            },

            on_success: function () {
                document.getElementById('rut').style.borderColor = "transparent";
                document.getElementById("error").textContent = "";
                $("#btnenviar").prop("disabled", false);

                var rut = $('#rut').val();
                checkrut(rut);

            }
        });



//$('#correo').on('blur',function () {
//
//        if( this.value.length > 0){   
//           var correo = $('#correo').val();
//           checkcorreo(correo);
//
//     }
//});
$('#correo').keyup(function() {
		var correo = $('#correo').val();
      checkcorreo(correo);
	});

//$( "#correo" ).focus(function() {
//$("#btnenviar").prop("disabled", true);
//});


$('#password2').on('blur',function () {
    if( this.value.length > 0){
            var password1= $('#password').val();
           var password2 = $('#password2').val();
           
           if(password1!= password2){
            document.getElementById('password2').style.borderColor = "red";
            document.getElementById("error2").textContent = "Las contraseñas no coinciden";
            $("#btnenviar").prop("disabled", true);
        }else{
             document.getElementById('password2').style.borderColor = "transparent";
             document.getElementById("error2").textContent = "";
             $("#btnenviar").prop("disabled", false);
             $('#alerta').hide(500);
        }
            
     }
});


        //verifica si el rut ya existe 
        function checkrut(rut) {

            var base_url = "<?php Print(base_url()); ?>";

            if (rut.length > 0)
            {
                jQuery.ajax({
                    type: "POST",
                    url: base_url + "Usuarios/checkrut",
                    data: 'rut=' + rut,
                    cache: false,
                    success: function (response) {
                        if (response == 1) {

                            document.getElementById('rut').style.borderColor = "red";
                            document.getElementById("error").textContent = "El rut ingresado ya existe";
                            $("#btnenviar").prop("disabled", true);
                        } else {

                            document.getElementById('rut').style.borderColor = "transparent";
                            document.getElementById("error").textContent = "";
                            $("#btnenviar").prop("disabled", false);
                            $('#alerta3').hide(500);

                        }

                    }
                });

            }




        }
//
        function checkcorreo(correo) {

            var base_url = "<?php Print(base_url()); ?>";

            if (correo.length > 0)
            {
                jQuery.ajax({
                    type: "POST",
                    url: base_url + "Usuarios/checkcorreo",
                    data: 'correo=' + correo,
                    cache: false,
                    success: function (response) {
                        if (response == 1) {

                            document.getElementById('correo').style.borderColor = "red";
                            document.getElementById("error3").textContent = "Ya existe una cuenta registrada con este correo";
                            $("#btnenviar").prop("disabled", true);
                        } else {

                            document.getElementById('correo').style.borderColor = "transparent";
                            document.getElementById("error3").textContent = "";
                            $("#alerta2").hide(500);
                            $("#btnenviar").prop("disabled", false);

                        }

                    }
                });

            }




        }

    </script>


    <!--se envian los datos del formulario al controlador por ajax -->
    <script>
        $("#alerta").hide();
        $("#alerta2").hide();
        $("#alerta3").hide();
        $(document).ready(function () {


            $('#crearform').on('submit', function (e) {
            
           var password1 = $('#password').val();
           var password2 = $('#password2').val();
           if(password1 != password2){
                    
$('#alerta').show(500);
e.preventDefault();
               
            }
            
            if(document.getElementById("error3").textContent != ""){
                $('#alerta2').show(500);
                e.preventDefault();
            }
            if(document.getElementById("error").textContent != ""){
                $('#alerta3').show(500);
                e.preventDefault();
            }

    $(this).find(':input[type=submit]').prop('disabled', true);




        });
        
        
   
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
    
        
        });




    </script>