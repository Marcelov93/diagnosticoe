<?php


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);



// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Diagnostico Empresarial', "Ficha Antecedentes - mipeUp.cl\n ");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// Set some content to print
$html= '<h1>Ficha Antecedentes de la Empresa</h1><br><br><br>';
$html .="<h3><strong>Información del Empresario</strong></h3><br><br>";
$html .= '<table border="0">';    
$html .=  '<tbody>';
    $html .="<tr>";
    $html .='<td width="20%"><strong>Nombre:</strong></td>';
    $html .="<td>".$userdata->USU_NOMBRES." ".$userdata->USU_APELLIDOS."</td>";
     $html .="</tr>";
    $html .="<tr>";
    $html .='<td><strong>RUT:</strong></td>';
    $html .="<td>".$userdata->USU_RUT."</td>";
     $html .="</tr>";
    $html .="<tr>";
    $html .='<td><strong>Correo:</strong></td>';
    $html .="<td>".$userdata->USU_CORREO."</td>";
     $html .="</tr>";

$html .= '</tbody></table><br><br>';   

    

$html .="<h3><strong>Información de la Empresa</strong></h3><br><br><br>";

$html .= '<table border="0">';    
$html .=  '<tbody>';

     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Razón Social:</strong></td>';
    $html .='<td>'.$empresa->EMP_RAZON_SOCIAL."</td>";
     $html .='</tr><br>';
     
         $html .='<tr>';
    $html .='<td width="40%"><strong>Nombre de Fantasía:</strong></td>';
    $html .='<td>'.$empresa->EMP_NOMBRE_FANTASIA."</td>";
     $html .='</tr><br>';
     
     $html .='<tr>';
    $html .='<td width="40%"><strong>RUT:</strong></td>';
    $html .='<td>'.$empresa->EMP_RUT."</td>";
     $html .='</tr><br>';
     
         $html .='<tr>';
    $html .='<td width="40%"><strong>Rubro:</strong></td>';
    $html .='<td>'.$empresa->EMP_RUBRO."</td>";
     $html .='</tr><br>';
     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Teléfono:</strong></td>';
    $html .='<td>'.$empresa->EMP_TELEFONO."</td>";
     $html .='</tr><br>';
     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Correo Electrónico:</strong></td>';
    $html .='<td>'.$empresa->EMP_CORREO."</td>";
     $html .='</tr><br>';
     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Página Web:</strong></td>';
    $html .='<td>'.$empresa->EMP_PAGINA_WEB."</td>";
     $html .='</tr><br>';
     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Antigüedad:</strong></td>';
    $html .='<td>'.$empresa->EMP_ANTIGUEDAD."</td>";
     $html .='</tr><br>';
     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Ventas Mensuales Promedio:</strong></td>';
    $html .='<td>$ '.$empresa->EMP_VTASMENSUALESPROM.".-</td>";
     $html .='</tr><br>';
     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Costos Mensuales Promedio:</strong></td>';
    $html .='<td>$ '.$empresa->EMP_CTOSMENSUALESPROM.".-</td>";
     $html .='</tr><br>';
     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Utilidad Mensual Promedio:</strong></td>';
    $html .='<td>$ '.$empresa->EMP_UTILIDADMENSUALPROM.".-</td>";
     $html .='</tr><br>';
     
    $html .='<tr>';
    $html .='<td width="40%"><strong>Número de Empleados:</strong></td>';
    $html .='<td>'.$empresa->EMP_NROEMPLEADOS."</td>";
     $html .='</tr><br>';



    $html .= '</tbody></table><br><br><br>';



       
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('ficha_empresa.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
