
<link rel="stylesheet" type='text/css' href="<?= base_url() ?>assets/css/formularios.css">
<script>
    var base_url = "<?php Print(base_url()); ?>";


</script>




<div class="contenedor">
     <?php
    if($this->session->flashdata('Fail1'))
    {
        ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <p><strong>Contraseña Incorrecta! , </strong>no se han guardado los cambios.</p>
        </div>
        <br>
        <?php
    }else if($this->session->flashdata('Fail2'))
    {
        ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <p><strong>Las contraseñas no coinciden! , </strong>no se han guardado los cambios</p>
        </div>
        <br>
        <?php
    }
    ?>
    <br>
    <br>

    
    
    <form id="crearform" action="<?=base_url()?>Usuarios/actualizar" method="POST" class="formulario">
<h3 style="text-align:left">Actualizar Información Personal</h3>
<br>
    <br>
        <div class="col-sm-12 col-md-12">
            <div class="col-sm-6 col-md-6">



                <label for="nombre">Nombres</label>
                <input type="text"  name="nombres" id="nombres"  value="<?= $usuario->USU_NOMBRES ?>" placeholder="<?= $usuario->USU_NOMBRES ?>" maxlength="65" minlength="3"  width="100%" required> 


            </div> 


            <div class="col-sm-6 col-md-6">

                <label for="apellidop">Apellidos</label>
                <input type="text"  name="apellidos" id="apellidos" value="<?= $usuario->USU_APELLIDOS ?>" placeholder="<?= $usuario->USU_APELLIDOS ?>" maxlength="45" minlength="3"  width="100%" required> 



            </div> 




        </div>



        <div class="col-sm-12 col-md-12">

            <div class="col-sm-6 col-md-6">

                <label for="correo">Correo</label>
                <input type="email"  name="correo" id="correo" value="<?= $usuario->USU_CORREO ?>" placeholder="<?= $usuario->USU_CORREO ?>" maxlength="65" minlength="3"  width="100%" >
                <b><p id= "error3" style="font-size: 10px; text-align: left; font-weight: bold; color: red;"> </p> </b>
                <br>
                <br>
            </div>

        </div>

        <div class="col-sm-12 col-md-12" >
            <button id="btnenviar" type="submit" class="btn btn-primary btn-block btn-large">Actualizar</button>



        </div>


    </form>
    <br>
    
                <form class="formulario" action="<?php echo base_url();?>Usuarios/editar_password" method="post">
                    <h3 style="text-align: left">Cambiar Contraseña</h3><br><br>
                  <div class="col-md-12">  
                                      <div class="col-md-4">
                <label for="clave">Contraseña Actual</label>
                <input type="password"  name="password" id="password"  maxlength="25" minlength="6"  width="100%" required> 
                </div>
             
                <div class="col-md-4">
                    
                <label for="clave2">Nueva Contraseña</label>
                <input type="password"  name="password1" id="password1"  maxlength="25" minlength="6"  width="100%" required> 

                </div>
                    <div class="col-md-4">
                <label for="clave3">Confirmación de la nueva contraseña</label>
                <input type="password"  name="password2" id="password2"  maxlength="25" minlength="6"  width="100%" required> 

                <br>
                <br>
                </div>
                </div>
                <div class="col-sm-12 col-md-12" >
            <button type="submit" class="btn btn-primary btn-block btn-large">Actualizar</button>
                    <br>
                    <br>
                    <div class="col-md-10"></div>
                    <div class="col-md-2 col-sm-2">
                        <a id="back" class="btn btn-success col-md-12 col-sm-12">VOLVER</a>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>


        </div>
                <br>
                <br>
                <br>
            </form>

</div>




<!--se envian los datos del formulario al controlador por ajax -->
<script>
//    $('#correo').on('blur',function () {
//
//        if( this.value.length > 0){   
//           var correo = $('#correo').val();
//           checkcorreo(correo);
//
//     }
//});
$('#correo').keyup(function() {
		var correo = $('#correo').val();
      checkcorreo(correo);
	});
    
            function checkcorreo(correo) {

            var base_url = "<?php Print(base_url()); ?>";

            if (correo.length > 0)
            {
                jQuery.ajax({
                    type: "POST",
                    url: base_url + "Usuarios/checkcorreoEditar",
                    data: 'correo=' + correo,
                    cache: false,
                    success: function (response) {
                        if (response == 1) {

                            document.getElementById('correo').style.borderColor = "red";
                            document.getElementById("error3").textContent = "Ya existe una cuenta registrada con este correo";
                            $("#btnenviar").prop("disabled", true);
                        } else {

                            document.getElementById('correo').style.borderColor = "transparent";
                            document.getElementById("error3").textContent = "";
                            $("#alerta2").hide(500);
                            $("#btnenviar").prop("disabled", false);

                        }

                    }
                });

            }




        }
    
    
    
    $(document).ready(function () {


          
        $(".alert-dismissible").fadeTo(3000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});
        $('#back').click(function(){
            parent.history.back();
            return false;
        });
        
        
         $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
        
        



        });

        





</script>