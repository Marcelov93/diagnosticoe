
<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">
<script>
    var base_url = "<?php Print(base_url()); ?>";


</script>

<style>
    textarea{ 
	width: 100%; 
	margin-bottom: 10px; 
	background: rgba(0,0,0,0.3);
	border: none;
	outline: none;
	padding: 10px;
	font-size: 13px;
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
	border: 1px solid rgba(0,0,0,0.3);
	border-radius: 4px;
	box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
	-webkit-transition: box-shadow .5s ease;
	-moz-transition: box-shadow .5s ease;
	-o-transition: box-shadow .5s ease;
	-ms-transition: box-shadow .5s ease;
	transition: box-shadow .5s ease;
        resize: none;
}
</style>

<div class="contenedor">
    <br>
    <br>
    <h2>Información Adicional</h2>
    <br>
    <br>
    <form id="crearform" action="<?=base_url()?>Usuarios/insertarInfoAdicional" method="POST" class="formulario">
        
        
   
 <div class="col-sm-12 col-md-12">
<div class="form-group">
  <label for="nestudio">Nivel de Estudios:</label>
  <select style="	width: 100%; 
	margin-bottom: 10px; 
	background: rgba(0,0,0,0.3);
	border: none;
	outline: none;
	font-size: 13px;
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
	border: 1px solid rgba(0,0,0,0.3);
	box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
	-webkit-transition: box-shadow .5s ease;
	-moz-transition: box-shadow .5s ease;
	-o-transition: box-shadow .5s ease;
	-ms-transition: box-shadow .5s ease;
	transition: box-shadow .5s ease;"
        class="form-control" name="nestudio" id="nestudios">
     
    <option selected>Seleccione</option>        
    <option>Superior</option>        
    <option>Enseñansa Media</option>   
    <option>Enseñansa Básica</option>    
    <option>Sin Estudios</option>


  </select>
</div>
</div>
                    

                    <div class="col-sm-12 col-md-12">
                    
                                <label for="anosrubro">Nro de años en el rubro</label>
                                <input type="number"  name="anosrubro" id="anosrubro"  placeholder="N° años" max="99" min="0"  width="100%" required> 

                      

                    </div> 



                   <div class="col-sm-12 col-md-12">
                      
                                <label for="anosindependiente">Nro de años como independiente</label>
                                <input type="number" name="anosindependiente" id="anosindependiente" placeholder="N° años" max="99" minlength="0"  width="100%" required> 
                               

                   

                    </div>
                   
        
         <div class="col-sm-12 col-md-12">
<div class="form-group">
  <label for="regresado">Ha regresado a ser trabajador dependiente durante el periodo(s) de emprendimiento?</label>
  <select style="	width: 100%; 
	margin-bottom: 10px; 
	background: rgba(0,0,0,0.3);
	border: none;
	outline: none;
	font-size: 13px;
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
	border: 1px solid rgba(0,0,0,0.3);
	box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
	-webkit-transition: box-shadow .5s ease;
	-moz-transition: box-shadow .5s ease;
	-o-transition: box-shadow .5s ease;
	-ms-transition: box-shadow .5s ease;
	transition: box-shadow .5s ease;"
        class="form-control" name="regresado" id="regresado">
     
    <option selected>Seleccione</option>        
    <option>SI</option>        
    <option>NO</option>   

  </select>
</div>
</div>
        

                   <div class="col-sm-12 col-md-12">
                      
                                <label for="nemprendimientos">Nro de emprendimientos realizados a la fecha</label>
                                <input type="number" name="nemprendimientos" id="nemprendimientos" placeholder="N° emprendimientos" max="99" min="0"  width="100%" required> 
                               

                   

                    </div>

<div class="col-sm-12 col-md-12">
<div class="form-group">
  <label for="relacion">Si ha tenido emprendimientos anteriores, tienen relación con el actual?</label>
  <select style="	width: 100%; 
	margin-bottom: 10px; 
	background: rgba(0,0,0,0.3);
	border: none;
	outline: none;
	font-size: 13px;
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
	border: 1px solid rgba(0,0,0,0.3);
	box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px 1px rgba(255,255,255,0.2);
	-webkit-transition: box-shadow .5s ease;
	-moz-transition: box-shadow .5s ease;
	-o-transition: box-shadow .5s ease;
	-ms-transition: box-shadow .5s ease;
	transition: box-shadow .5s ease;"
        class="form-control" name="relacion" id="relacion">
     
    <option selected>Seleccione</option>        
    <option>SI</option>        
    <option>NO</option>   

  </select>
</div>
</div>

                            <div class="col-sm-12 col-md-12">
                     
                                <label for="motivaciones">Cuáles fueron sus principales razones para emprender?</label>
                                <textarea  name="motivaciones" id="motivaciones"  maxlength="500"   width="100%"></textarea>
     
                    </div>

        
        <div class="col-sm-12 col-md-12" >
                        <button type="submit" class="btn btn-primary btn-block btn-large">Guardar</button>

                        <br>
                        <br>
                        <br>
                        <br>    

                    </div>

        
    </form>
    <br>
</div>
<!--nuevo formulario-->

    


    <!--se envian los datos del formulario al controlador por ajax -->
    <script>
        $(document).ready(function () {


            $('#crearform').on('submit', function (e) {

    $(this).find(':input[type=submit]').prop('disabled', true);



        });
        });




    </script>