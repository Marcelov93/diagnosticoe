<?php


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);



// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Diagnostico Empresarial', "Ficha Antecedentes - mipeUp.cl\n ");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
$pdf->SetFont('helvetica', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// Set some content to print
$html= '<h1>Ficha Antecedentes del Empresario</h1><br><br>';
$html .="<h3><strong>Información Básica</strong></h3><br><br>";
$html .= '<table border="0">';    
$html .=  '<tbody>';
    $html .="<tr>";
    $html .='<td width="20%"><strong>Nombre:</strong></td>';
    $html .="<td>".$userdata->USU_NOMBRES." ".$userdata->USU_APELLIDOS."</td>";
     $html .="</tr>";
    $html .="<tr>";
    $html .='<td><strong>RUT:</strong></td>';
    $html .="<td>".$userdata->USU_RUT."</td>";
     $html .="</tr>";
    $html .="<tr>";
    $html .='<td><strong>Correo:</strong></td>';
    $html .="<td>".$userdata->USU_CORREO."</td>";
     $html .="</tr>";
    $html .="<tr>";
    $html .='<td><strong>Empresa:</strong></td>';
    $html .="<td>".$empresa->EMP_NOMBRE_FANTASIA."</td>";
     $html .="</tr>";
$html .= '</tbody></table><br><br>';    

    

$html .="<h3><strong>Información Adicional</strong></h3><br><br><br>";

$html .= '<table border="0">';    
$html .=  '<tbody>';
    $html .='<tr>';
    $html .='<td width="95%"><strong>Nivel de estudios:</strong></td>';
    $html .='<td>'.$oan->OAN_NIVELESTUDIOS."</td>";
     $html .='</tr><br>';
    $html .='<tr>';
    $html .='<td width="95%"><strong>Número de años trabajando en el rubro:</strong></td>';
    $html .='<td>'.$oan->OAN_ANOSENELRUBRO.'</td>';
     $html .='</tr>';
    $html .='<tr><br>';
    $html .='<td width="95%"><strong>Número de años como trabajador independiente:</strong></td>';
    $html .='<td>'.$oan->OAN_ANOSCOMOINDEPENDIENTE."</td>";
     $html .='</tr><br>';
    $html .='<tr>';
    $html .='<td width="95%"><strong>Ha regresado a trabajar como dependiente durante el periodo de emprendimiento:</strong></td>';
    $html .='<td>'.$oan->OAN_REGRESADOCOMOINDEPENDIENTE."</td>";
     $html .='</tr><br>';
    $html .='<tr>';
    $html .='<td width="95%"><strong>Numero de emprendimientos a la fecha:</strong></td>';
    $html .='<td>'.$oan->OAN_NUMEROEMPRENDIMIENTOS."</td>";
     $html .='</tr><br>';
    $html .='<tr>';
    $html .='<td width="95%"><strong>Emprendimientos anteriores tienen relación con el actual:</strong></td>';
    $html .='<td>'.$oan->OAN_RELACIONDEEMPRENDIMIENTOS."</td>";
     $html .='</tr><br>';
    $html .= '</tbody></table><br><br><br>';


$html .= "<h3><strong>Principales razones para emprender:</strong></h3> <br><br> ";
$html .= "" .nl2br($oan->OAN_MOTIVACIONES)."";
$html .= '<br><br><hr>';
       
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('ficha_usuario.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
