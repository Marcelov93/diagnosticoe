<link rel="stylesheet"  href="<?=base_url()?>assets/css/estilo.css" type="text/css" media="all" />



<div class="contenedor">
   <?php
    if($this->session->flashdata('Exito'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong>La contraseña se ha actualizado correctamente!</strong></p>
        </div>
        <br>
        <?php
    }else{
        if($this->session->flashdata('Exito1'))
    {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
            <p><i class="glyphicon glyphicon-saved"></i><strong>Tus datos se han actualizado correctamente!</strong></p>
        </div>
        <br>
        <?php
    }
    }
?>
    <br>

<div class="col-md-10"></div>
    <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda <i class="glyphicon glyphicon-question-sign"></i></button>
    <h1>DIAGNÓSTICO</h1>
    <br>
    <br>
    
  
</div>
<div class="contenedor-perfil">

<!-- si $continua es nulo, es porque no hay dominios sin contestar y puede iniciar un diagnostico nuevo -->
    <?php if(isset($continua)){ 
         echo '<a class="continua" href="'.base_url().'Criterios/dominios_completados"><li  class="list-group-item list-group-item-success"><i class="glyphicon glyphicon-chevron-right">
             </i><strong> CONTINUAR</strong></li></a>
    <br>';
   
   }else{ 
   echo '<a href="'.base_url().'Criterios/dominios"><li  class="list-group-item list-group-item-success"><i class="glyphicon glyphicon-chevron-right"></i>
       <strong> INICIAR NUEVO DIAGNÓSTICO</strong></li></a>
    <br>';
   }?>


<a class="evaluacionposterior" href="<?=base_url()?>Resultados/diagnosticos_evaluacion_posterior"><li  class="list-group-item"><i class="glyphicon glyphicon-repeat"></i>
        <strong>  EVALUACION POSTERIOR</strong></li></a>
    <br>
    <a class="resultado" href="<?=base_url()?>Resultados/diagnosticos"><li  class="list-group-item"><i class="glyphicon glyphicon-stats"></i>
            <strong>  RESULTADOS DIAGNÓSTICO</strong></li></a>
    <br>
    <a class="compara" href="<?=base_url()?>Resultados/comparar"><li  class="list-group-item"><i class="glyphicon glyphicon-stats"></i>
            <strong>  RESULTADOS REEVALUACIÓN</strong></li></a>
    <br>
    
    <?php if(isset($continua) && $puede==0){
   echo '<a class="eliminar" href="'.base_url().'Criterios/eliminar_diagnosticos"><li  class="list-group-item"><i class="glyphicon glyphicon-trash"></i>
            <strong>  ELIMINAR DIAGNÓSTICO</strong></li></a>    <br>';
    }else{
        if(isset($continua) && $puede!=0){
            echo '<a class="eliminar" href="'.base_url().'Criterios/eliminar_diagnosticos"><li  class="list-group-item"><i class="glyphicon glyphicon-trash"></i>
            <strong>  ELIMINAR DIAGNÓSTICO</strong></li></a>    <br>';
        }else{
            if($puede==0){
            echo '<a data-toggle="modal" data-target="#myModal2" href="#"><li  class="list-group-item"><i class="glyphicon glyphicon-trash"></i>
            <strong>  ELIMINAR DIAGNÓSTICO</strong></li></a>    <br>';
        }else{
            if($puede!=0){
                echo '<a class="eliminar" href="'.base_url().'Criterios/eliminar_diagnosticos"><li  class="list-group-item"><i class="glyphicon glyphicon-trash"></i>
            <strong>  ELIMINAR DIAGNÓSTICO</strong></li></a>    <br>';
            }
        }
            
        }
        
    }
   
   ?>

    <a class="mejora" href="<?=base_url()?>Criterios/mejoramiento"><li  class="list-group-item"><i class="glyphicon glyphicon-ok"></i>
            <strong>  PLAN DE MEJORA</strong></li></a>
    <br>






<!-- Modal Comparar -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Atención !</h4>
      </div>
      <div class="modal-body">
        <p>No puedes comparar diagnosticos a menos que hayas realizado por lo menos 2.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal comparar -->

<!-- Modal Resultado -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Atención !</h4>
      </div>
      <div class="modal-body">
        <p>No has realizado ningún diagnostico aún.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal Resultado -->


<!-- Modal evaluacion posterior -->
<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Atención !</h4>
      </div>
      <div class="modal-body">
        <p>Para realizar una evaluación posterior debes tener por lo menos un analisis inicial.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

    <div id="ayuda" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Instrucciones:</h4>
                </div>
                <div class="modal-body">
 
                    <i class="glyphicon glyphicon-chevron-right"></i> Para comenzar, dirígase a <strong>INICIAR NUEVO DIAGNÓSTICO</strong>
                    <br>
                    <br>
                   <i class="glyphicon glyphicon-chevron-right"></i> Para reevaluar el estado de la empresa, diríjase a 
                   <strong>EVALUACIÓN POSTERIOR</strong> y seleccione el análisis inicial.(Es recomendable esperar por lo menos 6 meses antes de reevaluar)
                   <br>
                   <br>
                   <i class="glyphicon glyphicon-chevron-right"></i> Para ver los resultados de sus diagnosticos seleccione la opción <strong>RESULTADOS DIAGNÓSTICO</strong>
                   y seleccione el diagnóstico por fecha.
                   <br>
                   <br>
                   <i class="glyphicon glyphicon-chevron-right"></i> Si ya ha reevaluado un diagnóstico en <strong>EVALUACION POSTERIOR</strong>, puede volver a ver los resultados en la opción
                   <strong>RESULTADOS REEVALUACIÓN</strong>, donde podrá elegir los resultados según la fecha inicial y posterior de los diagnósticos, para ver una comparación detallada por criterios, dominios y la medición del impacto en la gestión.
                   <br>
                   <br>
                   <i class="glyphicon glyphicon-chevron-right"></i> para el eliminar un diagnóstico diríjase a la opción <strong>ELIMINAR DIAGNOSTICO</strong> y
                   seleccione por fecha el diagnostico que desea eliminar.
                   <br>
                   <br>
                   <i class="glyphicon glyphicon-chevron-right"></i> En la opción <strong>PLAN DE MEJORA</strong> puede crear un presupuesto y acciones a realizar para mejorar
                   el rendimiento de su empresa, en base a los resultados que obtuvo en el diagnóstico.
                   <br>
                   <br>
                   
               
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
                </div>
            </div>

        </div>
    </div>


<!-- Modal Continuar -->
</div>
    

<script>
$(document).ready(function() {
   
        $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});
  if(<?=$puede?><2){
  
         $('a[href*="<?=base_url()?>Resultados/comparar"]').attr('href' , '#');
        
      }
      
  if(<?=$puede?>==0){
  
         $('a[href*="<?=base_url()?>Resultados/diagnosticos"]').attr('href' , '#');
        $('a[href*="<?=base_url()?>Resultados/ver_respuestas"]').attr('href' , '#');
//        $('a[href*="<?=base_url()?>Criterios/eliminar_diagnosticos"]').attr('href' , '#');
        $('a[href*="<?=base_url()?>Criterios/diagnosticos_evaluacion_posterior"]').attr('href' , '#');
        $('a[href*="<?=base_url()?>Criterios/mejoramiento"]').attr('href' , '#');
      }

    if( <?=$puede?> < 2){
       
  $( ".compara" ).click(function(){
  
  $('#myModal1').modal('show');
});
         
        
    }  
    
    if( <?=$puede?> == 0){
       
  $( ".resultado" ).click(function(){
  
  $('#myModal2').modal('show');
});

        $( ".mejora" ).click(function(){

            $('#myModal2').modal('show');
        });

   $( ".respuestas" ).click(function(){

            $('#myModal2').modal('show');
        });
        

        $( ".evaluacionposterior" ).click(function(){

            $('#myModal3').modal('show');
        });    

    }
   


});
</script>





