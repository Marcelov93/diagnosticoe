<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->


<?php
$inicio='';
$info='';
$about='';
$services='';
$codes='';
$gallery='';
$contact='';
$login='';
$registro='';

if($img=='1'){
    $banner='banner-1';
}

if($img=='0'){
    $banner='banner';}

if($img=='2'){
$banner='sinbanner';
}    
    
if($activo=='inicio'){
    $inicio="active";  
  }

if($activo=='info'){
	$info="active";
}
  
  if($activo=='about'){
    $about="active";  
  }
  
  if($activo=='services'){
    $services="active";  
  }
  
  if($activo=='codes'){
    $codes="active";  
  }
  
  if($activo=='gallery'){
    $gallery="active";  
  }
  
   if($activo=='contact'){
    $contact="active";  
  }
  
  if($activo=='login'){
    $login="active";  
  }
  if($activo=='registro'){
    $registro="active";  
  }
?>





<head>


<title>Diagn&oacute;stico Empresarial</title>
<!-- For-Mobile-Apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //For-Mobile-Apps -->
<!-- Custom-Stylesheet-Links -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/flexslider.css" type="text/css" media="screen" />
<!-- //Custom-Stylesheet-Links -->
<!-- Web-Fonts -->
	<link href='//fonts.googleapis.com/css?family=Hammersmith+One' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
<!-- //Web-Fonts -->

<!--gallery-->
<!--JS for animate-->
<?php
if($animacion){  ?>
	<link href="<?=base_url()?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
	<script src="<?=base_url()?>assets/js/wow.min.js"></script>
		<script>
			new WOW().init();
		</script>
	<!--//end-animate-->
      <?php 
        } ?>
</head>
<body>
<!-- Banner -->

	<div class="<?=$banner?>">
		<!-- Header -->
	<div class="header">
		<div class="container">
		<!-- Navbar -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand wow fadeInLeft animated" data-wow-delay=".5s">MIPEUp</a>
				</div>

				<div class="navbar-collapse collapse hover-effect wow fadeInRight animated" data-wow-delay=".5s" id="navbar">
					<ul>
						<li><a href="<?=base_url()?>welcome/info" class="<?=$info?>" ><span data-hover="INFO">INFO</span></a></li>
						<li><a href="<?=base_url()?>" class="<?=$inicio?>" ><span data-hover="INICIO">INICIO</span></a></li>
						<li><a href="<?=base_url()?>usuarios/registro" class="<?=$registro?>" ><span data-hover="REGISTRO">REGISTRO</span></a></li>


					</ul>
				</div>

			</div>
		</nav>
		<!-- //Navbar -->
		
	</div>
            
            
	</div>
	</div>
                