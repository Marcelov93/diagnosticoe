<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>
    .formulario {

        width:60%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }
</style>

<script>
    var base_url = "<?php Print(base_url()); ?>";

</script>

<!--<style>
    html{overflow:scroll}
</style>-->

<div class="contenedor">
    <br>
    <br>
    <h2>REGISTRE SU EMPRESA</h2>
    <br>
    <br>
           <!-- Main content -->

           <form id="crearform" action="<?=base_url()?>Empresa/insertar"  method="POST" class="formulario">

                    <div class="col-sm-8 col-md-8">


                        <input type="hidden"  name="idusu" id="idusu"  value= "<?php echo $this->session->userdata('normal')->IDUSER ?>" maxlength="65"   width="100%" >




                    </div>


                    <div class="col-sm-12 col-md-12">



                                <label for="rutempresa">Rut empresa</label>

                                <input type="text"  name="rutempresa" id="rutempresa" placeholder="Ej: 12345678-9 " maxlength="45" minlength="3"  width="100%" required>
                                <b><p id= "error" style="font-size: 10px; text-align: left; font-weight: bold; color: #CC0000;"> </p> </b>

                        </div>





                    <div class="col-sm-12 col-md-12">

                                <label for="razonsocial">Raz&oacute;n social</label>
                                <input type="text"  name="razonsocial" id="razonsocial"  maxlength="50" minlength="3"  width="100%" required>



                    </div>

                    <div class="col-sm-12 col-md-12">

                                <label for="nombrefantasia">Nombre de fantasía</label>
                                <input type="text"  name="nombrefantasia" id="nombrefantasia"  maxlength="50" minlength="3"  width="100%" required>




                    </div>

                    <div class="col-sm-12 col-md-12">

                                <label for="rubro">Rubro</label>
                                <input type="text"  name="rubro" id="rubro"  maxlength="50" minlength="3"  width="100%" required>




                    </div>

                    <div class="col-sm-12 col-md-12">

                                <label for="telefono">Teléfono</label>
                                <input type="number"  name="telefono" id="telefono" maxlength="15"  width="100%" required>




                    </div>

                    <div class="col-sm-12 col-md-12">

                                <label for="correo">Correo electr&oacute;nico</label>
                                <input type="email"  name="correo" id="correo"  maxlength="30" minlength="3"  width="100%" required>


                                       </div>

                  

                    <div class="col-sm-12 col-md-12">

                                <label for="paginaweb">P&aacute;gina web</label>
                                <input type="text"  name="paginaweb" id="paginaweb"  maxlength="30" minlength="3"  width="100%" >



                    </div>

                    <div class="col-sm-12 col-md-12">

                                <label for="antiguedad">Años Antigüedad</label>
                                <input type="number"  name="antiguedad" id="antiguedad" placerholder="N° de años" max="99" min="0"  width="100%" required>




                    </div>

                    <div class="col-sm-12 col-md-12">

                                <label for="vtasmensualesprom">Ventas mensuales promedio</label>
                                <input type="number"  name="vtasmensualesprom" id="vtasmensualesprom"    width="100%" required>




                    </div>

                    <div class="col-sm-12 col-md-12">

                                <label for="ctosmensualesprom">Costos mensuales promedio</label>
                                <input type="number"  name="ctosmensualesprom" id="ctosmensualesprom"    width="100%" required>




                    </div>

                    <div class="col-sm-12 col-md-12">

                                <label for="utilidadmensualprom">Utilidad mensual promedio</label>
                                <input type="number"  name="utilidadmensualprom" id="utilidadmensualprom"    width="100%" required>



                    </div>


                    <div class="col-sm-12 col-md-12">

                                <label for="nroempleados">Número de empleados</label>
                                <input type="number"  name="nroempleados" id="nroempleados"  max="50" min="0"  width="100%" required>




                    </div>



                    <div class="col-sm-12 col-md-12" >
                        <button type="submit" name="btnenviar" id="btnenviar" class="btn btn-primary btn-block btn-large " style="width: 100% " >Registrar</button>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </form>

</div>

<!-- /.content-wrapper -->




<!--verifica si el rut es correcto-->
<script>

    $('#rutempresa').Rut({

        on_error: function () {
            document.getElementById('rutempresa').style.borderColor = "red";
            document.getElementById("error").textContent = "RUT incorrecto. Pruebe sin puntos ni guión";
            $("#btnenviar").prop("disabled", true);

        },

        on_success: function () {
            document.getElementById('rutempresa').style.borderColor = "transparent";
            document.getElementById("error").textContent = "";
            $("#btnenviar").prop("disabled", false);

            var rut = $('#rutempresa').val();
            checkrut(rut);

        }
    });

    //verifica si el rut ya existe
    function checkrut(rut) {

        var base_url = "<?php Print(base_url()); ?>";

        if (rut.length > 0)
        {
            jQuery.ajax({
                type: "POST",
                url: base_url + "Empresa/checkrut",
                data: 'rutempresa=' + rut,
                cache: false,
                success: function (response) {
                    if (response == 1) {

                        document.getElementById('rutempresa').style.borderColor = "red";
                        document.getElementById("error").textContent = "El rut ingresado ya existe";
                        //document.getElementById("error2").textContent = "-";
                        $("#btnenviar").prop("disabled", true);
                    } else {

                        document.getElementById('rutempresa').style.borderColor = "transparent";
                        document.getElementById("error").textContent = "";
                        //document.getElementById("error2").textContent = "";
                        $("#btnenviar").prop("disabled", false);

                    }

                }
            });

        }




    }
</script>

<!--se envian los datos del formulario al controlador por ajax -->
<script>
    $(document).ready(function () {


        $('#crearform').on('submit', function (e) {


    $(this).find(':input[type=submit]').prop('disabled', true);


    });




</script>