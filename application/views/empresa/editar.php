<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">


<script>
    var base_url = "<?php Print(base_url()); ?>";
</script>

<style>
    input[readonly] {
        background-color: white !important;
        cursor: text !important;
    }
</style>



    <div class="contenedor">
        <br>
        <br>
        <h2>Modificar Información Empresa</h2>
        <br>
        <br>




        <form id="crearform" action="<?=base_url()?>Empresa/update" method="POST" class="formulario">


                            <?php foreach ($empresa as $value){?>

                                <div class="control-group">

                                    <input type="hidden" class="form-control" name="idusu" id="idusu"  value= "<?php echo $this->session->userdata('normal')->IDUSER ?>" maxlength="65"   width="100%" >



                                </div>

                            <div class="col-sm-12 col-md-12">


                                        <input type="hidden"  name="rutempresa" id="rutempresa" value="<?php echo $value->EMP_RUT;  ?>" readonly="readonly"  width="100%" required>
                                        <b><p id= "error" style="font-size: 10px; text-align: left; font-weight: bold; color: #CC0000;"> </p> </b>


                            </div>



                            <div class="col-sm-12 col-md-12">

                                        <label for="razonsocial">Raz&oacute;n social</label>
                                        <input type="text"  name="razonsocial" id="razonsocial" value="<?php echo $value->EMP_RAZON_SOCIAL;  ?>" maxlength="50" minlength="3"  width="100%" required>




                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="nombrefantasia">Nombre de fantasia</label>
                                        <input type="text"  name="nombrefantasia" id="nombrefantasia"  value="<?php echo $value->EMP_NOMBRE_FANTASIA;  ?>"maxlength="50" minlength="3"  width="100%" required>




                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="rubro">Rubro</label>
                                        <input type="text"  name="rubro" id="rubro"  value="<?php echo $value->EMP_RUBRO;  ?>" maxlength="50" minlength="3"  width="100%" required>




                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="telefono">Teléfono</label>
                                        <input type="text"  name="telefono" id="telefono"  value="<?php echo $value->EMP_TELEFONO;  ?>" placeholder="+56912345678" maxlength="13" width="100%" required>




                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="correo">Correo electr&oacute;nico</label>
                                        <input type="email"  name="correo" id="correo"  value="<?php echo $value->EMP_CORREO;  ?>" maxlength="30" minlength="3"  width="100%" required>



                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="paginaweb">P&aacute;gina web</label>
                                        <input type="text"  name="paginaweb" id="paginaweb"  value="<?php echo $value->EMP_PAGINA_WEB;  ?>" maxlength="30" minlength="3"  width="100%" >




                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="antiguedad">Antiguedad</label>
                                        <input type="number"  name="antiguedad" id="antiguedad"  value="<?php echo $value->EMP_ANTIGUEDAD;  ?>" max="99" min="0"  width="100%" >



                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="vtasmensualesprom">Ventas mensuales promedio</label>
                                        <input type="number"  name="vtasmensualesprom" id="vtasmensualesprom"  value="<?php echo $value->EMP_VTASMENSUALESPROM;  ?>"   width="100%" required>




                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="ctosmensualesprom">Costos mensuales promedio</label>
                                        <input type="number"  name="ctosmensualesprom" id="ctosmensualesprom"  value="<?php echo $value->EMP_CTOSMENSUALESPROM;  ?>"   width="100%" required>




                            </div>

                            <div class="col-sm-12 col-md-12">

                                        <label for="utilidadmensualprom">Utilidad mensual promedio</label>
                                        <input type="number"  name="utilidadmensualprom" id="utilidadmensualprom"  value="<?php echo $value->EMP_UTILIDADMENSUALPROM;  ?>"   width="100%" required>




                            </div>


                            <div class="col-sm-12 col-md-12">

                                        <label for="nroempleados">Numero de empleados</label>
                                        <input type="number"  name="nroempleados" id="nroempleados"  value="<?php echo $value->EMP_NROEMPLEADOS;  ?>" max="50" min="0" width="100%" required>



                                <br>
                                <br>
                            </div>

                            <?php } ?>


                            <div class="col-sm-12 col-md-12" >
                                <button type="submit" name="btnenviar" id="btnenviar" class="btn btn-primary btn-block btn-large " style="width: 100% " >Actualizar empresa</button>
                                <br>
                                <br>



                            </div>
            <br>
            <br>
            <div class="col-md-10"></div>
            <div class="col-md-2 col-sm-2">
                <a id="back" class="btn btn-success col-md-12 col-sm-12">VOLVER</a>
            </div>
            <br>
            <br>

                </form>

                <br><br>


</div><!-- /.content-wrapper -->
<script>

    $(document).ready(function() {

        $('#back').click(function(){
            parent.history.back();
            return false;
        });
    });

</script>



