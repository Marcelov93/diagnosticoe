<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>

    .contenedor-mostrar-empresa{
        width:70%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }


</style>
<script>
    function changeclass(id) {

        var activo = document.getElementById(id)

        if(activo.className=='list-group-item active'){
            activo.className="list-group-item "

        }else
            activo.className="list-group-item active"


    }
</script>

<div class="contenedor">

    <br>
    <br>
    <h2>Personas</h2>
    <br>
    <br>
    <?php

    if($budget){
        echo '<h3>Presupuesto Requerido: $'.$budget.' </h3>';
        echo '<br>';
        echo '<br>';
    }?>

    <div class="contenedor-mostrar-empresa">
        <form id="dom3">
            <?php $cont=0;?>
        <?php foreach ($lista as $valor){?>

            <?php foreach ($respuestas as $value){
                $i=1;
                $tres=0;
                if(($value->CRIT_NUMERO > 33 && $value->CRIT_NUMERO < 43 && $value->PUNTAJEXCRITERIO==2) || $value->CRIT_NUMERO == 49 || $value->CRIT_NUMERO == 51 && $value->PUNTAJEXCRITERIO==2){
                    $tres=1;
                }
                
                if($value->CRIT_NUMERO==$valor->CRIT_NUMERO && $value->PUNTAJEXCRITERIO!=3 && $tres!=1) {?>
                    <div class="list-group col-md-12">
                        <a id="resp"  class="list-group-item active">
                            <p class="list-group-item-text">  <?php echo $value->CRIT_NUMERO;  ?>.- <?php echo $value->CRIT_PREGUNTA;  ?>  </p>

                        </a>
                    </div>



                    <!-- respuestas -->
                    <?php foreach ($respuestas2 as $resp){
                        ?>
                        <?php if($value->CRIT_NUMERO==$resp->CRIT_NUMERO){
                            if($i==1){$titulo='NO DESARROLLADO';}else{
                                if($i ==2){$titulo='ESCASAMENTE DESARROLLADO ';} else {
                                    if($i==3){$titulo='PARCIALMENTE DESARROLLADO';}else{
                                        if($i==4){$titulo='AMPLIAMENTE DESARROLLADO';}
                                    }
                                }
                            }
                            ?>
                            <?php if(($value->CRIT_NUMERO > 33 && $value->CRIT_NUMERO < 43)|| $value->CRIT_NUMERO == 49 || $value->CRIT_NUMERO == 51){ ?>
                                <div  class="list-group col-md-3" >
                                    <?php if($value->ID_RESPUESTA==$resp->ID_RESPUESTA){ ?>
                                        <a class="list-group-item active">
                                            <p class="list-group-item-text">
                                                <?php echo $resp->RESP_DESCRIPCION; ?>
                                            </p>
                                        </a>
                                    <?php }
                                    else{?>
                                        <a class="list-group-item">
                                            <p class="list-group-item-text">
                                                <?php echo $resp->RESP_DESCRIPCION; ?>
                                            </p>
                                        </a>
                                    <?php } ?>
                                </div>
                            <?php }else{ ?>
                                <div  class="list-group col-md-3" >
                                    <?php if($value->ID_RESPUESTA==$resp->ID_RESPUESTA){ ?>
                                        <a class="list-group-item active">
                                            <h4 class="list-group-item-heading"><?=$titulo?></h4>
                                            <br>
                                            <p class="list-group-item-text">
                                                <?php echo $resp->RESP_DESCRIPCION; ?>
                                            </p>
                                        </a>
                                    <?php }
                                    else{?>
                                        <a class="list-group-item">
                                            <h4 class="list-group-item-heading"><?=$titulo?></h4>
                                            <br>
                                            <p class="list-group-item-text">
                                                <?php echo $resp->RESP_DESCRIPCION; ?>
                                            </p>
                                        </a>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php $i=$i+1;} ?>
                    <?php }  ?>

                    <div class=" col-md-12">



                        <label>Accion a realizar:</label>

                        <textarea class="form-control" rows="4" name="accion" id="accion<?=$cont;?>" ><?=$value->ACCION;?></textarea>
                        <br>
                    </div>
                    <div class="col-md-12">

                        <label>Presupuesto estimado:</label>

                        <input type="number" name="presupuesto" id="presupuesto<?=$cont;?>" value="<?=$value->PRESUPUESTO;?>" maxlength="11" >
                        <br>

                    </div>
                    <div class=" col-md-12">

                        <label>Plazo estimado:</label>

                        <input type="text"  name="plazo" id="plazo<?=$cont;?>" value="<?=$value->PLAZO;?>" maxlength="100"  >
                        <br>

                    </div>
                    <div class=" col-md-12">

                        <label>Area o persona responsable:</label>

                        <input type="text"  name="responsable" id="responsable<?=$cont;?>" value="<?=$value->RESPONSABLE;?>" maxlength="100"  >
                        <br>
                        <br>

                    </div>
                    <?php $terminado=$this->Criterios_model->existe_plan($idana,$value->CRIT_NUMERO);
                    if($terminado==1){    ?>
                        <div class=" col-md-3">
                            <a><li id="<?=$cont;?>" class="list-group-item active" onclick="changeclass(<?=$cont?>);">TAREA TERMINADA</li></a>

                            <br>
                            <br>
                        </div>
                    <?php }else if($terminado==2){?>
                        <div class=" col-md-3">
                            <a><li id="<?=$cont;?>" class="list-group-item" onclick="changeclass(<?=$cont?>);">TAREA TERMINADA</li></a>

                            <br>
                            <br>
                        </div>
                    <?php }else{ ?>
                        <div class=" hidden col-md-3">
                            <a><li id="<?=$cont;?>" class="list-group-item" onclick="changeclass(<?=$cont?>);">TAREA TERMINADA</li></a>

                            <br>
                            <br>
                        </div>
                    <?php  } ?>
                <?php  $crit_numero[$cont]=$value->CRIT_NUMERO;
                    $cont++;
                } ?>
            <?php }  ?>
        <?php } ?>

            <div class="row">
                <div class="col-md-10">
                    <button type="submit" name="btnenviar" id="btnenviar" class="btn btn-warning col-md-12 col-xs-12"  >Guardar</button>
                </div>
                <div class="col-md-2">
                    <a id="back" class="btn btn-success col-md-12 col-xs-12">VOLVER</a>
                </div>
            </div>
</form>
    </div>

    <!-- Main content -->


    <br>
    <br>
    <!-- /.content -->

</div><!-- /.content-wrapper -->
<script>
    $(document).ready(function(){
        $('#back').click(function(){
            parent.history.back();
            return false;
        });

        var crit_numero=<?php echo json_encode($crit_numero);?>;
        //console.log(crit_numero);
        var submitted = false;
        $('#dom3').on('submit', function (e) {
            submitted = true;

            var id=<?=$idana?>;

            var accion = [];
            var presupuesto = [];
            var plazo = [];
            var responsable = [];
            var terminado = [];

            for(var i=0;i<<?=$cont?>;i++){

                accion[i]=$('#accion'+i+'').val();
                if(i<<?=$cont-1?>)
                    accion[i]=accion[i]+'-';
                presupuesto.push($('#presupuesto'+i+'').val());
                plazo.push($('#plazo'+i+'').val());
                responsable[i]=$('#responsable'+i+'').val();
                if(i<<?=$cont-1?>)
                    responsable[i]=responsable[i]+'-';

                window["terminado" + i]=document.getElementById(i);
                if(window["terminado" + i].className=='list-group-item active'){
                    terminado.push(1);
                }else
                    terminado.push(2);

            }
            var crit_numero=<?php echo json_encode($crit_numero);?>;

            /*------------------------------------------------------------*/

            var form_data = new FormData();

            form_data.append('accion',accion );
            form_data.append('presupuesto',presupuesto);
            form_data.append('plazo', plazo);
            form_data.append('responsable',responsable );
            form_data.append('idana',id );
            form_data.append('crit_numero',crit_numero );
            form_data.append('dominio',3);
            form_data.append('terminado',terminado );



            var url = "<?php echo base_url(); ?>Criterios/insert_mejoramiento";
            $.ajax({

                type: "POST",
                url: url,
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (php) {
                    localStorage.setItem('exito', '1');
                    window.location.href = "<?= base_url('Criterios/seleccionar_dominio/')?>"+id;


                }
            });//ajax

            return false;

        });//onsubmit

    });
</script>