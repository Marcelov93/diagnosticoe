<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">


<style>

    .contenedor-perfil{
        width:50%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }

</style>

<?php
if($this->session->flashdata('registrostatus'))
{
    ?>
    <div class="alert alert-success alert-dismissible" role="alert" id="alerta2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p><i class="glyphicon glyphicon-saved"></i><strong>Diagnóstico eliminado correctamente!</strong></p>
    </div>
    <br>
    <?php
}
?>

<div class="contenedor">
    <br>
    <br>
    <br>
    <br>
    <h1>SELECCIONE EL DIAGNÓSTICO QUE DESEA ELIMINAR</h1>
    <br>
    <br>
</div>
<div class="contenedor-perfil">

    <?php  $i=1 ?>
    <?php foreach ($diagnosticos as $value){

     if($continua!=$value->ANA_ID ){ ?>
    <a class="diag" value='<?=$value->ANA_ID?>' ><li  class="opcion list-group-item"><i class="glyphicon glyphicon-chevron-right"></i><strong> <?php echo $i.'.-'.date("d-m-Y", strtotime($value->ANA_FECHA))?></strong> </li></a>
        <br>

     <?php }else{/*si hay un diagnostico sin completar se le agrega una etiqueta */ ?>
        <a class="diag" value='<?=$value->ANA_ID?>' ><li  class="opcion list-group-item"><i class="glyphicon glyphicon-chevron-right"></i><strong> <?php echo $i.'.-'.date("d-m-Y", strtotime($value->ANA_FECHA))?></strong><span class="badge badge-danger">Sin Terminar</span> </li></a>
        <br>

    <?php  }?>

        <div id="<?=$value->ANA_ID?>" class="modal fade" role="dialog">
            <div class="modal-dialog">

                Modal content
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Atenci&oacute;n !</h4>
                    </div>
                    <div class="modal-body">
                        <p>En el an&aacute;lisis seleccionado se evaluaron los siguientes dominios: <!--<?=$value->ANA_ID?>--></p>
                        <br>
                        <?php
                        $dominios=$this->Resultados_model->dominios_seleccionados($value->ANA_ID);
                        foreach($dominios as $dom){
                        if($continua!=$value->ANA_ID ) {
                            echo
                                '<a><li class="list-group-item">' . $dom->DOM_NOMBRE . '<span class="badge">Ponderaci&oacute;n: ' . $dom->PONDERACION . '%</span></li></a><br>';
                        }else if($dom->PUNTAJEXDOMINIO == NULL)
                            echo '<a><li class="list-group-item">' . $dom->DOM_NOMBRE . '<span class="badge badge-danger">Sin Terminar</span> </li></a><br>';
                            else echo '<a><li class="list-group-item">' . $dom->DOM_NOMBRE . '<span class="badge badge-success">Terminado</span> </li></a><br>';
                        }?>


                        <br>
                        <p>Est&aacute; seguro que desea eliminar el diagn&oacute;stico realizado?</p>
                    </div>
                    <div class="modal-footer">
                        <button onclick="javascript:window.location='<?php echo base_url('Criterios/eliminar')."/".$value->ANA_ID; ?>'" type="button" class="btn btn-danger" data-dismiss="modal">ELIMINAR</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>

            </div>
        </div>

        <?php  $i++; }?>

    <br>
    <br>

    <div class="col-md-10 col-sm-10"></div>

    <div class="col-md-2 col-sm-2">
        <a href="<?=base_url()?>Usuarios/menu_diagnostico" class="btn btn-success col-md-12 col-sm-12">VOLVER</a>
    </div>
    <br>
    <br>

</div>

<script>

    $(document).ready(function() {

        $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-dismissible").alert('close');
});

        $( ".diag" ).click(function(){
            var id = $(this).attr("value");
            $('#'+id+'').modal('show');
        });

    });




</script>

