<?php


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);



// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Diagnostico Empresarial', "Plan de mejora - mipeUp.cl\n ");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
$pdf->SetFont('helvetica', '', 11, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// Set some content to print
$html= '<h1>Plan de Mejora</h1><br>';
$fechaNormal = date("d-m-Y", strtotime($fecha));
$html .= '<strong>Fecha análisis: ' .$fechaNormal.'</strong><br><br>';

$html .="<strong>EMPRESARIO: </strong>".$empresario->USU_NOMBRES.' '.$empresario->USU_APELLIDOS.'<br>';
$html .="<strong>EMPRESA: </strong>".$empresa->EMP_NOMBRE_FANTASIA."<br><br>";

$html .= '<table>';    
$html .= '<thead>
    <tr >
      <th><h4>Area o Dominio</h4> </th>
      <th><h4>Porcentaje Cumplimiento</h4></th>

    </tr>
  </thead>
  <tbody>';
$i=1;
foreach($dominios as $dom){


    $html .="<tr>";
    $html .="<td>".$i.".-".$dom->DOM_NOMBRE."</td>";

    $puntajeOmitido=$this->Resultados_model->puntaje_omitido($dom->ANA_ID, $dom->DOM_ID);
    if(isset($puntajeOmitido)){
        $puntajeMaximo=$dom->DOM_PONDERACION-$puntajeOmitido;
        $score=round((($dom->PUNTAJEXDOMINIO*100)/$puntajeMaximo),2);

    $html .="<td>".$score."%</td>";
        $html .="</tr>";
    }else{
        $score=round((($dom->PUNTAJEXDOMINIO*100)/$dom->DOM_PONDERACION),2);
        $html .="<td>".$score."%</td>";
     $html .="</tr>";
    }

       $i++;
     
}

$html .= '</tbody></table>';    
$html .= '<br><br><hr>';


foreach ($dominios as $dom){
    
    if($dom->DOM_ID== 1){
         
       $accion1= $this->Criterios_model->acciones_informe($idana, 1,10);
        if($accion1){
            $html .="<h3>".$dom->DOM_NOMBRE."</h3>";

                foreach ($accion1 as $a1){
                        if(strlen($a1->ACCION)>0){
                    $html.="<strong>Criterio: </strong>$a1->CRIT_PREGUNTA<br><br>";
                    $html.="<strong>Acción:</strong> ".$a1->ACCION."<br>";
                    $html.="<strong>Presupuesto:</strong> $".$a1->PRESUPUESTO."<br>";
                    $html.="<strong>Plazo:</strong> ".$a1->PLAZO."<br>";
                    $html.="<strong>Responsable:</strong> ".$a1->RESPONSABLE."<br>";
                    if($a1->TERMINADO==2)$html.="<strong>Estado:</strong>  En proceso<br><br><br>";
                    else $html.="<strong>Estado:</strong> Terminado<br><br><br>";
                    }
                }
        }
}else{
        if($dom->DOM_ID== 2){
         
       $accion2= $this->Criterios_model->acciones_informe($idana, 11,33);
        if($accion2){
            $html .="<h3>".$dom->DOM_NOMBRE."</h3>";
                foreach ($accion2 as $a2){
                    if(strlen($a2->ACCION)>0){
                    $html.="<strong>Criterio: </strong>$a2->CRIT_PREGUNTA<br><br>";
                    $html.="<strong>Acción:</strong> ".$a2->ACCION."<br>";
                    $html.="<strong>Presupuesto:</strong> $".$a2->PRESUPUESTO."<br>";
                    $html.="<strong>Plazo:</strong> ".$a2->PLAZO."<br>";
                    $html.="<strong>Responsable:</strong> ".$a2->RESPONSABLE."<br>";
                    if($a2->TERMINADO==2)$html.="<strong>Estado:</strong>  En proceso<br><br><br>";
                    else $html.="<strong>Estado:</strong> Terminado<br><br><br>";
                    }
                }
        }
}else{
        if($dom->DOM_ID== 3){
         
       $accion3= $this->Criterios_model->acciones_informe($idana, 34,53);
        if($accion3){
            $html .="<h3>".$dom->DOM_NOMBRE."</h3>";
                foreach ($accion3 as $a3){
                    if(strlen($a3->ACCION)>0){
                    $html.="<strong>Criterio: </strong>$a3->CRIT_PREGUNTA<br><br>";
                    $html.="<strong>Acción:</strong> ".$a3->ACCION."<br>";
                    $html.="<strong>Presupuesto:</strong> $".$a3->PRESUPUESTO."<br>";
                    $html.="<strong>Plazo:</strong> ".$a3->PLAZO."<br>";
                    $html.="<strong>Responsable:</strong> ".$a3->RESPONSABLE."<br>";
                    if($a3->TERMINADO==2)$html.="<strong>Estado:</strong>  En proceso<br><br><br>";
                    else $html.="<strong>Estado:</strong> Terminado<br><br><br>";
                    }
                }
        }
}else{
        if($dom->DOM_ID== 4){
         
       $accion4= $this->Criterios_model->acciones_informe($idana, 54,79);
        if($accion4){
            $html .="<h3>".$dom->DOM_NOMBRE."</h3>";
                foreach ($accion4 as $a4){
                    if(strlen($a4->ACCION)>0){
                    $html.="<strong>Criterio: </strong>$a4->CRIT_PREGUNTA<br><br>";
                    $html.="<strong>Acción:</strong> ".$a4->ACCION."<br>";
                    $html.="<strong>Presupuesto:</strong> $".$a4->PRESUPUESTO."<br>";
                    $html.="<strong>Plazo:</strong> ".$a4->PLAZO."<br>";
                    $html.="<strong>Responsable:</strong> ".$a4->RESPONSABLE."<br>";
                    if($a4->TERMINADO==2)$html.="<strong>Estado:</strong>  En proceso<br><br><br>";
                    else $html.="<strong>Estado:</strong> Terminado<br><br><br>";
                    }
                }
        }
}else{
        if($dom->DOM_ID== 5){
         
       $accion5= $this->Criterios_model->acciones_informe($idana, 80,86);
        if($accion5){
            $html .="<h3>".$dom->DOM_NOMBRE."</h3>";
                foreach ($accion5 as $a5){
                    if(strlen($a5->ACCION)>0){
                    $html.="<strong>Criterio: </strong>$a5->CRIT_PREGUNTA<br><br>";
                    $html.="<strong>Acción:</strong> ".$a5->ACCION."<br>";
                    $html.="<strong>Presupuesto:</strong> $".$a5->PRESUPUESTO."<br>";
                    $html.="<strong>Plazo:</strong> ".$a5->PLAZO."<br>";
                    $html.="<strong>Responsable:</strong> ".$a5->RESPONSABLE."<br>";
                    if($a5->TERMINADO==2)$html.="<strong>Estado:</strong>  En proceso<br><br><br>";
                    else $html.="<strong>Estado:</strong> Terminado<br><br><br>";
                    }
                }
        }
}else{
        if($dom->DOM_ID== 6){
         
       $accion6= $this->Criterios_model->acciones_informe($idana, 87,92);
        if($accion6){
            $html .="<h3>".$dom->DOM_NOMBRE."</h3>";
                foreach ($accion6 as $a6){
                    if(strlen($a6->ACCION)>0){
                    $html.="<strong>Criterio: </strong>$a6->CRIT_PREGUNTA<br><br>";
                    $html.="<strong>Acción:</strong> ".$a6->ACCION."<br>";
                    $html.="<strong>Presupuesto:</strong> $".$a6->PRESUPUESTO."<br>";
                    $html.="<strong>Plazo:</strong> ".$a6->PLAZO."<br>";
                    $html.="<strong>Responsable:</strong> ".$a6->RESPONSABLE."<br>";
                    if($a6->TERMINADO==2)$html.="<strong>Estado:</strong>  En proceso<br><br><br>";
                    else $html.="<strong>Estado:</strong> Terminado<br><br><br>";
                    }
                }
        }
}
}
}
}
}
}
}
       
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('plan_de_mejora.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
