<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style type="text/css">
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
          
          a:hover{
              cursor: pointer; cursor: hand;
          }
          .clear {clear:both;}
        </style>
        
        
<script> 
    $(function(){
           if(  localStorage.getItem('exito')){
                var exitoso = '';
                   if(  localStorage.getItem('exito')==='1'){      
               
     exitoso = '   <div class="alert alert-success" id="success-alert2" style="margin-top: 15px;">'+
   ' <button type="button" class="close" data-dismiss="alert">x</button>'+
    '<i class="glyphicon glyphicon-saved"></i><strong> Tus respuestas han sido guardadas correctamente!</strong>'+
                             '</div>';
                     
                     } else {
                           exitoso = '   <div class="alert alert-danger" id="success-alert2" style="margin-top: 15px;">'+
   ' <button type="button" class="close" data-dismiss="alert">x</button>'+
    '<strong>Lo sentimos, hubo un error al guardar tus respuestas, por favor intentalo denuevo</strong>'+
                             '</div>';   
                         }
  
    $('#exitoso').html(exitoso);
     localStorage.removeItem('exito'); 
    
      $("#success-alert2").alert();
                $("#success-alert2").fadeTo(3000, 500).slideUp(500, function(){
               $("#success-alert2").alert('close');
                });   
   
 
}
 
    });
    </script>              

<div class="contenedor">
    <ol class="breadcrumb">

        <?php $total=0;
        $actual=3;?>
        <?php foreach ($dominios as $dom){

            ?>
            <li><?=$dom->DOM_NOMBRE?></li>

            <?php
            if($dominios[$total]->DOM_ID==$actual){
                $aux=$total;
            }

            $total=$total+1;}

        if($dominios[$total-1]->DOM_ID==$actual){
            $siguiente='Criterios/dominios_completados';
        }else{
            $next=$dominios[$aux+1]->DOM_ID;
            $str="$next";
            $siguiente='Criterios/dom'.$str;

        }
        ?>

    </ol>
    <div class="clear" id="exitoso" ></div>
       <br>
    <br>
    <h2>3.- Personas</h2>
    <br>
    <br>



    <!-- Main content -->


    <div class="contenedor-mostrar-empresa">

                   <form id="dom3" class="noselect">
            <?php 
            $subnombre= 0;
            foreach ($lista as $value){ 
                $i=1;
                
            if($value->CRIT_NUMERO>33 && $value->CRIT_NUMERO<43 && $subnombre==0){
                echo '<h2 style="text-align:left">3.1 '.$value->SUB_NOMBRE.'</h2><br><br>';
                $subnombre=1;
            }    
            
            if($value->CRIT_NUMERO>42 && $value->CRIT_NUMERO<52 && $subnombre==1){
                echo '<h2 style="text-align:left">3.2 '.$value->SUB_NOMBRE.'</h2><br><br>';
                $subnombre=2;
            }    
            if($value->CRIT_NUMERO>51 && $value->CRIT_NUMERO<54 && $subnombre==2){
                echo '<h2 style="text-align:left">3.3 '.$value->SUB_NOMBRE.'</h2><br><br>';
                $subnombre=3;
            }    
           
            

            
                ?>
       <!-- preguntas -->  
       
       <div class="pregunta">
        <div class="list-group col-md-12">
 <a id="resp"  class="list-group-item active">
     <p class="list-group-item-text">  <?php echo $value->CRIT_NUMERO;  ?>.- <?php echo $value->CRIT_PREGUNTA;  ?>  </p>

</a>
                </div>    

   
      <?php foreach ($respuestas as $resp){
       ?>
   
   <!-- ----------------------------------------------  -->
   <?php if($value->CRIT_NUMERO==$resp->CRIT_NUMERO){ ?>
   <?php
       
       if($i==1){$titulo='NO DESARROLLADO';}else{
           if($i ==2){$titulo='ESCASAMENTE DESARROLLADO ';} else {
               if($i==3){$titulo='PARCIALMENTE DESARROLLADO';}else{
                   if($i==4){$titulo='AMPLIAMENTE DESARROLLADO';}
               }
}
       }
       ?>

  <!-- preguntas con 3 respuestas -->
   <?php if(($value->CRIT_NUMERO > 33 && $value->CRIT_NUMERO < 43)|| $value->CRIT_NUMERO == 49 || $value->CRIT_NUMERO == 51){ ?>
   
      <div  class="list-group col-md-3" value="<?=$resp->CRIT_NUMERO?>">
  <a id="<?=$resp->ID_RESPUESTA;?>"  class="list-group-item" value="<?=$resp->RESP_PONDERACION?>">
      <h4 class="list-group-item-heading"><?=$resp->RESP_DESCRIPCION?></h4>
                     
  </a>
</div>
   
   <?php }else{ ?> <!--preguntas de 4 respuestas -->
   
   
   <!-- respuestas -->
   <div  class="list-group col-md-3" value="<?=$resp->CRIT_NUMERO?>">
  <a id="<?=$resp->ID_RESPUESTA;?>"  class="list-group-item" value="<?=$resp->RESP_PONDERACION?>">
      <h4 class="list-group-item-heading"><?=$titulo?></h4>
      <br>
    <p class="list-group-item-text">
                    <?php echo $resp->RESP_DESCRIPCION; ?>
</p>

                    
  </a>
</div>
   <?php } ?> <!--fin else -->
<!-- fin respuestas -->

<?php $i=$i+1;}  ?>
   <!-- ----------------------------------------------  -->

                    
            
                <?php } ?>
       </div><div class="clear"></div>

                <?php } ?>

            
        
        <div class="col-sm-12 col-md-12">
            <button type="submit" name="btnenviar" id="btnenviar" class="btn botonesgestion " style="width: 100% " >Continuar</button>
            <br>
            <br>
        </div>

 </form>

    <!-- /.content -->

</div><!-- /.content-wrapper -->
</div><!-- /.content-wrapper -->




<div id="modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Atención !</h4>
      </div>
      <div class="modal-body">
          <p><strong>Debes responder por lo menos una pregunta !</strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<div id="contestado" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Dominio: Personas</h4>
      </div>
      <div class="modal-body">
          <p><strong>Tus respuestas han sido guardadas correctamente!</strong> </p>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="javascript:window.location='<?=base_url()?><?=$siguiente?>'" class="btn btn-info">Continuar</button>
      </div>
    </div>

  </div>
</div>


<script>
 
   $('.list-group[value=34]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=34]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=34]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=35]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=35]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=35]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=36]').on('click','> a', function(e) {
   var $this = $(this);
  
  if($('.list-group[value=36]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=36]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=37]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=37]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=37]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=38]').on('click','> a', function(e) {
   var $this = $(this);
  
  if($('.list-group[value=38]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=38]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=39]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=39]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=39]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=40]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=40]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=40]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=41]').on('click','> a', function(e) {
   var $this = $(this);
   
      if($('.list-group[value=41]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=41]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=42]').on('click','> a', function(e) {
   var $this = $(this);
       if($('.list-group[value=42]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=42]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=43]').on('click','> a', function(e) {
   var $this = $(this);
   
      if($('.list-group[value=43]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=43]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=44]').on('click','> a', function(e) {
   var $this = $(this);
      if($('.list-group[value=44]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=44]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=45]').on('click','> a', function(e) {
   var $this = $(this);
     if($('.list-group[value=45]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=45]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=46]').on('click','> a', function(e) {
   var $this = $(this);
       if($('.list-group[value=46]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=46]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=47]').on('click','> a', function(e) {
   var $this = $(this);
      if($('.list-group[value=47]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=47]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=48]').on('click','> a', function(e) {
   var $this = $(this);
      if($('.list-group[value=48]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=48]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=49]').on('click','> a', function(e) {
   var $this = $(this);
      if($('.list-group[value=49]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=49]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=50]').on('click','> a', function(e) {
   var $this = $(this);
      if($('.list-group[value=50]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=50]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=51]').on('click','> a', function(e) {
   var $this = $(this);
  if($('.list-group[value=51]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=51]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=52]').on('click','> a', function(e) {
   var $this = $(this);
   if($('.list-group[value=52]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=52]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=53]').on('click','> a', function(e) {
   var $this = $(this);
    if($('.list-group[value=53]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=53]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});


     
        $(document).ready(function () {

var submitted = false;
            $('#dom3').on('submit', function (e) {
                 submitted = true;
                var idresp = [];
                var numcrit = [];
                var puntaje = [];
                var puntajetotal=0;
                var flag=0;


/* preguntas 34-53*/
for(var i=34;i<54;i++){
    
    if($('.list-group[value='+i+']').find('.active').attr('id')){
    idresp.push($('.list-group[value='+i+']').find('.active').attr('id'));
    puntaje.push($('.list-group[value='+i+']').find('.active').attr('value'));
    numcrit.push(i);
    flag=1;
    puntajetotal= puntajetotal+parseInt(($('.list-group[value='+i+']').find('.active').attr('value')));
    }
}
                
                /*------------------------------------------------------------*/
                
                   if(flag == 0){
                    e.preventDefault();
                    
                    $('#modal').modal('show');
                   
                    
                
                }else{
                
                var form_data = new FormData();

                form_data.append('id_respuesta',idresp );
                form_data.append('crit_numero', numcrit);
                form_data.append('puntajexcriterio', puntaje);
                form_data.append('domid', 3);
                form_data.append('puntajetotal', puntajetotal);

                var url = "<?php echo base_url(); ?>Criterios/insertresultadosxcriterioydominio";
                $.ajax({

                    type: "POST",
                    url: url,
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (php) {
                        localStorage.setItem('exito', '1');

//                             $('#contestado').modal('show');
                        window.location.href = "<?= base_url() ?><?=$siguiente?>";


                    }
                });//ajax

                return false;
                }
            });//onsubmit
window.onbeforeunload = function (e) {
        if (submitted == false) {
            var message = "You have not saved your changes.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }

        });//docready

    </script>