<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">
<style type="text/css">
.noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
}
          
          a:hover{
              cursor: pointer; cursor: hand;
          }
          .clear {clear:both;}
        </style>
        
        
 <script> 
    $(function(){
           if(  localStorage.getItem('exito')){
                var exitoso = '';
                   if(  localStorage.getItem('exito')==='1'){      
               
     exitoso = '   <div class="alert alert-success" id="success-alert2" style="margin-top: 15px;">'+
   ' <button type="button" class="close" data-dismiss="alert">x</button>'+
    '<i class="glyphicon glyphicon-saved"></i><strong> Tus respuestas han sido guardadas correctamente!</strong>'+
                             '</div>';
                     
                     } else {
                           exitoso = '   <div class="alert alert-danger" id="success-alert2" style="margin-top: 15px;">'+
   ' <button type="button" class="close" data-dismiss="alert">x</button>'+
    '<strong>Lo sentimos, hubo un error al guardar tus respuestas, por favor intentalo denuevo</strong>'+
                             '</div>';   
                         }
  
    $('#exitoso').html(exitoso);
     localStorage.removeItem('exito'); 
    
      $("#success-alert2").alert();
                $("#success-alert2").fadeTo(3000, 500).slideUp(500, function(){
               $("#success-alert2").alert('close');
                });   
   
 
}
 
    });
    </script>       
        


    
    
    
<div class="contenedor">
 
    <ol class="breadcrumb">



        <?php $total=0;
        $actual=2;?>
        <?php foreach ($dominios as $dom){

            ?>
            <li><?=$dom->DOM_NOMBRE?></li>

            <?php
            if($dominios[$total]->DOM_ID==$actual){
                $aux=$total;
            }

            $total=$total+1;}

        if($dominios[$total-1]->DOM_ID==$actual){
            $siguiente='Criterios/dominios_completados';
        }else{
            $next=$dominios[$aux+1]->DOM_ID;
            $str="$next";
            $siguiente='Criterios/dom'.$str;

        }
        ?>

    </ol>
       <div class="clear" id="exitoso" ></div>
       <br>
    <br>
    <h2>2.- Administración y Contabilidad</h2>
    <br>
    <br>



    <!-- Main content -->


    <div class="contenedor-mostrar-empresa">

        <form id="dom2" class="noselect">
             <?php 
            $subnombre= 0;
            foreach ($lista as $value){ 
                $i=1;
                
            if($value->CRIT_NUMERO>10 && $value->CRIT_NUMERO<14 && $subnombre==0){
                echo '<h2 style="text-align:left">2.1 '.$value->SUB_NOMBRE.'</h2><br><br>';
                $subnombre=1;
            }    
            
            if($value->CRIT_NUMERO>13 && $value->CRIT_NUMERO<22 && $subnombre==1){
                echo '<h2 style="text-align:left">2.2 '.$value->SUB_NOMBRE.'</h2><br><br>';
                $subnombre=2;
            }    
            if($value->CRIT_NUMERO>21 && $value->CRIT_NUMERO<29 && $subnombre==2){
                echo '<h2 style="text-align:left">2.3 '.$value->SUB_NOMBRE.'</h2><br><br>';
                $subnombre=3;
            }    
            if($value->CRIT_NUMERO>28 && $value->CRIT_NUMERO<34 && $subnombre==3){
                echo '<h2 style="text-align:left">2.4 '.$value->SUB_NOMBRE.'</h2><br><br>';
                $subnombre=4;
                
            }    
            

            
                ?>
       <!-- preguntas -->  
       
       <div class="pregunta">
        <div class="list-group col-md-12">
 <a id="resp"  class="list-group-item active">
     <p class="list-group-item-text">  <?php echo $value->CRIT_NUMERO;  ?>.- <?php echo $value->CRIT_PREGUNTA;  ?>  </p>

</a>
                </div>    

   
      <?php foreach ($respuestas as $resp){
       ?>
   
   <!-- ----------------------------------------------  -->
   <?php if($value->CRIT_NUMERO==$resp->CRIT_NUMERO){ ?>
   <?php
   
   /* ----------------------------*/ 
       if($i==1){$titulo='NO DESARROLLADO';}else{
           if($i ==2){$titulo='ESCASAMENTE DESARROLLADO ';} else {
               if($i==3){$titulo='PARCIALMENTE DESARROLLADO';}else{
                   if($i==4){$titulo='AMPLIAMENTE DESARROLLADO';}
               }
}
       }
       
       /*------------- */
       ?>
   
   <!-- preguntas con 3 respuestas -->
   <?php if($value->CRIT_NUMERO > 21 && $value->CRIT_NUMERO < 34){ ?>
   
      <div  class="list-group col-md-3" value="<?=$resp->CRIT_NUMERO?>">
  <a id="<?=$resp->ID_RESPUESTA;?>"  class="list-group-item" value="<?=$resp->RESP_PONDERACION?>">
      <h4 class="list-group-item-heading"><?=$resp->RESP_DESCRIPCION?></h4>
                    
  </a>
</div>
   
   <?php }else{ ?> <!--preguntas de 4 respuestas -->
   
   
   <!-- respuestas -->
   <div  class="list-group col-md-3" value="<?=$resp->CRIT_NUMERO?>">
  <a id="<?=$resp->ID_RESPUESTA;?>"  class="list-group-item" value="<?=$resp->RESP_PONDERACION?>">
      <h4 class="list-group-item-heading"><?=$titulo?></h4>
      <br>
    <p class="list-group-item-text">
                    <?php echo $resp->RESP_DESCRIPCION; ?>
</p>

                    
  </a>
</div>
   <?php } ?> <!--fin else -->
<!-- fin respuestas -->
<?php $i=$i+1;}  ?>
   <!-- ----------------------------------------------  -->

                    
            
                <?php } ?>
       </div><div class="clear"></div>
                <?php } ?>

     
      
        
        <div class="col-sm-12 col-md-12">
            <button type="submit" name="btnenviar" id="btnenviar" class="btn botonesgestion " style="width: 100% " >Continuar</button>
            <br>
            <br>
        </div>
  </form>
   
    <!-- /.content -->

</div><!-- /.content-wrapper -->



<div id="modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Atención !</h4>
      </div>
      <div class="modal-body">
          <p><strong>Debes responder por lo menos una pregunta !</strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<div id="contestado" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Dominio: Administración y Contabilidad</h4>
      </div>
      <div class="modal-body">
          <p><strong>Tus respuestas han sido guardadas correctamente!</strong> </p>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="javascript:window.location='<?=base_url()?><?=$siguiente?>'" class="btn btn-info">Continuar</button>
      </div>
    </div>

  </div>
</div>


 <script>
 
   $('.list-group[value=11]').on('click','> a', function(e) {
   var $this = $(this);
   
    if($('.list-group[value=11]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=11]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=12]').on('click','> a', function(e) {
   var $this = $(this);
   
    if($('.list-group[value=12]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=12]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=13]').on('click','> a', function(e) {
   var $this = $(this);
   if($('.list-group[value=13]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=13]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});

   $('.list-group[value=14]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=14]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=14]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=15]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=15]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=15]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=16]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=16]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=16]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=17]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=17]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=17]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=18]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=18]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=18]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=19]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=19]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=19]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=20]').on('click','> a', function(e) {
   var $this = $(this);
  
  if($('.list-group[value=20]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=20]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=21]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=21]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=21]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=22]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=22]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=22]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=23]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=23]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=23]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=24]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=24]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=24]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=25]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=25]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=25]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=26]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=26]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=26]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=27]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=27]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=27]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=28]').on('click','> a', function(e) {
   var $this = $(this);
   
   if($('.list-group[value=28]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=28]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=29]').on('click','> a', function(e) {
   var $this = $(this);
   if($('.list-group[value=29]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=29]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=30]').on('click','> a', function(e) {
   var $this = $(this);
   if($('.list-group[value=30]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=30]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=31]').on('click','> a', function(e) {
   var $this = $(this);
    
    if($('.list-group[value=31]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=31]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=32]').on('click','> a', function(e) {
   var $this = $(this);
    if($('.list-group[value=32]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=32]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});
   $('.list-group[value=33]').on('click','> a', function(e) {
   var $this = $(this);
    if($('.list-group[value=33]').find('.active').attr('value')== $this.attr('value')){
       $this.toggleClass('list-group-item-success active');
   }else{
    $('.list-group[value=33]').find('.active').removeClass('list-group-item-success active');
    $this.addClass('list-group-item-success active');}
});


     
        $(document).ready(function () {

var submitted= false;
            $('#dom2').on('submit', function (e) {
                 submitted = true;
                var idresp = [];
                var numcrit = [];
                var puntaje = [];
                var puntajetotal=0;    
                var flag=0;

/* preguntas 11-34*/
for(var i=11;i<34;i++){
    
    if($('.list-group[value='+i+']').find('.active').attr('id')){
    idresp.push($('.list-group[value='+i+']').find('.active').attr('id'));
    puntaje.push($('.list-group[value='+i+']').find('.active').attr('value'));
    numcrit.push(i);
    flag=1;
    puntajetotal= puntajetotal+parseInt(($('.list-group[value='+i+']').find('.active').attr('value')));
    }
}
                
                /*------------------------------------------------------------*/
                   if(flag == 0){
                    e.preventDefault();
                    
                    $('#modal').modal('show');
                   
                    
                
                }else{
                
                var form_data = new FormData();

                form_data.append('id_respuesta',idresp );
                form_data.append('crit_numero', numcrit);
                form_data.append('puntajexcriterio', puntaje);
                form_data.append('domid', 2);
                form_data.append('puntajetotal', puntajetotal);


                var url = "<?php echo base_url(); ?>Criterios/insertresultadosxcriterioydominio";
                $.ajax({

                    type: "POST",
                    url: url,
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (php) {
                        localStorage.setItem('exito', '1');

//                            $('#contestado').modal('show');
                        window.location.href = "<?= base_url() ?><?=$siguiente?>";


                    }
                });//ajax

                return false;
                }
            });//onsubmit

window.onbeforeunload = function (e) {
        if (submitted == false) {
            var message = "You have not saved your changes.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }
        });//docready

    </script>