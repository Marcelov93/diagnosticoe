<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>


    .contenedor-mostrar-empresa{
        width:50%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;

    }



</style>

<script>
    $(function(){
        if(  localStorage.getItem('exito')){
            var exitoso = '';
            if(  localStorage.getItem('exito')==='1'){

                exitoso = '   <div class="alert alert-success" id="success-alert2" style="margin-top: 15px;">'+
                    ' <button type="button" class="close" data-dismiss="alert">x</button>'+
                    '<i class="glyphicon glyphicon-saved"></i><strong> Tu información ha sido guardada correctamente!</strong>'+
                    '</div>';

            } else {
                exitoso = '   <div class="alert alert-danger" id="success-alert2" style="margin-top: 15px;">'+
                    ' <button type="button" class="close" data-dismiss="alert">x</button>'+
                    '<strong>Lo sentimos, hubo un error al guardar tu información, por favor intentalo denuevo</strong>'+
                    '</div>';
            }

            $('#exitoso').html(exitoso);
            localStorage.removeItem('exito');

            $("#success-alert2").alert();
            $("#success-alert2").fadeTo(3000, 500).slideUp(500, function(){
                $("#success-alert2").alert('close');
            });


        }

    });
</script>

<div class="contenedor">
    <div class="clear" id="exitoso" ></div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <h2>SELECCIONE UN DOMINIO A MEJORAR</h2>
        </div>


    </div>
    <br>

    <?php

    if($existe){
        $total=$this->Criterios_model->presupuesto_total($idana);
        echo '<h3>Presupuesto Total Requerido: $'.$total.' </h3>';
        }?>
    <br>
    <br>



    <!-- Main content -->

    <div class="contenedor-mostrar-empresa ">

        <!--<div class="col-md-12">-->
        <?php foreach ($dominios as $value){

             $puntajeOmitido=$this->Resultados_model->puntaje_omitido($value->ANA_ID, $value->DOM_ID);
        if(isset($puntajeOmitido)){
        $puntajeMaximo=$value->DOM_PONDERACION-$puntajeOmitido;
        $score=round((($value->PUNTAJEXDOMINIO*100)/$puntajeMaximo),2);
        }else{
            $score=round((($value->PUNTAJEXDOMINIO*100)/$value->DOM_PONDERACION),2);
        }
     
            ?>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                                <?php
      if($score>67 && $score<100){
        echo '<a href="'.base_url().'Criterios/ver_respuestas'.$value->DOM_ID.'/'.$idana.'" ><li class="list-group-item"><i class="glyphicon glyphicon-chevron-right"> </i><strong> '. $value->DOM_NOMBRE.'</strong><span class="badge badge-success">Porcentaje Cumplimiento: '.$score.'%</span></li></a>';
            }else if($score>33 &&$score<=67){
                echo '<a href="'.base_url().'Criterios/ver_respuestas'.$value->DOM_ID.'/'.$idana.'" ><li class="list-group-item"><i class="glyphicon glyphicon-chevron-right"> </i><strong> '.$value->DOM_NOMBRE.'</strong><span class="badge badge-warning">Porcentaje Cumplimiento: '.$score.'%</span></li></a>';
            }else if($score>=0 &&$score<=33){
                echo '<a href="'.base_url().'Criterios/ver_respuestas'.$value->DOM_ID.'/'.$idana.'" ><li class="list-group-item"><i class="glyphicon glyphicon-chevron-right"> </i><strong> '.$value->DOM_NOMBRE.'</strong><span class="badge badge-danger">Porcentaje Cumplimiento: '.$score.'%</span></li></a>';
            }  ?>
<!--                    <a href="<?=base_url()?>Criterios/ver_respuestas<?=$value->DOM_ID.'/'.$idana?>" ><li class="list-group-item"><?php echo $value->DOM_NOMBRE;  ?></li></a>-->
                </div>

            </div>

            <br>
        <?php } ?>
        <br>

        <br>


        <div class="col-md-6 col-sm-6"></div>
        <div class="col-md-4 col-sm-4">
            <a href="<?=base_url()?>Criterios/pdf_plan_mejora/<?=$idana?>" target="_blank" class="btn btn-info"><i class="glyphicon glyphicon-print"></i> IMPRIMIR INFORME</a>
        </div>
        <div class="col-md-2 col-sm-2">
            <a href="<?=base_url()?>Criterios/mejoramiento" class="btn btn-success ">VOLVER</a>
        </div>

        <br>

        <br>
        <br>

        <br>

        <!--</div>-->
    </div>

    <script>



    </script>