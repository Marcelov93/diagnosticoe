<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">


<script>
    function changeclass(id) {

        var activo = document.getElementById(id)

        if(activo.className=='list-group-item active col-md-12'){
            activo.className="list-group-item col-md-12"
        }else
            activo.className="list-group-item active col-md-12"

    }
</script>

<div class="contenedor">
    <div class="alert alert-warning alert-dismissible" role="alert" id="alerta">

        <strong>Un momento!</strong> La ponderacion de los dominios seleccionados debe sumar 100.
    </div>
    <div class="alert alert-warning alert-dismissible" role="alert" id="alerta2">

        <strong>Un momento!</strong> Seleccione un dominio.
    </div>
    <br>
    <br>
    <div class="row">
    <div class="col-md-9 col-xs-9">
        <h2>SELECCIONE LOS DOMINIOS A EVALUAR</h2>
    </div>
    <button type="button" id="manual" class="btn btn-warning">Ponderaci&oacute;n manual</button>
    <button type="button" id="automatico" class="btn btn-warning">Ponderaci&oacute;n Automatica</button>
        <button type="button"  class="btn btn-info" data-toggle="modal" data-target="#ayuda">Ayuda  <i class="glyphicon glyphicon-question-sign"></i></button>
        </div>
    <br>
    <br>



    <!-- Main content -->
    <form id="dominios">

        <div class="contenedor-mostrar-empresa ">

        <!--<div class="col-md-12">-->
            <?php $id=11?>
        <?php foreach ($lista as $value){

            ?>

            <div class="row">
                <div class="col-md-8 col-xs-9">
                    <a ><li id="<?=$value->DOM_ID?>" class="list-group-item" onclick="changeclass(<?=$value->DOM_ID?>);"><strong><?php echo $value->DOM_NOMBRE;  ?></strong></li></a>
                </div>
                <div class="col-md-2 col-xs-1"></div>
                <div class="col-md-2 col-xs-2">
                    <select class="form-control" id="<?=$id?>">
                        <?php for ($i=0;$i<=100;$i++){ ?>
                        <option><?php echo $i ."%" ?></option>
                        <?php } ?>

            </select>
            </div>
                </div>

            <br>
        <?php $id++; } ?>
        <br>
        <button type="submit" name="btnenviar" id="btnenviar" class="btn btn-success col-md-8 col-xs-9"  >Continuar</button>

            <br>
            <br>
            <br>


        </form>


    <div id="ayuda" class="modal fade " role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Instrucciones:</h4>
                </div>
                <div class="modal-body">
                   <i class="glyphicon glyphicon-chevron-right"></i> Debe elegir seg&uacute;n su criterio, cuales son los dominios o areas que le interesa medir.
                    <br>
                    <br>
                   <i class="glyphicon glyphicon-chevron-right"></i> Puede optar por la opci&oacute;n manual, para designar manualmente las ponderaciones de cada dominio, o bien
                    optar por la opci&oacute;n autom&aacute;tica, que distribuye los porcentajes de forma instantanea.
                    
                   
                    <br>
                    <br>
                    <br>
                    
                    <strong>Descripción de los dominios:</strong>
                    <p><i>*Si algunos de los subdominios no corresponden a su empresa, puede omitir las preguntas posteriormente.</i></p>
                    <hr>
                    
                    <div class="panel-group" id="accordion">
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
        1.- Gobierno Empresarial y Estratégia</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
          <p>
              <b>SUBDOMINIO 1.1: ESTRATEGIA</b><br><br>              
       Este apartado busca conocer las líneas principales del negocio,
          cuál es su razón de ser, su objetivo final para conseguir transmitir su propuesta de valor y finalmente,
          generar valor tanto para sus clientes como para la empresa.
          <br><br>
      También describe los mecanismos por los cuales la empresa está llevando a cabo las líneas del negocio. La estrategia marca
      la forma en que se logra alcanzar los objetivos plasmados en la misión y visión de la organización,
      a través de objetivos estratégicos, metas y planes de acción.
           </p>
           <br>
           
           <p>
               <b>SUBDOMINIO 1.2: TOMA DE DECISIONES Y COHERENCIA</b><br><br>
Este apartado describe la forma en que la empresa
(empresario y empleados si tuviera) toma decisiones para alcanzar sus objetivos,
así como también la coherencia entre la misión, visión, objetivos, estrategia, metas,
planes y toma de decisiones para mantener una dirección y conducción clara en la organización 
para alcanzar sus objetivos.
          </p>
      
      </div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
       2.- Administración y Contabilidad</a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
            <p>
              <b>SUBDOMINIO 2.1: ADMINISTRACIÓN DE LA EMPRESA.</b><br><br>              
      Este apartado incluye características con las que debe contar el administrador y/o dueño
      de la empresa para gestionar los procesos internos de la organización, así como también políticas,
      normas, códigos o maneras en que la empresa se organiza y conduce a sus trabajadores por el cumplimiento de los objetivos,
      buscando la mejora continua dentro de sus procesos.
           </p>
           <br>
           
           <p>
               <b>SUBDOMINIO 2.2: CONTABILIDAD FINANCIERA</b><br><br>
Este apartado muestra si la empresa hace registro y movimiento de ingresos y egresos de la organización,
con el fin de conocer la rentabilidad del negocio, sus utilidades reales y cuánto de éstas serán
parte del “sueldo del empresario” y cuánto para la empresa. Al mismo tiempo, busca conocer cómo 
se organizan y distribuyen los costos que incurre el negocio en su proceso productivo y si existe 
claridad del capital de trabajo necesario para producir.
           </p><br>
                 <p>
              <b>SUBDOMINIO 2.3: CUMPLIMIENTO DE NORMATIVAS LEGALES</b><br><br>              
      En este apartado se pretende conocer el estado legal de la empresa, su estado de formalización 
      y el cumplimiento de normativas básicas requeridas para operar dentro del marco legal apropiado al tipo de negocio.
                 </p><br>
                 <p>
              <b>SUBDOMINIO 2.4: CONTABILIDAD TRIBUTARIA</b><br><br>              
En este punto del análisis, se busca conocer si la empresa realiza 
acciones básicas dado el sistema de tributación al cual se encuentra acogida, 
de modo de que las operaciones de ésta se desarrollen en un marco tributario correcto.
           </p>
            
            
            
        </div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
        3.- Personas</a>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <div class="panel-body">
      
      <p>
              <b>SUBDOMINIO 3.1: SITUACIÓN CONTRACTUAL</b><br><br>              
 Este apartado pretende conocer la situación contractual de la empresa con sus empleados,
 la existencia de una jornada de trabajo establecida, definición de cargos y estado de cotizaciones e imposiciones,
 entre otros temas relevantes en términos legales de la situación actual de las relaciones laborales en la empresa.
           </p>
           <br>
           
           <p>
               <b>SUBDOMINIO 3.2: INTEGRACIÓN DE PERSONAL</b><br><br>
Este apartado muestra la forma en que se gestionan y desarrollan a
las personas dentro de la empresa, describe el desarrollo de empleados, 
su reclutamiento, selección, rol en la empresa, capacitación, sueldos e incentivos.
Factores fundamentales para generar una comunicación fluida entre las partes 
e incentivar la mejora continua dentro de la empresa.
           </p><br>
           
           <p>
               <b>SUBDOMINIO 3.3: CAPACITACIÓN Y DESARROLLO</b><br><br>
Describe al existencia de capacitaciones para los empleados, 
así como también formas que en se desarrollan dentro de la empresa,
entendiendo la existencia de necesidades de la empresa para capacidad al 
personal en función de los objetivos de la organización.
           </p><br>
      
      </div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
        4.- Modelos de Negocios</a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <div class="panel-body">
      
      <p>
              <b>SUBDOMINIO 4.1: PROPUESTA DE VALOR</b><br><br>              
 En este apartado se abarcaran los aspectos esenciales del negocio,
 dentro de su cadena de valor, entendiendo en primer lugar la propuesta de valor,
 y producto/servicio que se ofrece, para luego entender como es ofrecido,
 con quienes se relaciona para conseguir el producto/servicio final,
 a quienes ofrece, y finalmente, la existencia de estrategias para 
 conseguir que el valor sea transmitido para los clientes y que la empresa
 sea capaz de absorber valor para el negocio.
           </p>
           <br>
           
           <p>
               <b>SUBDOMINIO 4.2: CLIENTES</b><br><br>
En este subdominio se establece la relación entre la empresa y sus clientes,
las formas que tiene ésta para manejar sus bases de clientes,
la relación formal e informal que se genera con éstos,
y todos los aspectos concernientes a la gestión efectiva de clientes
           </p><br>
           
           <p>
               <b>SUBDOMINIO 4.3: COMERCIALIZACIÓN</b><br><br>
En el análisis de este subdominio se consideran aspectos relativos
al producto o servicio comercializado (y como éste cumple con niveles de calidad,
procesos de mejora continua, atención a las necesidades de los clientes, entre otros),
así como también entender los mecanismos que la empresa utiliza para llegar a sus clientes.
           </p><br>
           
           <p>
               <b>SUBDOMINIO 4.4: COHERENCIA ENTRE PROPUESTA DE VALOR, ESTRATEGIAS Y CLIENTES</b><br><br>
En este subdominio se evalúa la relación existente entre los elementos del modelo de negocios,
específicamente entre la propuesta de valor (qué es lo que se ofrece),
estrategias (cómo logro transmitir la propuesta de valor) 
y clientes (a quién irá dirigida la propuesta), y con esto evaluar cuan coherentes 
y alineados están entre sí para conseguir los objetivos de la empresa.
           </p><br>
           <p>
               <b>SUBDOMINIO 4.5 RELACIÓN CON PROVEEDORES</b><br><br>
Dentro de este subdominio se pretende establecer
el tipo de relación que posee la empresa con sus proveedores:
los criterios de selección que rigen la elección de éstos,
los mecanismos de análisis, control, relaciones cooperativas entre estos, entre otras.
           </p><br>
      
      
      </div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
        5.- Procesos</a>
      </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
      <div class="panel-body">
      
      <p>
              En este apartado se establece los métodos que 
              tiene la empresa en su proceso productivo, el uso de 
              recursos de manera eficiente, así como también el conocimiento de inventario,
              control de calidad y presupuesto de producción para ofrecer un producto/servicio 
              de calidad y capturar mayor valor para el negocio.
           </p><br>
      </div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
        6.- Medición, Análisis y Desempeño</a>
      </h4>
    </div>
    <div id="collapse6" class="panel-collapse collapse">
      <div class="panel-body">
          <p>
         Este dominio busca establecer cómo la empresa mide,
         analiza y utiliza la información del desempeño de la empresa
         y así tomar decisiones en busca de una mejora continua de sus 
         procesos y actividades que permitan generar mayor valor al negocio.     
          </p>      
          </div>
    </div>
  </div>
</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendido</button>
                </div>
            </div>

        </div>
    </div>

    </div>
    </div>


<script>

    $("#alerta").hide();
    $("#alerta2").hide();
    $("#automatico").hide();


    for(var i=11;i<=16;i++){
        $('#'+i+'').hide();
    }

    $(document).ready(function () {

        $("#manual").click(function(){
            $("#automatico").show();
            $("#manual").hide();
            
            
            for(var i=11;i<=16;i++){
                $('#'+i+'').toggle();
            }

        });
        
        $("#automatico").click(function(){
            $("#automatico").hide();
            $("#manual").show();
            
            
            for(var i=11;i<=16;i++){
                $('#'+i+'').toggle();
            }

        });

        $('#dominios').on('submit', function (e) {

        //$(this).find(':input[type=submit]').prop('disabled', true);
        
            var id_dom = [];
            var cont= 0, pond = [];

            var resp1 = document.getElementById(1)
            var resp2 = document.getElementById(2)
            var resp3 = document.getElementById(3)
            var resp4 = document.getElementById(4)
            var resp5 = document.getElementById(5)
            var resp6 = document.getElementById(6)



            if(resp1.className=='list-group-item active col-md-12'){

                id_dom.push(1);
                cont=cont+1;

            }
            if(resp2.className=='list-group-item active col-md-12'){

                id_dom.push(2);
                cont=cont+1;
            }
            if(resp3.className=='list-group-item active col-md-12'){

                id_dom.push(3);
                cont=cont+1;
            }
            if(resp4.className=='list-group-item active col-md-12'){

                id_dom.push(4);
                cont=cont+1;
            }
            if(resp5.className=='list-group-item active col-md-12'){

                id_dom.push(5);
                cont=cont+1;
            }
            if(resp6.className=='list-group-item active col-md-12'){

                id_dom.push(6);
                cont=cont+1;
            }

            var resp11 = parseInt($('#11').val());
            var resp12 = parseInt ($('#12').val());
            var resp13 = parseInt ($('#13').val());
            var resp14 = parseInt ($('#14').val());
            var resp15 = parseInt ($('#15').val());
            var resp16 = parseInt ($('#16').val());

            var suma=0;

            if(resp1.className=='list-group-item active col-md-12' && resp11==0){

                pond.push(100/cont);
                suma=suma+(100/cont);
            }else if(resp1.className=='list-group-item active col-md-12' && resp11!=0){
                pond.push(resp11);
                suma=suma+resp11;
            }

            if(resp2.className=='list-group-item active col-md-12' && resp12==0){

                pond.push(100/cont);
                suma=suma+(100/cont);
            }else if(resp2.className=='list-group-item active col-md-12' && resp12!=0){
                pond.push(resp12);
                suma=suma+resp12;
            }

            if(resp3.className=='list-group-item active col-md-12' && resp13==0){

                pond.push(100/cont);
                suma=suma+(100/cont);
            }else if(resp3.className=='list-group-item active col-md-12' && resp13!=0){
                pond.push(resp13);
                suma=suma+resp13;
            }

            if(resp4.className=='list-group-item active col-md-12' && resp14==0){
                pond.push(100/cont);
                suma=suma+(100/cont);
            }else if(resp4.className=='list-group-item active col-md-12' && resp14!=0){
                pond.push(resp14);
                suma=suma+resp14;
            }

            if(resp5.className=='list-group-item active col-md-12' && resp15==0){
                pond.push(100/cont);
                suma=suma+(100/cont);

            }else if(resp5.className=='list-group-item active col-md-12' && resp15!=0){
                pond.push(resp15);
                suma=suma+resp15;
            }

            if(resp6.className=='list-group-item active col-md-12' && resp16==0){

                pond.push(100/cont);
                suma=suma+(100/cont);
            }else if(resp6.className=='list-group-item active col-md-12' && resp16!=0){
                pond.push(resp16);
                suma=suma+resp16;
            }

            /*------------------------------------------------------------*/
            if(cont!=0){

            if(suma<101 && suma>=100){
               $(this).find(':input[type=submit]').prop('disabled', true);

            var form_data = new FormData();

            form_data.append('dom_id',id_dom );
            form_data.append('ponderacion',pond );



            var url = "<?php echo base_url(); ?>Criterios/ponderacionxdominio";
            $.ajax({

                type: "POST",
                url: url,
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (php) {
                    localStorage.setItem('enviado', '1');

                    window.location.href = "<?= base_url() ?>Criterios/progreso";


                }
            });//ajax
            }    else{
                $("#alerta").fadeTo(3000, 500).slideUp(500, function(){
                    $("#alerta").alert('close');
                });




            }

            }else{
                $("#alerta2").fadeTo(3000, 500).slideUp(500, function(){
                    $("#alerta2").alert('close');
                });




            }
            return false;

        });//onsubmit





    });//docready




</script>