<link rel="stylesheet" type='text/css' href="<?=base_url()?>assets/css/formularios.css">

<style>
    .contenedor-mostrar-empresa{
        width:40%;
        max-width:1000px;
        margin:auto;
        overflow:hidden;
    }
    textarea {
        resize: none;
    }



</style>
<script> 
    $(function(){
           if(  localStorage.getItem('exito')){
                var exitoso = '';
                   if(  localStorage.getItem('exito')==='1'){      
               
     exitoso = '   <div class="alert alert-success" id="success-alert2" style="margin-top: 15px;">'+
   ' <button type="button" class="close" data-dismiss="alert">x</button>'+
    '<i class="glyphicon glyphicon-saved"></i><strong> Tus respuestas han sido guardadas correctamente!</strong>'+
                             '</div>';
                     
                     } else {
                           exitoso = '   <div class="alert alert-danger" id="success-alert2" style="margin-top: 15px;">'+
   ' <button type="button" class="close" data-dismiss="alert">x</button>'+
    '<strong>Lo sentimos, hubo un error al guardar tus respuestas, por favor intentalo denuevo</strong>'+
                             '</div>';   
                         }
  
    $('#exitoso').html(exitoso);
     localStorage.removeItem('exito'); 
    
      $("#success-alert2").alert();
                $("#success-alert2").fadeTo(3000, 500).slideUp(500, function(){
               $("#success-alert2").alert('close');
                });   
   
 
}
 
    });
    </script>      

<div class="contenedor">
    <div class="clear" id="exitoso" ></div>
    <br>
    <br>
    <h2>PROGRESO ANÁLISIS</h2>
    <br>
    <br>



    <!-- Main content -->
    

    <div class="contenedor-mostrar-empresa">
    
        
        <?php 
        $i=0;
        foreach ($dominios as $dom){  
        ?>
    <ul class="list-group col-md-12">
    
    <?php if($dom->PUNTAJEXDOMINIO != NULL){   
      echo  '<a class="list-group-item list-group-item-info">';
   echo '<span class="badge badge-success"> Terminado </span>';
    } else {
      echo ' <a  href="'.base_url().'Criterios/dom'.$dom->DOM_ID.'" class="list-group-item list-group-item-info">';
        echo '<span class="badge badge-danger"> Sin Contestar </span>';
        $i=1;
    }?>       
        <strong><?=$dom->DOM_NOMBRE?></strong>
    </a>
    </ul>
        
           <?php } ?>
        <br>
        
       </div>    
        
      <div class="contenedor-mostrar-empresa">  
          <?php if($i==1){
        echo '<a  class="btn btn-success col-md-12">Puedes ver los resultados cuando completes todos los dominios!</a>';
          }else{
        echo '<a  href="'.base_url().'Resultados/results" class="btn btn-success col-md-12">VER RESULTADOS</a>';
          } ?>
                <br>
        <?php if (isset($ana_id_inicial) && $i==0){
           
        echo '<br><a href="'.base_url().'Resultados/comparar_diagnosticos/'.$ana_id_inicial.'/'.$ana_id.'" class="btn btn-success col-md-12">VER COMPARACIÓN Y MEDICION DE IMPACTO</a>';
         }
        ?>    
        <br>
        <br>
        <a href="<?=base_url()?>Usuarios/menu_diagnostico" class="btn btn-danger col-md-12">SALIR</a>
    </div>
    <br>
    <br>
    </div>

